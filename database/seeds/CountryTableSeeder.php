<?php

use Illuminate\Database\Seeder;
use App\Country;
class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = ['PAKISTAN','DUBAI','SAUDI ARABIA','IRAQ'];
        foreach($countries as $country){
            Country::create(['name' => $country]);
        }
    }
}
