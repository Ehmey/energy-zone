<?php

use Illuminate\Database\Seeder;
use App\Company;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = ['DGS','TELENOR','Warid','ZONG'];
        foreach($companies as $company){
            Company::create(['name' => $company]);
        }
    }
}
