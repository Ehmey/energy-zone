<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CompanyTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(JobTitleTableSeeder::class);
        $this->call(LangaugesTableSeeder::class);
        $this->call(SkillsTableSeeder::class);
        $this->call(SchoolTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(DegreeTableSeeder::class);
        $this->call(GroupTypeSeeder::class);
        // $this->call(UsersTableSeeder::class);
    }
}
