<?php

use Illuminate\Database\Seeder;

class DegreeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $degrees = ['BSC','MSC','PHD','INTER'];
        foreach($degrees as $degree){
            \App\User\Degree::create(['name' => $degree]);
        }
    }
}
