<?php

use Illuminate\Database\Seeder;

use App\User\School;


class SchoolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jobsTitle = ['LGS','LACAS','Lahore Garrison'];
        foreach($jobsTitle as $jobsTitle){
            School::create(['name' => $jobsTitle]);
        }
    }
}
