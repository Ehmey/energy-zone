<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name' => 'Ahmed Alvi', 'email' => 'ahmedalvi.34@hotmail.com', 'password' => bcrypt('111111'), 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1, 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2],
            ['name' => 'Mohsina Asrar', 'email' => 'mohsinaasrar372@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' Asrar', 'email' => 'mohsinaasrar3723@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' Assrar', 'email' => 'mohsinaasrasr3723@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting', 'email' => 'testing@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting1', 'email' => 'testing1@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting2', 'email' => 'testing2@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting3', 'email' => 'testing3@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting4', 'email' => 'testing4@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting5', 'email' => 'testing5@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting6', 'email' => 'testing6@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting7', 'email' => 'testing7@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting8', 'email' => 'testing8@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting9', 'email' => 'testing9@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting10', 'email' => 'testing10@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting11', 'email' => 'testing11@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting12', 'email' => 'testing12@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting13', 'email' => 'testing13@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => ' tsting15', 'email' => 'testing15@gmail.com', 'password' => bcrypt('111111'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 2, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,],
            ['name' => 'Admin', 'email' => 'admin@admin.com', 'password' => bcrypt('adminadmin'), 'step' => 3, 'is_block' => 0, 'active' => 1, 'role_id' => 1, 'company_id' => 1, 'country_id' => 1, 'job_title_id' => 1,]
        ];

        foreach ($users as $user) {
            $user = User::create($user);
            \App\UserInfo::create(['user_id' => $user->id]);
        }

    }
}
