<?php

use Illuminate\Database\Seeder;
use App\Groups\GroupTypes;

class GroupTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['Engineers','Soil Inspectors','Workers'];
        foreach($types as $type){
            GroupTypes::create(['name' => $type]);
        }


    }
}
