<?php

use Illuminate\Database\Seeder;

class JobTitleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jobsTitle = ['SOIL INSPECTOR','SURFACE ENGINEER','MANAGEMENT INCHARGE'];
        foreach($jobsTitle as $jobsTitle){
            \App\JobTitle::create(['name' => $jobsTitle]);
        }
    }
}
