<?php

use Illuminate\Database\Seeder;
use App\User\Skill;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skills = ['HTML','CSS','JS','PHP','PROJECT MANAGEMENTS'];
        foreach($skills as $skill){
            Skill::create(['name' => $skill]);
        }
    }
}
