<?php

use Illuminate\Database\Seeder;
use App\User\Language;


class LangaugesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = ['URDU','ENGLISH','ARABIC'];
        foreach($languages as $language){
            Language::create(['name' => $language]);
        }
    }
}
