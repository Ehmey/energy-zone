<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserExperincesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_experinces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('experince','2000');
            $table->integer('user_id')->unsigned();
            $table->integer('job_title_id')->unsigned();
            $table->integer('company_id')->unsigned();

            $table->foreign(['user_id'])->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign(['company_id'])->references('id')->on('companies')
                ->onDelete('cascade');

            $table->foreign(['job_title_id'])->references('id')->on('job_titles')
                ->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_experinces');
    }
}
