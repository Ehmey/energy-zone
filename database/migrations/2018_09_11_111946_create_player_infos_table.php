<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('auth_code')->nullable();
            $table->string('description','2000')->nullable();
            $table->string('cover_image','1000')->default('energy-zone/user/images/user_cover_images/sample_cover.jpg')->nullable();
            $table->timestamps();
            $table->foreign(['user_id'])->references('id')->on('users')
                ->onDelete('cascade');
            //$table->foreign(['user_id'])->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_infos');
    }
}
