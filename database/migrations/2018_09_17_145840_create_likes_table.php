<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('post_id')->nullable()->unsigned();
            $table->integer('comment_id')->nullable()->unsigned();
          //  $table->integer('repl_id')->nullable()->unsigned();


            $table->unique(array('user_id', 'post_id','comment_id'));

            $table->foreign(['post_id'])->references('id')->on('posts')
                ->onDelete('cascade');
            $table->foreign(['user_id'])->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign(['comment_id'])->references('id')->on('comments')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
}
