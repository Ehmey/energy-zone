<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('job_title_id')->unsigned()->nullable();
            $table->integer('company_id')->unsigned()->nullable();
            $table->boolean('welcome_flag')->default(false);
            $table->integer('steps')->default('0');
            $table->string('email','50')->unique();
            $table->string('profile_image','1000')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('is_block')->default(false);
            $table->integer('role_id')->default('2');
            $table->integer('step')->default('0');
            $table->boolean('active')->default(true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
