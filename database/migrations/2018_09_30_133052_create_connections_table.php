<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accept_id')->unsigned();
            $table->integer('send_id')->unsigned();
            $table->enum('status', ['Pending', 'Accepted', 'Rejected']);
            $table->foreign(['accept_id'])->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign(['send_id'])->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connections');
    }
}
