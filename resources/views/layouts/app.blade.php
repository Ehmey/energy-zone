<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>
    <link rel="shortcut icon" type="image/png" href="{{asset('energy-zone/favicon.png')}}"/>
    <!-- Scripts -->

    <link rel="shortcut icon" type="image/png" href="{{asset('energy-zone/favicon.png')}}"/>
    <!-- Fonts -->


    <!-- Styles -->
    @include('user.includes.css')
    @include('user.includes.js')
</head>
<body>
<header class="complate-main-header-wrapper bg-white">
    @include('user.includes.header.user-main-header')
    @include('user.includes.menus.user-main-menu')
</header>
<section class="main-site-area">
    @include('alerts.alerts')
    <div id="for_alert">  </div>
    <div class="profile_db_main">
        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </div>

</section>
@include('user.includes.footers.user-footer')
@include('user.includes.popups.not-found-popup')

</body>
</html>
