<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>
    <link rel="shortcut icon" type="image/png" href="{{asset('energy-zone/favicon.png')}}"/>

    @include('admin.includes.css')

</head>
<body>
<header class="main-header-wrapper" id="sliding3">
    @include('admin.includes.header.header')
</header>

@include('admin.includes.sidebar.sidebar')




<div class="main-site-area-wrapper" id="sliding2">
                @yield('content')
</div>

@include('admin.includes.js')


</body>
</html>
