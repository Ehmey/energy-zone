@if (Session::has('success'))

    @if ($errors->has('email'))
        <div class="alert alert-danger" id="alert_notice">
            <strong>Failed!</strong> {{ $errors->first('email') }}
        </div>
    @endif


    <div class="alert alert-success clearfix" style="width:100%;display:inline-block;">
        <strong>Success!</strong> {{Session::get('success')}}
    </div>
    @elseif(Session::has('exception'))
    <div class="alert alert-danger" id="alert_notice">
        <strong>Failed!</strong> Some technical Issue
    </div>
@elseif(Session::has('wrong-code'))
    <div class="alert alert-danger" id="alert_notice">
        <strong>Failed!</strong> {{Session::get('wrong-code')}}
    </div>
@endif


@if (Session::has('edited-success'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('edited-success')}}
    </div>
    @endif

    @if (Session::has('post-deleted-success'))
        <div class="alert alert-success" id="alert_notice">
            <strong>Success!</strong> {{Session::get('post-deleted-success')}}
        </div>
@endif



@if (Session::has('skill-edit-success'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('skill-edit-success')}}
    </div>
@endif

@if (Session::has('skill-added'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('skill-added')}}
    </div>
@endif




@if (Session::has('language-added-failed'))
    <div class="alert alert-danger" id="alert_notice">
        <strong>Failed!</strong> {{Session::get('language-added-failed')}}
    </div>
@endif

@if (Session::has('skill-added-failed'))
    <div class="alert alert-danger" id="alert_notice">
        <strong>Failed!</strong> {{Session::get('skill-added-failed')}}
    </div>
@endif

@if (Session::has('language-added'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('language-added')}}
    </div>
@endif




@if (Session::has('language-delete-success'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('language-delete-success')}}
    </div>
@endif


@if (Session::has('post-success'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('post-success')}}
    </div>
@endif




@if (Session::has('education-added'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('education-added')}}
    </div>
@endif

@if (Session::has('education-delete-success'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('education-delete-success')}}
    </div>
@endif




@if (Session::has('post-hide-success'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('post-hide-success')}}
    </div>
@endif

@if (Session::has('experince-added'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('experince-added')}}
    </div>
@endif

@if (Session::has('school-added-success'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('school-added-success')}}
    </div>
@endif




@if (Session::has('degree-added-success'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('degree-added-success')}}
    </div>
@endif

@if (Session::has('company-added-success'))
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> {{Session::get('company-added-success')}}
    </div>
@endif



