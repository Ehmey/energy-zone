<div class="modal fade" id="companyModal" role="dialog">
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Company</h4>
            </div>
            <div class="modal-body">


                <div class="form-group">
                    <div class="input-group">
                        <div class="btn btn-default input-group-addon" type="submit">Add Company</div>
                    <input class="form-control" id="add_company" placeholder="Add Company" name="name" type="text" value="" />

                </div>
            </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="btn btn-default input-group-addon" type="submit">Add Image</div>
                        <input class="form-control" id="company_image" placeholder="Add Image" type="file" value="" />

                    </div>
                </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="saveSetup('companyModal')" class="btn btn-success" >Save</button>
    </div>

</div>

</div>
</div>




<div class="modal fade" id="languageModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Language</h4>
                </div>
                <div class="modal-body">


                    <div class="form-group">
                        <div class="input-group">
                            <div class="btn btn-default input-group-addon" type="submit">Add Language</div>
                            <input class="form-control" id="add_language" placeholder="Add Language" name="name" type="text" value="" />

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success"  onclick="saveSetup('languageModal')" >Save</button>
                </div>

        </div>

    </div>
</div>






<div class="modal fade" id="skillModal" role="dialog">
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Skill</h4>
            </div>
            <div class="modal-body">


                <div class="form-group">
                    <div class="input-group">
                        <div class="btn btn-default input-group-addon" type="submit">Add Skill</div>
                    <input class="form-control" id="add_skill" placeholder="Add Skill" name="name" type="text" value="" />

                </div>
            </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="saveSetup('skillModal')"  class="btn btn-success" >Save</button>
    </div>

</div>

</div>
</div>



<div class="modal fade" id="countryModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Country</h4>
            </div>
            <div class="modal-body">


                <div class="form-group">
                    <div class="input-group">
                        <div class="btn btn-default input-group-addon" type="submit">Add Country</div>
                        <input class="form-control" id="add_country" placeholder="Add Country" name="name" type="text" value="" />

                    </div>
                </div>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" onclick="saveSetup('countryModal')"  class="btn btn-success" >Save</button>
            </div>

        </div>

    </div>
</div>



<div class="modal fade" id="schoolModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add School</h4>
            </div>
            <div class="modal-body">


                <div class="form-group">
                    <div class="input-group">
                        <div class="btn btn-default input-group-addon" type="submit">Add School</div>
                        <input class="form-control" id="add_school" placeholder="Add School" name="name" type="text" value="" />

                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <div class="btn btn-default input-group-addon" type="submit">Add School Image</div>
                        <input class="form-control" id="add_school_image" placeholder="Add School" name="name" type="file"  accept="image/*" />

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" onclick="saveSetup('schoolModal')"  class="btn btn-success" >Save</button>
            </div>

        </div>

    </div>
</div>

<div class="modal fade" id="degreeModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Degree</h4>
            </div>
            <div class="modal-body">


                <div class="form-group">
                    <div class="input-group">
                        <div class="btn btn-default input-group-addon" type="submit">Add Degree</div>
                        <input class="form-control" id="add_degree" placeholder="Add Degree" name="name" type="text" value="" />

                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" onclick="saveSetup('degreeModal')"  class="btn btn-success" >Save</button>
            </div>

        </div>

    </div>
</div>