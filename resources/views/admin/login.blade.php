<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ config('app.name', 'Energy Zone') }} - Login</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">


	@include('admin.includes.css')
       
</head>

	<body>

	 <section class="admin_login_bg">
     	<div class="admin_login_main">
			<form method="post" action="{{route('login')}}">
				@csrf
        	<h1 class="admin_login_text">Login</h1>
            <ul class="admin_login_main_ul">
            	<li>
                	<i class="fas fa-user"></i>
                    <input type="text" placeholder="Email" name="email">

                </li>
				@if ($errors->has('email'))
					<span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
				@endif
                <li>
                	<i class="fas fa-key"></i>
                    <input type="password" placeholder="Password" name="password">

                </li>
				@if ($errors->has('password'))
					<span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
				@endif
                <li>
                	<input type="submit" value="Sign in">
                </li>
            </ul>
				<a class="forgot_pasword_admin" href="{{ route('password.request') }}">
					{{ __('Forgot Your Password?') }}
				</a>

			</form>
        </div>
     </section>
		



		@include('admin.includes.js')
	   
		
	</body>

</html>