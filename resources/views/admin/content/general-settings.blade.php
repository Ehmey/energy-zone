@extends('layouts.app-admin')

@section('title', "General Settings")

@section('settings_active', "active_nav")

@section('content')

    <div class="manage-content-wrapper">

        <div class="dashboard-heading clearfix">
            <h2>General Settings</h2>
            <ul>
                <li>Energy Zone / General Settings</li>
            </ul>
        </div>

        <div class="content-sorting clearfix">
            @include('alerts.alerts')
            <div class="drop_down_tab_main">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">

                    <li><a href="#setups" role="tab" data-toggle="tab"  onclick="getSetupsData('schools')">Schools</a></li>
                    <li><a href="#setups" role="tab" data-toggle="tab"  onclick="getSetupsData('degrees')">Degress</a></li>



                </ul>
                <!-- Tab panes -->
                <div class="tab-content" id="setups-tab-content">
                    <div class="tab-pane active" id="setups">

                        <div id="setup-table"> </div>
                    </div>


                </div>
            </div>





        </div>

    </div>

    @include('admin.includes.popups.setup-popups')
    @include('admin.includes.setups.setups-js')
    <script>getSetupsData('schools');</script>
@endsection