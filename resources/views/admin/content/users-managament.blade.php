@extends('layouts.app-admin')

@section('title', "Users Management")

@section('users_active', "active_nav")

@section('content')

    <div class="manage-content-wrapper">

        <div class="dashboard-heading clearfix">
            <h2>Users Management</h2>
            <ul>
                <li>Energy Zone / Users Management</li>
            </ul>
        </div>

        <div class="content-sorting clearfix">
            @include('alerts.alerts')
            <div class="drop_down_tab_main">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">

                    <li><a href="#setups" role="tab" data-toggle="tab"  >All Users</a></li>




                </ul>
                <!-- Tab panes -->
                <div class="tab-content" id="setups-tab-content">
                    <div class="tab-pane active" id="setups">

                        <div id="setup-table"> </div>
                    </div>


                </div>
            </div>





        </div>

    </div>

    @include('admin.includes.popups.setup-popups')
    @include('admin.includes.setups.setups-js')
    <script>getSetupsData('users');</script>
@endsection