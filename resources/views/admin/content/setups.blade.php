@extends('layouts.app-admin')
@section('title', "Setup")
@section('content')
@section('settings_active', "active_nav")
    <div class="manage-content-wrapper">

        <div class="dashboard-heading clearfix">
            <h2>User Profile Settings</h2>
            <ul>
                <li>Energy Zone / User Profile Settings</li>
            </ul>
        </div>

        <div class="content-sorting clearfix">
            @include('alerts.alerts')
            <div class="drop_down_tab_main">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#setups" role="tab" data-toggle="tab" onclick="getSetupsData('companies')">Companies</a></li>
                    <li><a href="#setups" role="tab" data-toggle="tab"  onclick="getSetupsData('skills')">Skills</a></li>
                    <li><a href="#setups" role="tab" data-toggle="tab"  onclick="getSetupsData('countries')"> Countries</a></li>

                    <li><a href="#setups" role="tab" data-toggle="tab"  onclick="getSetupsData('languages')">Languages</a></li>


                </ul>
                <!-- Tab panes -->
                <div class="tab-content" id="setups-tab-content">
                    <div class="tab-pane active" id="setups">

                        <div id="setup-table"> </div>
                    </div>


                </div>
            </div>





        </div>

    </div>

    @include('admin.includes.popups.setup-popups')
    @include('admin.includes.setups.setups-js')
    <script>getSetupsData('companies');</script>
@endsection