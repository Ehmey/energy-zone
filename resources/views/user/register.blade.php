<!DOCTYPE html>
<html lang="en">
<head>



    <title>{{ config('app.name', 'Energy Zone') }} - SignUp</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">-->
    @include('user.includes.css')


</head>

<body>

<!-- main- taplate start here -->

<div class="main-site-area">

    <section class="ah-signup-wrapper text-center">
        <div class="center-me">
            <div class="container">

                <div class="signUp-flex1">
                    <div class="ah-signUp-heading">

                        <h2>Signup</h2>
                        <div class="text">welcome to enrgy zone find better jobs together.
                        </div>

                    </div>
                </div>

                <div class="signUp-flex2 bg-white">
                    <div class="ah-signUp-content">

                        <div class="logo-wrapper">
                            <a href="{{route('home')}}"><img src="{{asset('energy-zone/user/images/logo.png')}}" alt="logo"></a>
                        </div>

                        <div class="signUp-steps clearfix">
                            <ul class="clearfix">
                                <li>
                                    <div>
                                        <span  @if(empty(app('request')->input('step')) || empty(app('request')->input('id')))
                                               class="active"
                                               @endif
                                               >1</span>
                                        <strong>Step</strong>
                                        <h4>Basic Information</h4>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <span @if(!empty(app('request')->input('step')) || !empty(app('request')->input('id')))
                                            @if(app('request')->input('step') == 2)
                                              class="active"
                                            @endif

                                            @endif>2</span>
                                        <strong>Step</strong>
                                        <h4>Create your Identity</h4>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <span @if(!empty(app('request')->input('step')) || !empty(app('request')->input('id')))
                                              @if(app('request')->input('step') == 3)
                                              class="active"
                                            @endif @endif>3</span>
                                        <strong>Step</strong>
                                        <h4>Authentication</h4>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="signUp-form">
                            @if((empty(app('request')->input('step')) && app('request')->input('step') != 0) || empty(app('request')->input('id')))
                                @include('user.includes.forms.register-form-step-1')
                            @elseif(app('request')->input('step') == 0)
                                @include('user.includes.forms.register-form-step-0-social-login')
                                @elseif(app('request')->input('step') == 2)

                                @include('user.includes.forms.register-form-step-2')
                            @elseif(app('request')->input('step') == 3)
                                @include('user.includes.forms.register-form-step-3')
                            @endif


                        </div>

                    </div>
                </div>

            </div>

            <div class="signUp-copyrights">
                <div class="text">© Copyright Energy Zone all rights reserved</div>
            </div>

        </div>
    </section>

</div>

<!-- main- taplate start here clsed -->







@include('user.includes.js')

</body>

</html>
