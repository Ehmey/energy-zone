@extends('layouts.app')
@section('title', "Edit Profile")
@section('content')
    @include('user.includes.sidebar.edit-profile-sidebar')
    <div class="col-md-8">

        <div class="edit-profile-right-area">

            <div class="edit-profile01 bg-white clearfix">
                <div class="clearfix">
                    <div class="in-left">
                        <div class="ed-profile">
                           <div id="img_preview"> <img  src="@if(Auth::user()->profile_image != null) {{asset(Auth::user()->profile_image)}} @else  {{asset('energy-zone/userIcon.png')}} @endif"
                                 alt="img"></div>
                            <div class="edit-pencil">
                                <label>
                                    <input type="file" id="image_profile" name="image_profile" onchange="editProfileImage()" accept="image/*">
                                    <i class="fas fa-pencil-alt"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="in-right">
                        <h3> <span id="user_name"> {{Auth::user()->name}} </span> <span id="pencil-click"><i onclick="changeName(1)" class="fas fa-pencil-alt"></i></span></h3>
                    </div>
                </div>
                <div class="connection"><a href="{{route('user.public-profile',[Auth::user()->id])}}" style="margin-right:20px;">View Profile</a><a href="{{route('user.connections')}}">Connections</a></div>
            </div>


            @include('user.includes.edit-profile.edit-basic-info-section')


            <div class="edit-profile04">




                @include('user.includes.edit-profile.experince-section')


                @include('user.includes.edit-profile.education-section')

                @include('user.includes.edit-profile.language-section')

                @include('user.includes.edit-profile.skill-section')


            </div>

        </div>
    </div>
    @include('user.includes.edit-profile.edit-profile-js')
@endsection