@extends('layouts.app')
@section('title', "Dashboard")
@section('content')
    @include('user.includes.sidebar.profile-dashboard-sidebar')
    @include('user.includes.developerjs')
    <div class="col-lg-8 col-md-7 col-sm-12">

        <div class="row">
            <div class="col-xs-12">
                <div class="pd_right_sec1">
                    @include('user.includes.forms.post-form')
                </div>
            </div>
            <div class="col-xs-12">
                <ul class="profile_db_comments_ul" id="posts_url">
                    @if($data['posts']->count() == 0)
                    <li id="no_pst" style="text-align:center;"><br><br><img style="margin:0 auto;" src="{{asset('energy-zone/post/no_post.png')}}">
                    <br><br><p class="access_pera">No Posts to Show</p>
                    </li>
                        @endif
                </ul>
            </div>
        </div>
    </div>

    <script>loadposts();</script>
    @include('user.includes.posts.postsJs')
@endsection