@extends('layouts.app')
@section('title', "Connections")
@section('content')
    @include('user.includes.sidebar.connection-sidebar')
    @include('user.includes.developerjs')
    <div class="col-md-8 col-sm-12">
        <div class="connection-right-area">

            <div class="inner-content01 bg-white">
                @if($data['pending_connections']->count() > 0)
                <div class="our-heading"><h3 id="invitation_heading">invitations</h3></div>
                @foreach($data['pending_connections']->take(5) as $connection)
                <div class="clearfix inner-content" id="invitation_{{$connection->user->id}}">

                    <div class="in-left">

                            <img src="{{asset($connection->user->profile_image)}}" alt="profile">

                    </div>
                    <div class="in-right">
                        <div class="text">
                            <a href="{{route('user.public-profile',[$connection->user->id])}}">{{$connection->user->name}}</a>
                        </div>
                        <a class="btn-small" onclick="acceptInvitation({{$connection->user->id}},{{$connection->id}})">Accept</a>
                    </div>
                </div>
                    @endforeach
                    @else
                    <div class="our-heading"><h3>No Pending invitations</h3></div>
                @endif
            </div>

            <div class="our-heading"><h3> People you may know</h3></div>

            <div class="row" id="load_thumbnail">
{{--@if(!empty($data['people_you_may_know']  ==0))--}}
               {{--@foreach($data['people_you_may_know'] as $people)--}}
                {{--<div class="col-md-4 col-sm-6">--}}
                    {{--<div class="common-connection-wrapper bg-white text-center">--}}

                        {{--<div class="profile"><img src="{{asset($people[0]->profile_image)}}" alt="profile"></div>--}}
                        {{--<h3><a href="{{route('user.public-profile',[$people[0]->id])}}">{{$people[0]->name}}</a></h3>--}}
                        {{--<div class="text">{{$people[0]->jobTitle->name}} at {{$people[0]->company->name}}</div>--}}
                        {{--<div><a href="#"><i class="fas fa-users"></i> Zahid and 51 others</a></div>--}}
                        {{--<button class="btn-small">Connect</button>--}}

                    {{--</div>--}}
                {{--</div>--}}

                   {{--@endforeach--}}
    {{--@else--}}
                    {{--<div style="margin-top: 8px;" class="col-md-12"><p class="bg-white" style="padding:10px;"> No More Record to Show</p></div>--}}
    {{--@endif--}}
            </div>

        </div>
    </div>

    @include('user.includes.connections.connections-js')

@endsection