@extends('layouts.app')
@section('title', Auth::user()->name." - Profile")
@section('content')
    @include('user.includes.developerjs')

    @include('user.includes.sidebar.profile-public-sidebar')
    <div class="col-lg-8 col-md-7 col-sm-12">

        <div class="public_view_main">

            <div class="row">

                <div class="col-xs-12">
                    <div class="public_view_sec1">

                    @php 
                    $backgroundPath= 'url("'.asset($data['user']->userInfo->cover_image).'")'
                    @endphp
                        <div class="public_view_img" >
                        <div class="public_p" id="preview"><span id="image_upload_preview" style="width: 100%;height: 250px;background:{{$backgroundPath}};background-size:cover;display:inline-block;"></span>
                        </div> @if(Auth::user()->id == $data['user']->id)
                           <div class="edit-pencil2">
                                <label>

                                <input type="file" id="filePost" name="user_cover_images" onchange="changeCover()" accept="image/*">
                                <i class="fas fa-pencil-alt"></i>

                                </label>
                                <script>
                                    var fileInput = document.getElementById('filePost');
                                    var preview = document.getElementById('preview');

                                    fileInput.addEventListener('change', function (e) {//alert(preview);
                                        var url = URL.createObjectURL(e.target.files[0]);

										bgUrl = `url(`+url+`)`;
                preview.innerHTML = `<span id="image_upload_preview" style="width: 100%;height: 250px;background:`+bgUrl+`;background-size:cover;display:inline-block;" alt="error img">`;
                                    });

                                </script>
                                
                                
                            </div>@endif
                        </div>
                        
                        <div class="public_in_main">
                            <div class="public_content_main">

                            <span class="public_user_img"><img src="{{asset($data['user']->profile_image)}}"
                                                               alt="error img"></span>
                                <h2 class="public_user_name">{{$data['user']->name}} </h2>
                                <p class="public_user_content">{{$data['user']->company->name}}</p>
                                <span class="public_user_state"> {{$data['user']->country->name}}</span>
                                <ul class="public_sec1_ul">
                                    @if(Auth::user()->id != $data['user']->id)
                                    <li>
                                        @if(!empty($data['request_status']))
                                        @if($data['request_status']->status != "Pending" && $data['request_status']->status != "Accepted")
                                            <a id="connect_btn"  onclick="sendRequest({{$data['user']->id}})">Connect</a>
                                            @elseif($data['request_status']->status == "Pending" && $data['request_status']->accept_id != Auth::user()->id && $data['request_status']->send_id == Auth::user()->id)
                                            <a style="background:none !important; box-shadow:none; border: 1px solid #7bb82f;" id="connect_btn" class="clr-green" >Pending</a>
                                            @elseif($data['request_status']->status == "Pending" && $data['request_status']->accept_id == Auth::user()->id)
                                                <a id="connect_btn"  onclick="acceptRequest({{$data['request_status']->id}})">Accept</a>
                                        @elseif($data['request_status']->status == "Accepted")
                                            <a id="connect_btn" >Accepted</a>
                                            {{--@elseif($data['request_status']->send_id == Auth::user()->id && $data['request_status']->status == "Pending")--}}
                                                {{--<a id="connect_btn"  >Accept</a>--}}
                                        @endif

                                            @else

                                            <a id="connect_btn"  onclick="sendRequest({{$data['user']->id}})">Connect</a>
                                            @endif
                                    </li>
                                    @endif
                                    <li><a data-toggle="modal" data-target="#not_found">Message</a></li>
                                    <li><a data-toggle="modal" data-target="#not_found">More</a></li>
                                </ul>
                            </div>
                            <ul class="public_sec1_ul2">
                                <li>
                                <span class="public_icon"><img
                                            src="{{asset('energy-zone/user/assets/images/ogd-icon.png')}}"
                                            alt="error img"></span>
                                    <span class="public_icon_text">OGDCL UAE</span>
                                </li>
                                <li>
                                <span class="public_icon"><img
                                            src="{{asset('energy-zone/user/assets/images/contact_icon.png')}}"
                                            alt="error img"></span>
                                    <span style="cursor: pointer;" class="public_icon_text" data-toggle="modal" data-target="#contact_info" >See contact info</span>
                                </li>
                                <li>
                                <span class="public_icon"><img
                                            src="{{asset('energy-zone/user/assets/images/univ-icon.png')}}"
                                            alt="error img"></span>
                                    <span class="public_icon_text">@if($data['user']->education()->count() !=0) {{$data['user']->education->first()->educationSchool->name}} @else
                                            .... @endif</span>
                                </li>
                                <li>
                                <span class="public_icon"><img
                                            src="{{asset('energy-zone/user/assets/images/connection.png')}}"
                                            alt="error img"></span>
                                    <span class="public_icon_text">connections ({{$data['user']->acceptedConnections()->get()->count()}})</span>
                                </li>
                            </ul>
                            <hr>
                            <div>{{$data['user']->userInfo->description}}</div>
                        </div>
                    </div>
                </div>
                @if(Auth::user()->id != $data['user']->id)
                <div class="col-xs-12">
                    <div class="highligts_sec">
                        <h2 class="highlights_heading">Mutual Friends</h2>
                        <div class="connection_div">
                            <ul class="connection_ul">
                                @if($data['mutual_friends']['mutual_friends_count'] > 0)
                                    @foreach($data['mutual_friends']['mutual_friends']->take(2)->get() as $mutual)
                                <li><img src="{{asset($mutual->user->profile_image)}}"
                                         alt="error img"></li>

                                    @endforeach
                                    @else
                                    <img style="width:50px;" src="{{asset('energy-zone/no-user.png')}}"
                                         alt="error img">
                                    @endif
                            </ul>
                            <div class="connection_right_text">
                                <h3 class="mutual_text">{{$data['mutual_friends']['mutual_friends_count'] }} Mutual Connections</h3>
                                <span class="mutual_pera">{{$data['mutual_friends']['mutual_friends_public_profile'] }}</span>
                            </div>
                        </div>
                    </div>
                </div>
@endif

                @include('user.includes.public-profile.experience-public-profile-section')


                @include('user.includes.public-profile.education-public-profile-section')


                <div class="col-xs-12">
                    <div class="ah-main-skills-wrapper">

                       <div  class="ah-skills-01 bg-white">
                        <div class="our-heading"><h3>Skills & Endorsements</h3></div>
                        <ul  >
                            @if($data['user']->skills()->count() !=0)
                                @foreach($data['user']->skills as $skill)
                                    <li>
                                        <h3>{{$skill->name}}
                                            <small>.32</small>
                                        </h3>
                                        <div class="text">See 32 endorsements for Supply Chain Management32</div>
                                    </li>


                                @endforeach

                            <!-- <li class="text-center">
                                        <a href="#">See more</a>
                                    </li>-->

                            @else
                                <li style="padding: 20px 25px;">      No Skills Added till yet.</li>
                            @endif


                        </ul>

                        @if($data['user']->skills()->count() > 3)
                            <div class="see_more_main text-center">
                                <a href="javascript:void(0)" id="see_more">See more</a>
                            </div>
                        @endif

                    </div>
                    </div>
                        <div class="ah-main-skills-wrapper">
                        <div class="ah-skills-02 bg-white">
                            <div class="our-heading"><h3>Interests</h3></div>
                            <div class="clearfix">
                                <ul>
                                    <li class="clearfix">
                                        <div class="in-left">
                                            <img src="{{asset('energy-zone/user/assets/images/dummy-img.png')}}"
                                                 alt="img">
                                        </div>
                                        <div class="in-right">
                                            <h3>Buyer-World EUROPE - NORTH ...</h3>
                                            <div class="text">23,000 Members</div>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="in-left">
                                            <img src="{{asset('energy-zone/user/assets/images/dummy-img.png')}}"
                                                 alt="img">
                                        </div>
                                        <div class="in-right">
                                            <h3>Buyer-World EUROPE - NORTH ...</h3>
                                            <div class="text">23,000 Members</div>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="in-left">
                                            <img src="{{asset('energy-zone/user/assets/images/dummy-img.png')}}"
                                                 alt="img">
                                        </div>
                                        <div class="in-right">
                                            <h3>Buyer-World EUROPE - NORTH ...</h3>
                                            <div class="text">23,000 Members</div>
                                        </div>
                                    </li>
                                </ul>
                                <ul>
                                    <li class="clearfix">
                                        <div class="in-left">
                                            <img src="{{asset('energy-zone/user/assets/images/dummy-img.png')}}"
                                                 alt="img">
                                        </div>
                                        <div class="in-right">
                                            <h3>Buyer-World EUROPE - NORTH ...</h3>
                                            <div class="text">23,000 Members</div>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="in-left">
                                            <img src="{{asset('energy-zone/user/assets/images/dummy-img.png')}}"
                                                 alt="img">
                                        </div>
                                        <div class="in-right">
                                            <h3>Buyer-World EUROPE - NORTH ...</h3>
                                            <div class="text">23,000 Members</div>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="in-left">
                                            <img src="{{asset('energy-zone/user/assets/images/dummy-img.png')}}"
                                                 alt="img">
                                        </div>
                                        <div class="in-right">
                                            <h3>Buyer-World EUROPE - NORTH ...</h3>
                                            <div class="text">23,000 Members</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="seemorebutton text-center"><a href="#">See all</a></div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="contact_info" role="dialog">
        <div class="general_popup_in">
            <span class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></span>
            <h1 class="general_popup_heading">Contact Info</h1>
            <div class="popup_white_sec">

                <ul class="popup_ul">

                    <li>
                        <label>Email</label>
                        <label>{{$data['user']->email}}</label>
                    </li>
                </ul>

            </div>
        </div>
    </div>

@include('user.includes.public-profile.public-profile-js')
@endsection