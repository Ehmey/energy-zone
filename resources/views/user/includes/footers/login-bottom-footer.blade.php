<ul class="landing_social_media">
    <li><a href="https://twitter.com/?lang=en" _blank><i class="fab fa-twitter"></i></a></li>
    <li><a href="https://www.instagram.com/" _blank><i class="fab fa-instagram"></i></a></li>
    <li><a href="https://www.facebook.com/" _blank><i class="fab fa-facebook-f"></i></a></li>
    <li><a href="https://www.youtube.com/" _blank><i class="fab fa-youtube"></i></a></li>
</ul>
<p class="copy_right_text">© Copyright Energy Zone all rights reserved
</p>
