<script>


    function postReplyOnKeYdDown(commentId, event){
        if(event.keyCode == 13){ postReply(commentId);}
    }


    function loadPostReply(commentId) {
        postHtml = `<div class="coment_white_sec">
            <input type="text" id="reply_input_` + commentId + `" onkeydown="postReplyOnKeYdDown(`+commentId+`,event)"  placeholder="Add a comment" class="ad_coment_text" id="comment_input_id_1">
            <div class="browse_coment_main">
                <label class="file_upload_btn2">
                    <input name="myFile"   type="file"><span><i class="fas fa-camera" data-toggle="modal" data-target="#not_found"></i> </span>
                </label>
                <input type="button" onclick="postReply(` + commentId + `)" value="Comment" class="post_com_btn">
            </div>
        </div>`;
        var node = postHtml;
        document.getElementById('reply_id_' + commentId).innerHTML = document.getElementById('reply_id_' + commentId).innerHTML + postHtml;

    }


    function postReply(commentId) {

        baseUrl = "{{url('user/comment/')}}" + "/" + commentId + "/reply/post?reply=" + document.getElementById('reply_input_' + commentId).value;


        if(document.getElementById('reply_input_' + commentId).value == ""){ return;}
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                data = JSON.parse(this.responseText);
                document.getElementById('comment_replies_' + commentId).innerHTML = data[0].comments_count;
                loadReply(commentId, 'orderf_asc');
                console.log(data);
            }
        }
        xhttp.open("GET", baseUrl, true);
        xhttp.send();
    }


    function deleteReply(replyId, commentId) {
        document.getElementById("reply_box_" + replyId).remove();
        baseUrl = "{{url('user/reply/')}}" + "/" + replyId + "/delete";
        // alert(baseUrl);
        innerHtml = "";
        document.getElementById('comment_replies_' + commentId).innerHTML = document.getElementById('comment_replies_' + commentId).innerHTML - 1;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                data = JSON.parse(this.responseText);

            }
        }
        xhttp.open("GET", baseUrl, true);
        xhttp.send();
    }


    replyResult = 0;

    function loadReply(commentId, order = null) {
        if(replyResult !=0) {return;}
        replyResult =1;
        baseUrl = "{{url('user/comment/')}}" + "/" + commentId + "/reply";
        if (order != null) {
            baseUrl = "{{url('user/comment/')}}" + "/" + commentId + "/reply?order=asc";
        }
        // alert(baseUrl);
        innerHtmlReply = "";
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                data = JSON.parse(this.responseText);
                if (data[0] != null) {
                    console.log(data[0]);
                    for (i = 0; i < data[0].replies.length; i++) {


                        ulToEmbed = `<ul class="drop_down_main2">
                                <li><span>` + data[0].replies[i].created_at + `</span></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle drop_down_anker2" data-toggle="dropdown" aria-expanded="true">
                                        <i class="fas fa-ellipsis-h"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#not_found"><i class="fas fa-pencil-alt"></i>
                                    &nbsp;&nbsp;        Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a onclick="deleteReply(` + data[0].replies[i].reply_id + `,` + commentId + `)"><i class="fas fa-trash-alt"></i> &nbsp;&nbsp;Delete</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>`;
                        if (data[0].replies[i].self_status != "mine") {
                            ulToEmbed = "";
                        }
                        innerHtmlReply = innerHtmlReply + `

                        <div class="clearfix a" id="reply_box_` + data[0].replies[i].reply_id + `">
                        ` + ulToEmbed + `
                            <div class="in-left">
                                <img  src="` + data[0].replies[i].profile_image + `" >
                            </div>
                            <div class="in-right">
                                <h3>` + data[0].replies[i].user_name + `</h3>
                                <div class="text">
                                   ` + data[0].replies[i].reply + `
                                </div>
                                <ul class="clearfix">
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#not_found">Like
                                            <small>(0)</small>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="loadReply(` + commentId + `)">Reply

                                        </a>
                                    </li>
                                </ul>
                            </div>
</div>
                       `;
                        // innerHtml ="hello";
                    }
                    document.getElementById('reply_id_' + commentId).innerHTML = innerHtmlReply;
                    loadPostReply(commentId);
                    document.getElementById('reply_id_' + commentId).style.display = "block";
                }
                else {
                    document.getElementById('reply_id_' + commentId).innerHTML = "";
                    loadPostReply(commentId);
                    document.getElementById('reply_id_' + commentId).style.display = "block";
                }
                replyResult =0;
            }

        }
        xhttp.open("GET", baseUrl, true);
        xhttp.send();


    }


    function addLike(postId, userId) {
        baseUrl = "{{url('user/post')}}" + "/" + postId + "/like?user_id=" + userId;
        //alert(baseUrl);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                data = JSON.parse(this.responseText);

                document.getElementById("like_count_post_" + postId).innerHTML = data.likes_count;
                if (data.status == 'like') {
                    console.log('like');

                    document.getElementById("like_status_post_" + postId).style.color = "blue";
                    document.getElementById("like_thumb_post_" + postId).style.color = "blue";
                } else {
                    document.getElementById("like_thumb_post_" + postId).style.color = "grey";
                    document.getElementById("like_status_post_" + postId).style.color = "grey";
                }
                console.log(this.responseText);
            }

        }
        xhttp.open("GET", baseUrl, true);
        xhttp.send();
    }


    function deleteComment(commentId) {
        deleteUrl = "{{url('user/post/comment/')}}" + "/" + commentId + "/delete";

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                data = JSON.parse(this.responseText);

                document.getElementById("comments_box_" + commentId).remove();
                document.getElementById("comment_action_ul_" + commentId).remove();
                document.getElementById("comment_count_post" + data.post_id).innerHTML = data.comments_count;
                console.log(this.responseText);
            }

        }
        xhttp.open("GET", deleteUrl, true);
        xhttp.send();

    }

    function postComment(postId, userId) {

        commentInput = document.getElementById("comment_input_id_" + postId).value;
        if(commentInput == ""){return;}
        baseUrl = "{{url('user/post')}}" + "/" + postId + "/comment?comment=" + commentInput + "&user_id=" + userId;

        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {

                commentHtml = "";


                data = JSON.parse(this.responseText);
                document.getElementById('comment_count_post' + postId).innerHTML = data.comments_count;
                console.log(data);

                loadComments(postId, userId);


            }
        };
        xhttp.open("GET", baseUrl, true);
        xhttp.send();
        document.getElementById("comment_input_id_" + postId).value = "";

    }


    function loadComments(postId, userId) {

        commentInput = document.getElementById("comment_input_id_" + postId).value;
        baseUrl = "{{url('user/post')}}" + "/" + postId + "/load-comment?comment=" + commentInput + "&user_id=" + userId;

        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                commentHtml = "";

                data = JSON.parse(this.responseText);
                console.log(data);
                //
                if (data[0] != null) {
                    console.log(data[0].comments);
                    for (i = 0; i < data[0].comments.length; i++) {
                        commentId = data[0].comments[i].comment_id;
                        deleteUrl = "{{url('user/post/comment/')}}" + "/" + data[0].comments[i].comment_id + "/delete";
                        commentHtml = commentHtml + ` <ul class="drop_down_main2" id="comment_action_ul_` + commentId + `">
                                            <li><span>` + data[0].comments[i].created_at + `</span></li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle drop_down_anker2"
                                                   data-toggle="dropdown" aria-expanded="true"><i
                                                            class="fas fa-ellipsis-h"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" data-toggle="modal" data-target="#not_found"><i class="fas fa-pencil-alt"></i>
                                                            &nbsp;&nbsp;Edit</a></li>
                                                    <li><a onclick="deleteComment(` + commentId + `)"  ><i class="fas fa-trash-alt"></i> &nbsp;&nbsp;Delete  </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <div class="clearfix_comment" id="comments_box_` + data[0].comments[i].comment_id + `">
                                        <div class="in-left"><img
                                                    src="` + data[0].comments[i].user_image + `">
                                        </div>
                                        <div class="in-right">
                                            <h3>` + data[0].comments[i].user_name + `</h3>
                                            <div class="text">` + data[0].comments[i].comment + `
                                            </div>
                                            <ul class="clearfix">
                                                <li><a onclick="likeComment(`+data[0].comments[i].comment_id+`)" data-toggle="modal" data-target="#not_found"><span id="comment_like_`+data[0].comments[i].comment_id+`">Like</span>
                                                        <small id="comment_like_count_`+data[0].comments[i].comment_id+`">(0)</small>
                                                    </a></li>
                                                <li><a onclick="loadReply(` + commentId + `)">Reply
                                                        <small >(<span id="comment_replies_` + commentId + `">` + data[0].comments[i].replies_count + `</span>)</small>
                                                    </a></li>
                                            </ul>
                                        </div>

  <div class="commentshow2 clearfix" style="display:none;"  id="reply_id_` + commentId + `">
  </div>

                </div>

                                    </div>`;
                    }
                    if (commentHtml != "") {
                        document.getElementById('post_comments_' + postId).innerHTML = commentHtml;
                        document.getElementById('post_comments_' + postId).style.display = "block";
                    } else {
                        document.getElementById('post_comments_' + postId).style.display = "none";
                    }

                }
            }
        };
        xhttp.open("GET", baseUrl, true);
        xhttp.send();
    }


         function changeCover() {
        //  alert(12);

        var formData = new FormData(this);

        formData.append('user_cover_images', $('input[type=file]')[0].files[0]);
        baseUrl = " {{(url('user/edit/cover-image'))}}";

        $.ajax({
            type: 'POST',
            url: baseUrl,
            data: formData,
            headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},

            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {



            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    }


    $(document).ready(function(e) {
        $('#see_more').on('click',function(){
            $('.ah-main-skills-wrapper .ah-skills-01 ul li:hidden').slice(0,3).show(500)
        });

        $('#see_more2').on('click',function(){
            $('.onlyfor_toggleli li:hidden').slice(0,3).show(500)
        });


    });

</script>