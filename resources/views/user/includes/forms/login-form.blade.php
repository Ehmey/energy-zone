@include('alerts.alerts')

<form method="post" action="{{route('login')}}">
    @csrf
    <input type="email" name="email" placeholder="Email" class="mail_field">

    @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
    @endif
    <input type="password" name="password" placeholder="Password" class="mail_field">
    @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
    @endif
    <div class="forget-main">


    <label class="check">
        <input type="checkbox" class="chk" name="remember">
        <span class="checkmark"></span>Remember me
    </label>
    <a class="forgot_pas" href="{{ route('password.request') }}">
        {{ __('Forgot Your Password?') }}
    </a>
    </div>

    <span class="login_btn"><input type="submit" value="Login"></span>
</form>
