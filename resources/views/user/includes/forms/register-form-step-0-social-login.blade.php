@include('alerts.alerts')
<form method="post" action="{{route('user.for-social-login-update-info')}}" enctype='multipart/form-data'>
    @csrf

    <input name="step" value="1" type="text" style="display:none;">
    <input name="social" value="1" type="text" style="display:none;">
    <input name="user_id"value="{{$data['user']->id}}" type="text" style="display:none;">
    <input type="text" name="name" value="{{$data['user']->name}}" placeholder="Name" required readonly>
    @if ($errors->has('name'))
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
    @endif

    <input type="email" name="email" placeholder="Email" value="{{$data['user']->email}}" required readonly>
    @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
    @endif
    <input type="password" name="password" placeholder="Password" required>
    @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
    @endif

    <div class="site-btn">
        <button type="submit">Next</button>
    </div>
</form>
