@include('alerts.alerts')

<form method="post" action="{{route('user.step')}}" enctype='multipart/form-data'>
    @csrf
    <input value="2" type="text" name="step" value="2" style="display: none;">
    <input type="text" name="user_id" value="{{app('request')->input('id')}}" style="display: none;">
    <div class="off-apperance">
        <select name="country_id" required>
            @foreach($data['countries'] as $country)
                <option value="{{$country->id}}">{{$country->name}}</option>
                @endforeach
        </select>
    </div>
    <div class="off-apperance">
        <select name="company_id" required>
            @foreach($data['companies'] as $company)
                <option value="{{$company->id}}">{{$company->name}}</option>
                @endforeach
        </select>
    </div>
    <div class="off-apperance">
        <select name="job_title_id" required>

            @foreach($data['job_titles'] as $job_title)
                <option value="{{$job_title->id}}">{{$job_title->name}}</option>
                @endforeach
        </select>
    </div>
    <div class="site-btn">
        <button type="submit">Next</button>
    </div>
</form>
