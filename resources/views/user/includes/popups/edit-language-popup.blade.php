<div class="modal fade" id="editLanguage" role="dialog">
    <div class="general_popup_in">
        <span class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></span>
        <h1 class="general_popup_heading">Add Skill</h1>
        <div class="popup_white_sec">
            <form method="post" action="{{route('user.add.language')}}">
                @csrf
                <ul class="popup_ul">

                    <li>
                        <label>Add Language</label>
                        <select name="language_id">
                            @foreach($data['all_languages'] as $language)
                                <option value="{{$language->id}}">{{$language->name}}</option>
                                @endforeach

                        </select>
                    </li>

                    <li>
                        <label>Proficency</label>
                        <select name="proficency">
                            <option value="Fluent">Fluent</option>
                            <option value="Native">Native</option>
                        </select>
                    </li>

                </ul>
                <input type="submit" value="Save" class="popup_save_btn">
            </form>
        </div>

    </div>
</div>