<div class="modal fade" id="addEducation" role="dialog">
    <div class="general_popup_in">
        <span class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></span>
        <h1 class="general_popup_heading">Add Education</h1>
        <div class="popup_white_sec">
            <form method="post" action="{{route('user.add.education')}}">
                @csrf
                <ul class="popup_ul">

                    <li>
                        <label>School</label>
                        <select name="school_id">

                            @foreach($data['schools'] as $school)
                                <option value="{{$school->id}}" >{{$school->name}} </option>
                            @endforeach
                        </select>
                    </li>

                    <li>
                        <label>Degree</label>
                        <select name="degree_id">
                            @foreach($data['degrees'] as $degree)
                                <option value="{{$degree->id}}">{{$degree->name}}</option>
                            @endforeach
                        </select>
                    </li>

                    <li>
                        <label>Activities and societies</label>
                        <textarea name="activities" required></textarea>
                    </li>

                </ul>
                <input type="submit" value="Save" class="popup_save_btn">
            </form>
        </div>

    </div>
</div>