<div class="modal fade" id="addExperince" role="dialog">
    <div class="general_popup_in">
        <span class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></span>
        <h1 class="general_popup_heading">Add Experince</h1>
        <div class="popup_white_sec">
            <form method="post" action="{{route('user.add.experince')}}">
                @csrf
                <ul class="popup_ul">

                    <li>
                        <label>Company</label>
                        <select name="company_id">

                            @foreach($data['companies'] as $company)
                                <option value="{{$company->id}}" >{{$company->name}} </option>
                            @endforeach
                        </select>
                    </li>

                    <li>
                        <label>Degree</label>
                        <select name="job_title_id">
                            @foreach($data['job_title'] as $job_title)
                                <option value="{{$job_title->id}}">{{$job_title->name}}</option>
                            @endforeach
                        </select>
                    </li>

                    <li>
                        <label>How was your Experince?</label>
                        <textarea name="experince" required></textarea>
                    </li>

                </ul>
                <input type="submit" value="Save" class="popup_save_btn">
            </form>
        </div>

    </div>
</div>