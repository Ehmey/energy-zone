<div class="modal fade" id="editEducation_{{$education->id}}" role="dialog">
    <div class="general_popup_in">
        <span class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></span>
        <h1 class="general_popup_heading">Edit Education</h1>
        <div class="popup_white_sec">
            <form method="post" action="{{route('user.edit.education')}}">
                <input style="display:none" name="user_education_id" value="{{$education->id}}">
                @csrf
                <ul class="popup_ul">

                    <li>
                        <label>School</label>
                        <select name="school_id">

                            @foreach($data['schools'] as $school)
                                <option value="{{$school->id}}" @if($school->id == $education->educationSchool->id) selected @endif>{{$school->name}}</option>
                            @endforeach
                        </select>
                    </li>

                    <li>
                        <label>Degree</label>
                        <select name="degree_id">
                            @foreach($data['degrees'] as $degree)
                                <option value="{{$degree->id}}" @if($degree->id == $education->educationDegree->id) selected @endif>{{$degree->name}}  </option>
                            @endforeach
                        </select>
                    </li>

                    <li>
                        <label>Activities and societies</label>
                        <textarea name="activities" required @if($degree->id == $education->educationDegree->id) selected @endif>{{$education->activities}}</textarea>
                    </li>

                </ul>
                <input type="submit" value="Save" class="popup_save_btn">
            </form>
        </div>

    </div>
</div>