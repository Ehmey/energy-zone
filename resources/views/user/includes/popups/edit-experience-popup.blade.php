<div class="modal fade" id="editExperince_{{$experince->id}}" role="dialog">
    <div class="general_popup_in">
        <span class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></span>
        <h1 class="general_popup_heading">Edit Experience</h1>
        <div class="popup_white_sec">
            <form method="post" action="{{route('user.edit.experience')}}">
                <input style="display:none" name="user_experience_id" value="{{$experince->id}}">
                @csrf
                <ul class="popup_ul">

                    <li>
                        <label>Company</label>
                        <select name="company_id">

                            @foreach($data['companies'] as $company)
                                <option value="{{$company->id}}" @if($company->id == $experince->experinceCompany->id) selected @endif>{{$company->name}}</option>
                            @endforeach
                        </select>
                    </li>

                    <li>
                        <label>Job Title</label>
                        <select name="job_title_id">
                            @foreach($data['job_title'] as $job_title)
                                <option value="{{$job_title->id}}" @if($job_title->id == $experince->experinceJobTitle->id) selected @endif>{{$job_title->name}}  </option>
                            @endforeach
                        </select>
                    </li>

                    <li>
                        <label>How was your experince?</label>
                        <textarea name="experience" required  >{{$experince->experince}}</textarea>
                    </li>

                </ul>
                <input type="submit" value="Save" class="popup_save_btn">
            </form>
        </div>

    </div>
</div>