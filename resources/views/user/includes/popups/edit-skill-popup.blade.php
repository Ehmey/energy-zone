<div class="modal fade" id="editSkill" role="dialog">
    <div class="general_popup_in">
        <span class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></span>
        <h1 class="general_popup_heading">Add Skill</h1>
        <div class="popup_white_sec">
            <form method="post" action="{{route('user.add.skill')}}">
                @csrf
                <ul class="popup_ul">

                    <li>
                        <label>Add Skill</label>
                        <select name="skill_id">
                            @foreach($data['all_skills'] as $skill)
                                <option value="{{$skill->id}}">{{$skill->name}}</option>
                                @endforeach

                        </select>
                    </li>


                </ul>
                <input type="submit" value="Save" class="popup_save_btn">
            </form>
        </div>

    </div>
</div>