<div class="col-lg-4 col-md-5 col-sm-12">
    <div class="ah-veiw-left-area">
        <div class="ah-view-01 bg-white">
            <div class="our-heading"><h3>People Also Viewed</h3></div>
            <div class="inner-profile clearfix">
                <div class="in-left">
                    <img src="{{asset('energy-zone/user/assets/images/small-profile.png')}}" alt="img">
                </div>
                <div class="in-right">
                    <h4>Jhone Doe . <small>2nd</small></h4>
                    <div class="text">Member at National Youth Assembly, Visiting Faculty Member at GCUF</div>
                </div>
            </div>
            <div class="inner-profile clearfix">
                <div class="in-left">
                    <img src="{{asset('energy-zone/user/assets/images/small-profile.png')}}" alt="img">
                </div>
                <div class="in-right">
                    <h4>Jhone Doe . <small>2nd</small></h4>
                    <div class="text">Member at National Youth Assembly, Visiting Faculty Member at GCUF</div>
                </div>
            </div>
            <div class="inner-profile clearfix">
                <div class="in-left">
                    <img src="{{asset('energy-zone/user/assets/images/small-profile.png')}}" alt="img">
                </div>
                <div class="in-right">
                    <h4>Jhone Doe . <small>2nd</small></h4>
                    <div class="text">Member at National Youth Assembly, Visiting Faculty Member at GCUF</div>
                </div>
            </div>
            <div class="inner-profile clearfix">
                <div class="in-left">
                    <img src="{{asset('energy-zone/user/assets/images/small-profile.png')}}" alt="img">
                </div>
                <div class="in-right">
                    <h4>Jhone Doe . <small>2nd</small></h4>
                    <div class="text">Member at National Youth Assembly, Visiting Faculty Member at GCUF</div>
                </div>
            </div>
            <div class="inner-profile clearfix">
                <div class="in-left">
                    <img src="{{asset('energy-zone/user/assets/images/small-profile.png')}}" alt="img">
                </div>
                <div class="in-right">
                    <h4>Jhone Doe . <small>2nd</small></h4>
                    <div class="text">Member at National Youth Assembly, Visiting Faculty Member at GCUF</div>
                </div>
            </div>
            <div class="inner-profile clearfix">
                <div class="in-left">
                    <img src="{{asset('energy-zone/user/assets/images/small-profile.png')}}" alt="img">
                </div>
                <div class="in-right">
                    <h4>Jhone Doe . <small>2nd</small></h4>
                    <div class="text">Member at National Youth Assembly, Visiting Faculty Member at GCUF</div>
                </div>
            </div>
        </div>


        {{--<div class="ah-view-02 bg-white">--}}
            {{--<div class="our-heading"><h3>Learn the skills Mohsina has</h3></div>--}}
            {{--<div class="inner-profile clearfix">--}}
                {{--<div class="in-left">--}}
                    {{--<img src="{{asset('energy-zone/user/assets/images/small-video-thumb.png')}}" alt="img">--}}
                    {{--<i class="fas fa-play"></i>--}}
                {{--</div>--}}
                {{--<div class="in-right">--}}
                    {{--<h4>AutoCAD 2016 Essential <br>Training</h4>--}}
                    {{--<div class="text">Viewers: 24,780</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="inner-profile clearfix">--}}
                {{--<div class="in-left">--}}
                    {{--<img src="{{asset('energy-zone/user/assets/images/small-video-thumb.png')}}" alt="img">--}}
                    {{--<i class="fas fa-play"></i>--}}
                {{--</div>--}}
                {{--<div class="in-right">--}}
                    {{--<h4>AutoCAD 2016 Essential <br>Training</h4>--}}
                    {{--<div class="text">Viewers: 24,780</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="inner-profile clearfix">--}}
                {{--<div class="in-left">--}}
                    {{--<img src="{{asset('energy-zone/user/assets/images/small-video-thumb.png')}}" alt="img">--}}
                    {{--<i class="fas fa-play"></i>--}}
                {{--</div>--}}
                {{--<div class="in-right">--}}
                    {{--<h4>AutoCAD 2016 Essential <br>Training</h4>--}}
                    {{--<div class="text">Viewers: 24,780</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="inner-profile clearfix">--}}
                {{--<div class="in-left">--}}
                    {{--<img src="{{asset('energy-zone/user/assets/images/small-video-thumb.png')}}" alt="img">--}}
                    {{--<i class="fas fa-play"></i>--}}
                {{--</div>--}}
                {{--<div class="in-right">--}}
                    {{--<h4>AutoCAD 2016 Essential <br>Training</h4>--}}
                    {{--<div class="text">Viewers: 24,780</div>--}}
                {{--</div>--}}
                {{--<a href="#" class="feed_view_All2">View all recommendations</a>--}}
            {{--</div>--}}
        {{--</div>--}}



        <div class="">
            <div class="upgrade_sec">
                <p class="access_pera">Access exclusive tools &amp; insights Free Upgrade to Premium</p>
                <a data-toggle="modal" data-target="#not_found" class="upgrade_btn">Upgrade</a>
            </div>
        </div>
        <div class="">
            <div class="suggestion_tab_main">
                <span class="suggestion_text">Add your feed <i class="fas fa-info-circle"></i></span>
                <ul class="ad_your_feed">

                    @foreach($recent_users['user'] as $recent_user)
                        @if(isset($recent_user[0]->JobTitle->name))
                            <li>
                                <div class="ad_fed_left">
                                    <span class="feed_img"><img src="{{asset($recent_user[0]->profile_image)}}"
                                                                alt="error img"></span>
                                    <div class="feed_right">
                                        <span class="fed_nam">{{$recent_user[0]->name}}</span>
                                        <span class="chairman_oil">{{$recent_user[0]->JobTitle->name}}
                                            , {{$recent_user[0]->company->name}}</span>
                                    </div>
                                </div>
                                <a data-toggle="modal" data-target="#not_found" class="feed_follow_btn">+ Follow</a>
                            </li>
                        @endif
                    @endforeach


                </ul>
                <a data-toggle="modal" data-target="#not_found" class="feed_view_All">View all recommendations</a>
            </div>
        </div>
    </div>
</div>