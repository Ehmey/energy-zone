<div class="col-md-4 col-sm-12">
    <div class="connection-left-area">

        <div class="connetion-wrapper01 bg-white text-center">
            <h2 id="connection_count">{{$data['acceptedConnections']->count()}}</h2>
            <div class="text">Your connections</div>
            @if($data['acceptedConnections']->count() != 0)
            <a data-toggle="modal" data-target="#not_found" >See all</a>
            <ul>
                @if($data['acceptedConnections']->count() > 0)

                @foreach($data['acceptedConnections']->orderBy('id','desc')->take(3)->get() as $accepted)
                        <li><a data-toggle="modal" data-target="#not_found" ><img src="{{asset($accepted->user->profile_image)}}" alt="profile"></a></li>
                    @endforeach
                @endif


            </ul>
                @endif
        </div>

        <div class="connetion-wrapper02 bg-white text-center">
            <div class="text">Your contact import is ready Connect with your contacts and never lose touch</div>
            <div><button class="btn-small" data-toggle="modal" data-target="#not_found" >Contiune</button></div>
            <div><a  data-toggle="modal" data-target="#not_found"  >More options</a></div>
        </div>

    </div>
</div>