<div class="col-xs-12">
    <div class="highligts_sec">
        <h2 class="highlights_heading">Experience</h2>
        <ul class="experience_ul toggle_exp_ul">
            @if(Auth::user()->experince()->count() !=0)
                @foreach($data['user']->experince as $experience)
                    <li>
                            <span class="experience_img"><img
                                        src="{{asset($experience->experinceCompany->image_path)}}"
                                        alt="error img"></span>
                        <div class="experience_right_content">
                            <span class="experience_title">{{$experience->experinceCompany->name}}</span>
                            <span class="experience_descreption">{{$experience->experinceJobTitle->name}}</span>
                            <span class="experience_descreption"> {{$experience->experince}} </span>
                            {{--<span class="experience_descreption">Sep-2-2008 - Sep-2-2008</span>--}}
                        </div>
                    </li>
                @endforeach
            @else
                No Data till yet.
            @endif
        </ul>
        @if($data['user']->experince()->count() > 3)
            <div class="see_more_main text-center" style="background:#fff;">
                <a href="javascript:void(0)" id="see_more3">See more</a>
            </div>
        @endif
    </div>
</div>