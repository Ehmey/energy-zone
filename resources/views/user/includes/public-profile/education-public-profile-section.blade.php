<div class="col-xs-12">
    <div class="highligts_sec">
        <h2 class="highlights_heading">Education</h2>

        <ul class="experience_ul onlyfor_toggleli_education">

            @if(Auth::user()->education()->count() !=0)
                @foreach($data['user']->education as $education)
                    <li>
                                <span class="experience_img"><img
                                            src="{{asset('energy-zone/user/assets/images/experience-icon.png')}}"
                                            alt="error img"></span>
                        <div class="experience_right_content">
                            <span class="experience_title">{{$education->educationSchool->name}}</span>

                            <span class="experience_descreption">{{$education->educationDegree->name}} </span>
                            <span class="experience_descreption">{{$education->activities}} </span>
                            <span class="experience_descreption">{{$education->_from}} - {{$education->_to}} </span>
                        </div>
                    </li>
                @endforeach
            @else
                No Data till yet.
            @endif
        </ul>

        @if($data['user']->skills()->count() > 3)
            <div class="see_more_main text-center" style="background:#fff;">
                <a href="javascript:void(0)" id="see_more2">See more</a>
            </div>
        @endif

    </div>
</div>