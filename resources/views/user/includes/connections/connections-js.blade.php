<script>
    totalCountPeopleYouKnow = 0;
    function loadPeopleYouMyKnow(){

        if(document.getElementById('connection_no_see_more')){document.getElementById('connection_no_see_more').remove();}

    document.getElementById('load_thumbnail').innerHTML= document.getElementById('load_thumbnail').innerHTML+`<div class="col-xs-12 col-md-12" id="loading"><div class="see_more_main text-center" style="margin:20px 0px 10px 0px;background:white;">
                <a  >Loading</a>
            </div></div>`  ;



        baseUrl = `{{url("user/people-you-may-know")}}`+'/'+totalCountPeopleYouKnow;
        thumNails=document.getElementById('load_thumbnail').innerHTML;
        $.get(baseUrl, function (data, status) {

            if(document.getElementById('loading')){document.getElementById('loading').remove();}
            var thumNails=document.getElementById('load_thumbnail').innerHTML  ;

            for(i=0 ; i<data.users.length ;i++){
                thumNails = thumNails +`<div id="thumbnail_`+data.users[i].user_id+`" class="col-md-4 col-sm-6">
                    <div class="common-connection-wrapper bg-white text-center">

                        <div class="profile"><img src="`+data.users[i].profile_image+`" alt="profile"></div>
                        <h3><a href="`+data.users[i].user_profile_url+`">`+data.users[i].name+`</a></h3>
                        <div class="text">`+data.users[i].job_at+`</div>
                        <!--<div><a href=""><i class="fas fa-users"></i> Zahid and 51 others</a></div>-->
                        <br>
                        <button class="btn-small" onclick="sendRequestFromConnections(`+data.users[i].user_id+`)">Connect</button>

                    </div>
                </div>`;
            }

            totalCountPeopleYouKnow = totalCountPeopleYouKnow + 12;
            noSeeMore = "";
            if(data.see_more != 'no_see_more'){
                noSeeMore = `<div class="col-xs-12 col-md-12" id="connection_no_see_more">
                <div class="see_more_main text-center"  style="margin:20px 0px 10px 0px;background:white;">
                <a href="javascript:void(0)" onclick="loadPeopleYouMyKnow()">See more</a>
            </div>
  </div>

           `;
            }
            document.getElementById('load_thumbnail').innerHTML = thumNails +noSeeMore;
            if(data.length == 0){
                document.getElementById('load_thumbnail').innerHTML =`<div style="margin-top: 8px;" class="col-md-12"><p class="bg-white" style="padding:10px;"> No More Record to Show</p></div>`;
            }


        });
    }



    function sendRequestFromConnections(userId){
        document.getElementById('thumbnail_'+userId).remove();

        baseUrl = `{{url("user/")}}`+'/'+userId+`/send-request`;

        $.get(baseUrl, function (data, status) {


        });
    }





    function acceptInvitation(id, connectionId){

document.getElementById('invitation_'+id).remove();

        baseUrl = `{{url("user/accept_invitation")}}`+'/'+connectionId;

        $.get(baseUrl, function (data, status) {
            if(data.count == 0){
                document.getElementById('invitation_heading').innerHTML = "No Pending Invitations";

            }
            document.getElementById('connection_count').innerHTML = data.accepted_connections;
            console.log(data);
        });
    }

    loadPeopleYouMyKnow();
</script>