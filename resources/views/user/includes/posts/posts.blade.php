
<li>
    <div class="coment_sec1">
        <div class="coment_sec1_left">
                                <span class="coment_img"><img
                                            src="@if($post->user->profile_image != null) {{$post->user->profile_image}} @else {{asset('energy-zone/userIcon.png')}} @endif "
                                            alt="error img"></span>
            <a href="{{route('user.public-profile',['id'=>$post->user_id])}}">
                <div class="coment_content_right">
                    <span class="post_name">{{$post->user->name}}</span>
                    <p class="post_date">{{ $post->created_at->diffForHumans()}}</p>
                </div>
            </a>
        </div>
        <ul class="drop_down_main2">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle drop_down_anker2" data-toggle="dropdown"
                   aria-expanded="false"><i class="fas fa-ellipsis-h"></i></a>
                <ul class="dropdown-menu">
                    @if($post->user_id == Auth::user()->id)
                        <li><a href="{{route('user.delete.post',['id'=>$post->id])}}">Delete Post</a></li>
                        <li><a href="{{route('user.edit.post',['id'=>$post->id])}}">Edit Post</a></li>
                        <li><a href={{route('user.hide.post',['id'=>$post->id])}}>Hide Post</a></li>
                    @else
                        <li><a href={{route('user.hide.post',['id'=>$post->id])}}>Hide Post</a></li>
                    @endif
                </ul>
            </li>
        </ul>
    </div>
    <div class="post_content">
        <div class="post_pera"> <?php echo $post->description; ?> </div>

        {{--Image inside posts--}}


        @if($post->images->count() != 0)
            @include('user.includes.posts.post-images')

        @endif

        @if($post->videos->count() != 0)
            @include('user.includes.posts.post-videos')

        @endif

        {{--End Section--}}


        <ul class="comments_ul">
            <li><a href="javascritp:void(0)" onclick="addLike({{$post->id}}, {{Auth::user()->id}})">
                    <span id="like_status_post_{{$post->id}}"
                          @if($post->likes->where('post_id',$post->id)->where('user_id',Auth::user()->id)->count() ==1 ) style="color:blue" @endif><i
                                id="like_thumb_post_{{$post->id}}"
                                @if($post->likes->where('post_id',$post->id)->where('user_id',Auth::user()->id)->count() ==1 ) style="color:blue"
                                @endif class="fas fa-thumbs-up"></i> Like </span> &nbsp;( <span
                            id="like_count_post_{{$post->id}}">{{$post->likes->count()}}  </span> )</a>
            </li>
            <li><a href="javascritp:void(0)" data-toggle="collapse"
                   data-target="#coment_{{$post->id}}" onclick="loadComments({{$post->id}}, {{Auth::user()->id}})"><i
                            class="fas fa-comment"></i> Comments &nbsp; ( <span
                            id="comment_count_post{{$post->id}}">{{$post->comments->count()}}</span> )</a></li>
            <li><a href="javascritp:void(0)"><i class="fas fa-share"></i> Share &nbsp;(0)</a>
            </li>
        </ul>
    </div>

    {{--COMMENTS--}}

    @include('user.includes.posts.post-comments')

    {{--END COMMENTS--}}

</li>