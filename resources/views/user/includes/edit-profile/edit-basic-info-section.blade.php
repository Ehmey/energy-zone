<div class="edit-profile02 bg-white">
    <div class="our-heading"><h3>Profile Info</h3></div>
    <form method="post" action="{{route('user.edit-profile')}}">
        @csrf

        <div class="form-wrapper">
            <label>
                <span>Job Title*</span>
                <span id="job_title_edit" style="font-size:13px;display:inline-block;margin-right: 20px;">   @if(Auth::user()->jobTitle->id  ) {{Auth::user()->jobTitle->name}} @endif </span> <span id="pencil-click-job" style="display:inline-block;font-size:12px;"><i onclick="changejobTitle(1)"  class="fas fa-pencil-alt"></i></span>



            </label>
            <hr>
            <label>
                <span>Company*</span>

                <span id="company_edit" style="font-size:13px;display:inline-block;margin-right: 20px;">   @if(Auth::user()->company->id  ) {{Auth::user()->company->name}} @endif </span> <span id="pencil-click-company" style="display:inline-block;font-size:12px;;"><i onclick="changeCompany(1)"  class="fas fa-pencil-alt"></i></span>

            </label>
            <hr>
            <label>
                <span>Country*</span>
                <span id="country_edit" style="font-size:13px;display:inline-block;margin-right: 20px;">   @if(Auth::user()->country->id  ) {{Auth::user()->country->name}} @endif </span> <span id="pencil-click-country" style="display:inline-block;font-size:12px;;"><i onclick="changeCountry(1)"  class="fas fa-pencil-alt"></i></span>

            </label>
<hr>
            <label>
                <span><span style="display:inline-block;">Description </span><span id="pencil-click-desc" style="margin-left:10px;font-size:12px; display:inline-block;"><i onclick="changeDesc(1)"  class="fas fa-pencil-alt"></i></span></span>


                <span id="desc_edit" style="font-size:13px;width:100%;display:inline-block;margin-right: 20px;">  @if(Auth::user()->userInfo->description == "") ....  @else {{Auth::user()->userInfo->description}}  @endif </span>

                {{--<small>2000 characters remaining (2000 maximum)</small>--}}
            </label>


        </div>
    </form>
</div>