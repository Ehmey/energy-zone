<div class="adding-wrapper bg-white">
    <div class="our-heading clearfix">
        <h3>Languages</h3>
        <a class="btn-small" data-toggle="modal" data-target="#editLanguage">Add Languages</a>
    </div>
    <div class="text">@if(Auth::user()->languages()->count() ==0) You haven't added any Languages yet. @else

            @foreach(Auth::user()->languages as $language)

                <a href="{{route('user.delete.language',['id'=>$language->id])}}" class="label label-primary new_btn_icn">{{$language->name}} <i class="fa fa-times"></i> </a>
                @endforeach  @endif
              </div>
</div>
{{--<div class="text-center view-moere"><a href="">View More</a></div>--}}
@include('user.includes.popups.edit-language-popup')