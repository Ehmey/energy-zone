<div class="adding-wrapper @if($data['user_education']->count() == 0) bg-white @endif" style="padding-bottom:0px;">
    <div class="our-heading clearfix">
        <h3>Education</h3>
        <a class="btn-small" data-toggle="modal" data-target="#addEducation">Add Education</a>
    </div>

    @if($data['user_education']->count() != 0)



        @foreach($data['user_education']->get() as $education)

            <div class="edit-profile03">
                <div style="margin:0px;" class="hr-accontant bg-white">
                    <div class="clearfix">
                        <div class="in-left">
                            <img src="{{url($education->educationSchool->first()->image_path)}}" alt="img">
                        </div>
                        <div class="in-right">

                            <h3>{{$education->educationSchool->name}}   </h3>
                            <h4>{{$education->educationDegree->name}}    </h4>
                            <div class="text" style="padding:0px;">{{$education->activities}}
                            </div>
                        </div>
                    </div>
                    <div class="adit-del">
                        <a data-toggle="modal" data-target="#editEducation_{{$education->id}}"><i class="fas fa-pencil-alt"></i></a>
                        <a href="{{route('user.delete.education',['id'=>$education->id])}}"><i
                                    class="fas fa-trash-alt"></i></a>
                    </div>
                </div>
            </div>

            {{--{{$education->educationSchool->first()->name}}   </br>--}}

            {{--{{$education->educationDegree->first()->name}} </br>--}}
            {{--{{$education->activities}}--}}
            @include('user.includes.popups.edit-education-popup')

        @endforeach
    @else
        <div class="text">
            You haven't added any education yet.
        </div>
    @endif
</div>
{{--<div class="text-center view-moere"><a href="">View More</a></div>--}}
@include('user.includes.popups.add-education-popup')