<div class="adding-wrapper @if(Auth::user()->experince()->count() == 0) bg-white @endif" style="padding:0px;">
    <div class="our-heading clearfix" style="border:0px;">
        <h3>Experince</h3>
        <a class="btn-small" data-toggle="modal" data-target="#addExperince">Add Experince</a>
    </div>


@if(Auth::user()->experince()->count() != 0)

@foreach(Auth::user()->experince as $experince)
    <div class="edit-profile03">
        <div style="margin:0px;" class="hr-accontant bg-white">
            <div class="clearfix">
                <div class="in-left">
                    <img src="{{asset($experince->experinceCompany->image_path)}}" alt="img">
                </div>
                <div class="in-right">
                    <h3>{{$experince->experinceJobTitle->name}}</h3>
                    <h4>{{$experince->experinceCompany->name}}</h4>
                    <div class="text" style="padding:0px;">{{$experince->experince}}

                    </div>
                </div>
            </div>
            <div class="adit-del">
                <a data-toggle="modal" data-target="#editExperince_{{$experince->id}}"><i class="fas fa-pencil-alt"></i></a>
                <a href="{{route('user.delete.experince',[$experince->id])}}"><i class="fas fa-trash-alt"></i></a>
            </div>
        </div>
    </div>

            @include('user.includes.popups.edit-experience-popup')
    @endforeach

@else
    <div class="text">
        You haven't added any Experince yet.
    </div>
@endif
</div>


@include('user.includes.popups.add-experince-popup')