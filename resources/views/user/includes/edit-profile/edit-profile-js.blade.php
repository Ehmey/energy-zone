<script>
    @if(!empty($data['job_title']))
        jobEditHtml = `<select name="job_title" id="selector_job_title">
        @foreach($data['job_title'] as  $jobTitle)
        <option  value="{{$jobTitle->id}}"  @if(Auth::user()->jobTitle->id == $jobTitle->id) selected @endif>{{$jobTitle->name}}</option>
        @endforeach

        </select>`

    @endif
    function changejobTitle(flag) {
        if (flag == 1) {

            document.getElementById('job_title_edit').innerHtml = jQuery(document).find('#job_title_edit').html(jobEditHtml);
            document.getElementById('pencil-click-job').innerHTML = `<i onclick="changejobTitle(0)" style="    opacity: 1;" class="glyphicon glyphicon-ok"></i>`;

        } else {
            nameValue = document.getElementById('selector_job_title');
            console.log(nameValue);
            console.log(nameValue.options[nameValue.selectedIndex].text);

            jQuery(document).find('#job_title_edit').html(nameValue.options[nameValue.selectedIndex].text);
            jQuery(document).find('#pencil-click-job').html(`<i onclick="changejobTitle(1)" class="fas fa-pencil-alt"></i>`);

            baseUrl = "{{url('user/edit/jobtitle')}}?name=" + nameValue.value;
            $.get(baseUrl, function (data, status) {
         //       alert("Data: " + data + "\nStatus: " + status);
            });
        }
    }




    @if(!empty($data['companies']))
        companyEditHtml = `<select  id="selector_companies">
        @foreach($data['companies'] as  $company)
        <option  value="{{$company->id}}"  @if(Auth::user()->company->id == $company->id) selected @endif>{{$company->name}}</option>
        @endforeach

        </select>`

    @endif


    function changeCompany(flag) {
        if (flag == 1) {

            document.getElementById('company_edit').innerHtml = jQuery(document).find('#company_edit').html(companyEditHtml);
            document.getElementById('pencil-click-company').innerHTML = `<i onclick="changeCompany(0)" style="    opacity: 1;" class="glyphicon glyphicon-ok"></i>`;

        } else {
            nameValue = document.getElementById('selector_companies');
            console.log(nameValue);
            console.log(nameValue.options[nameValue.selectedIndex].text);

            jQuery(document).find('#company_edit').html(nameValue.options[nameValue.selectedIndex].text);
            jQuery(document).find('#pencil-click-company').html(`<i onclick="changeCompany(1)" class="fas fa-pencil-alt"></i>`);

            baseUrl = "{{url('user/edit/company')}}?name=" + nameValue.value;
            $.get(baseUrl, function (data, status) {
             //   alert("Data: " + data + "\nStatus: " + status);
            });
        }
    }



    @if(!empty($data['countries']))
        countriesEditHtml = `<select  id="selector_countries">
        @foreach($data['countries'] as  $country)
        <option  value="{{$country->id}}"  @if(Auth::user()->country->id == $country->id) selected @endif>{{$country->name}}</option>
        @endforeach

        </select>`

    @endif


    function changeCountry(flag) {
        if (flag == 1) {

            document.getElementById('country_edit').innerHtml = jQuery(document).find('#country_edit').html(countriesEditHtml);
            document.getElementById('pencil-click-country').innerHTML = `<i onclick="changeCountry(0)" style="    opacity: 1;" class="glyphicon glyphicon-ok"></i>`;

        } else {
            nameValue = document.getElementById('selector_countries');
            console.log(nameValue);
            console.log(nameValue.options[nameValue.selectedIndex].text);

            jQuery(document).find('#country_edit').html(nameValue.options[nameValue.selectedIndex].text);
            jQuery(document).find('#pencil-click-country').html(`<i onclick="changeCountry(1)" class="fas fa-pencil-alt"></i>`);

            baseUrl = "{{url('user/edit/country')}}?name=" + nameValue.value;
            $.get(baseUrl, function (data, status) {
                //   alert("Data: " + data + "\nStatus: " + status);
            });
        }
    }

var descEdit =` <textarea id="edit_desc_input"   style="width:100%;" >{{Auth::user()->userInfo->description}}</textarea>`;
    function changeDesc(flag){
        if (flag == 1) {

            document.getElementById('desc_edit').innerHTML = descEdit;
            document.getElementById('pencil-click-desc').innerHTML = `<i onclick="changeDesc(0)" style="    opacity: 1;" class="glyphicon glyphicon-ok"></i>`;

        } else {
            nameValue = document.getElementById('edit_desc_input');


            jQuery(document).find('#desc_edit').html(nameValue.value);
            jQuery(document).find('#pencil-click-desc').html(`<i onclick="changeCountry(1)" class="fas fa-pencil-alt"></i>`);

            baseUrl = "{{url('user/edit/desc')}}?name=" + nameValue.value;
            $.get(baseUrl, function (data, status) {
                 //  alert("Data: " + data + "\nStatus: " + status);
            });
        }
    }

</script>