<div class="adding-wrapper bg-white">
    <div class="our-heading clearfix">
        <h3>Skills</h3>
        <a class="btn-small"  data-toggle="modal" data-target="#editSkill">Add Skills</a>
    </div>

    <div class="text">@if(Auth::user()->skills()->count() ==0) You haven't added any skill yet. @else

                          @foreach(Auth::user()->skills as $skill)

                              <a href="{{route('user.delete.skill',['id'=>$skill->id])}}" class="label label-primary new_btn_icn">{{$skill->name}} <i class="fa fa-times"></i> </a>
                              @endforeach

        @endif</div>
</div>

@include('user.includes.popups.edit-skill-popup')
{{--<div class="text-center view-moere"><a href="">View More</a></div>--}}