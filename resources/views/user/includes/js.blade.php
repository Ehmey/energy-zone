<script src="{{asset('energy-zone/user/js/bootstrap.min.js')}}"></script>
<script src="{{asset('energy-zone/user/js/main.js')}}"></script>


<script>


    @auth
        totalCount = 0;



    function removeItemInPost(item){

        if(item == 'video'){
            document.getElementById('video-preview-post').innerHTML ="";
            document.getElementById('dummy_upload_video').style.display ="none";
            document.getElementById('real_upload_video').style.display = 'block';
            document.getElementById('real_upload_image').style.display = 'block';
            document.getElementById('dummy_upload_image').style.display = 'none';
            document.getElementById('filePostVideo').value = "";


        }else{
            document.getElementById('dummy_upload_video').style.display ="none";
            document.getElementById('real_upload_video').style.display = 'block';
            document.getElementById('real_upload_image').style.display = 'block';
            document.getElementById('dummy_upload_image').style.display = 'none';
            document.getElementById('preview').innerHTML ="";
            document.getElementById('filePost').value = "";

        }
    }


    function postCommentsOnEnter(postId, authUserId,event){
if(event.keyCode == 13){ postComment(postId, authUserId );}
    }


    function ifPostHasImage(data) {
        if (data.posts[i].post_image_path != null) {
            return `
<div class="post_img"><a href="#"><img
                src="` + data.posts[i].post_image_path + `"
                alt="error img"></a></div>`;
        } else if (data.posts[i].post_video_path != null) {

            return `
<div class="post_img"><a href="#">
        <video  controls>
            <source src="` + data.posts[i].post_video_path + `" type="video/mp4">
            Your browser does not support HTML5 video.
        </video>

        </a></div>`;
        } else {
            return "";
        }

    }


    function getMoreHtml(i, length, data) {
        if (data.posts[i].total_posts > 5) {
            if (i == length - 1) {//alert(totalCount);
                if (length)
                    return `<li id="more_posts"><div class="seemorebutton text-center">
<div class="see_more_main text-center">
                <a href="javascript:void(0)"  onclick="loadposts(1)" >See more</a>
            </div>


 </li>`;
            }
            return "";
        }
        else {
            return "";
        }

    }

    function getpostLists(data, i) {
        if (data.posts[i].posted_by == "me") {
            return `<li><a href="` + data.posts[i].post_delete_url + `">Delete Post</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#not_found">Edit Post</a></li>
                        <li><a href="` + data.posts[i].hide_post_url + `">Hide Post</a></li>`;
        } else {
            return `<li><a href="` + data.posts[i].hide_post_url + `">Hide Post</a></li>`;
        }
    }


    function loadposts(removeMore = null) {
        if (removeMore != null) {
            document.getElementById('more_posts').remove();
        }
        authUserId = {{Auth::user()->id}};
        baseUrl = "{{url('user/load/posts')}}" + "/?start=" + totalCount;
        //  document.getElementById('reply_input_' + commentId).value = "";

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            postsHtmL = document.getElementById('posts_url').innerHTML;
            if (this.readyState == 4 && this.status == 200) {
                data = JSON.parse(this.responseText);
                if (data.posts != null) {
//console.log("worked");
                    for (i = 0; i < data.posts.length; i++) {
                        getMore = getMoreHtml(i, data.posts.length, data);
                        postsLis = getpostLists(data, i);
                        likeBy = "";


                        if (data.posts[i].like_by == "me") {
                            likeBy = "style='color:blue;'";
                        }
                        postDesc = "";
                        if (data.posts[i].post_desc != null) {
                            postDesc = data.posts[i].post_desc;
                        }
                        PostHasImageOrVideo = ifPostHasImage(data);


                        postsHtmL = postsHtmL + `<li>
    <div class="coment_sec1">
        <div class="coment_sec1_left">
                                <span class="coment_img">
                                    <img src="` + data.posts[i].user_img + `" alt="error img">
                                </span>
                <a href="` + data.posts[i].user_public_profile + `">
                    <div class="coment_content_right">
                        <span class="post_name"> ` + data.posts[i].posted_user_name + `</span>
                        <p class="post_date"> ` + data.posts[i].created_at + `</p>
                    </div>
                </a>
        </div>
        <ul class="drop_down_main2">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle drop_down_anker2" data-toggle="dropdown" aria-expanded="false">
                    <i class="fas fa-ellipsis-h"></i>
                </a>
                <ul class="dropdown-menu">
                    ` + postsLis + `
                 </ul>
            </li>
        </ul>
    </div>
        <div class="post_content">
                    <div class="post_pera"> ` + postDesc + ` </div>
` + PostHasImageOrVideo + `


                    <ul class="comments_ul">
                        <li>
                            <a href="javascritp:void(0)" onclick="addLike(` + data.posts[i].post_id + `,` + authUserId + `)">




                            <span id="like_status_post_` + data.posts[i].post_id + `"   ><i id="like_thumb_post_` + data.posts[i].post_id + `" ` + likeBy + `   class="fas fa-thumbs-up"></i> Like </span> &nbsp;( <span id="like_count_post_` + data.posts[i].post_id + `">  ` + data.posts[i].posts_like + ` </span> )</a>
                        </li>
                        <li>
                            <a   data-toggle="collapse" data-target="#coment_` + data.posts[i].post_id + `" onclick="loadComments( ` + data.posts[i].post_id + `,` + authUserId + `)">
                                <i class="fas fa-comment"></i> Comments &nbsp; ( <span id="comment_count_post` + data.posts[i].post_id + `">` + data.posts[i].post_comments + ` </span> )
                            </a>
                        </li>
                        <li>
                             <a   data-toggle="modal" data-target="#not_found">
                                <i class="fas fa-share"  data-toggle="modal" data-target="#not_found"></i> Share &nbsp;(0)
                             </a>
                        </li>
                    </ul>
        </div>



           <div  class="collapse" id="coment_` + data.posts[i].post_id + `">
    <div class="ad_coment_main bg-white">
        <div class="coment_white_sec">
            <input type="text" placeholder="Add a comment" class="ad_coment_text" onkeydown="return postCommentsOnEnter(`+data.posts[i].post_id+`, authUserId, event)" id="comment_input_id_` + data.posts[i].post_id + `">
            <div class="browse_coment_main">
                <label class="file_upload_btn2">
                    <input name="myFile" multiple type="file"><span><i class="fas fa-camera" data-toggle="modal" data-target="#not_found"></i> </span>
                </label>
                <input type="button" onclick="postComment(` + data.posts[i].post_id + `, ` + authUserId + `)" value="Comment" class="post_com_btn">
            </div>
        </div>

        <div class="commentshow clearfix">
            <div class="man-cmment">
<div id="post_comments_` + data.posts[i].post_id + `" style="display:none">
Loading ...
</div>
            </div>

        </div>

    </div>
</div>
` + getMore + `
    <li>

</div>





    `;

                    }

                    document.getElementById('posts_url').innerHTML = postsHtmL;
                    totalCount = totalCount + 5;
                    console.log(data);
                }
                else {

                    if (document.getElementById('more_posts')) {
                        document.getElementById('more_posts').remove();
                    }


                }
            }
        }
        xhttp.open("GET", baseUrl, true);
        xhttp.send();
    }

    @endauth


    //$('#alert_notice').find(  );

    setTimeout(function () {
        $('#alert_notice').fadeOut('slow');
    }, 5000);

    //
    // setTimeout(function () {
    //     var e = jQuery(document).find('#admin_receiver_id').val('#alert_notice');
    //     alert(e);
    //     //e.remove();
    // }, 2000, 8000);


    function editProfileImage() {


        fileInput = jQuery(document).find('#image_profile').val(); //document.getElementById('image_profile').value;

        FileUploadPath = fileInput;


        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image

        if (Extension != "png" && Extension != "jpeg" && Extension != "jpg") {
            document.getElementById('for_alert').innerHTML = `<div class="alert alert-warning">
  <strong>Warning!</strong> Only .PNG, .JPG, and JPEG files are allowed.
</div>`;
            setTimeout(function () {
                $('#for_alert').fadeOut('slow');
            }, 5000);

            return;
        }
        //alert($('input[type=file]')[0].files[0].size );

        if ($('input[type=file]')[0].files[0].size > 1000000) {
            document.getElementById('for_alert').innerHTML = `<div class="alert alert-warning">
                <strong>Warning!</strong> Maximum Image Size which is alowed is 1 MB.
                </div>`;
            setTimeout(function () {
                $('#for_alert').fadeOut('slow');
            }, 5000);

            return;

        }

        document.getElementById("img_preview").innerHTML = `<div style="
    margin-top: 50px;
    ">Loading ... </div>`;
        var formData = new FormData(this);
        formData.append('image_profile', $('input[type=file]')[0].files[0]);
        baseUrl = "{{url('user/edit/profile-image')}}";

        $.ajax({
            type: 'POST',
            url: baseUrl,
            data: formData,
            headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},

            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {


            document.getElementById("img_preview").innerHTML = `<img src="` + data[0] + `"   >`;
                console.log(data[0]);
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    }


    /////////////////////       JS FOR EDIT PROFILE PAGE

    function changeName(flag) {
        if (flag == 1) {
            var name = jQuery(document).find('#user_name').html();
            document.getElementById('user_name').innerHtml = jQuery(document).find('#user_name').html(`<input id="edit-name-val" onkeydown="return limitText(this,event)" name="name" value="` + name + `" type="text" >`);
            document.getElementById('pencil-click').innerHTML = `<i onclick="changeName(0)" style="    opacity: 1;" class="glyphicon glyphicon-ok"></i>`;

        } else {
            nameValue = jQuery(document).find('#edit-name-val').val();
            jQuery(document).find('#user_name').html(jQuery(document).find('#edit-name-val').val());
            jQuery(document).find('#pencil-click').html(`<i onclick="changeName(1)" class="fas fa-pencil-alt"></i>`);
            jQuery(document).find('#edit-name-val').remove();
            baseUrl = "{{url('user/edit/name')}}?name=" + nameValue;
            $.get(baseUrl, function (data, status) {
                // alert("Data: " + data + "\nStatus: " + status);
            });
        }
    }


    function limitText(field, event, maxChar = 30) {

        var ref = $(field);
        val = ref.val();
        if (event.keyCode != 8) {
            if (val.length >= maxChar) {
                return false;
                ref.val(function () {
                    return val.substr(0, maxChar);
                });
            }
        }
    }

</script>