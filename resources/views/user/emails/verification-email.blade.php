
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding:0px;background:#f2f2f2">
    <tbody>
    <tr>
        <td style="background-color:#fff;">
            <table cellpadding="0" cellspacing="0" align="center" width="100%">
                <tbody>
                <tr>
                    <td>
                        <table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="600" style="border:2px solid #000; ">
                            <tbody>
                            <tr>
                                <td align="center" valign="top" style="padding:0px;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="600">
                                        <tbody>

                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="600">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center" valign="top" width="600">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td align="center" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" style="padding:10px 40px 0px;;background-size:100%;" width="100%">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td valign="top" align="left">
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tbody>
                                                                                                    <tr>
                                                                                                        <td style="width:100%" align="left">
                                                                                                            <a href="{{env('APP_URL')}}"><img width="143" alt="Welcome mail" title="Welcome mail" src="{{asset('energy-zone/user/images/logo.png')}}" class="CToWUd"></a>
                                                                                                        </td>

                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr align="center">
                                                                                <td style="padding:0px 40px 0px;">
                                                                                    <div style="font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;color:#777;font-size:30px;font-weight:600;padding-bottom:10px; color:#63b645;">
                                                                                        <img src="{{asset('energy-zone/user/images/verify.png')}}" alt="img">
                                                                                    </div>
                                                                                    <div style="padding:0px 0px 40px 0px;margin:0px;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;line-height:25px;font-size:22px">
                                                                                    <span style="color:#232228;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;">Verify Your Account
</span>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>

                                                                            <tr align="left" style="background:#f9fbfc;">
                                                                                <td style="padding:10px 40px 5px;margin:0px;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;line-height:50px;font-size:18px;color:#283a84;"> Your account information
                                                                                </td>
                                                                            </tr>

                                                                            <tr style="background:#e3ebee;">
                                                                                <td valign="middle" align="center" style="padding:0px 0px 0px">
                                                                                    <table cellpadding="0" cellspacing="0" width="100%" style="padding:30px 10px;width:540px">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td valign="left" width="100">
                                                                                                <div style="font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;font-size:14px;line-height:25px;font-weight:bold;color:#3f3f3f;text-align:left;font-size:12px;margin-top:0px;">Email</div>
                                                                                            </td>
                                                                                            <td valign="left">
                                                                                                <div style="font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;font-size:14px;line-height:25px;font-weight:bold;color:#757575;text-align:left;font-size:12px;margin-top:0px;">{{$data['email']}}</div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="left" width="100">
                                                                                                <div style="font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;font-size:14px;line-height:25px;font-weight:bold;color:#3f3f3f;text-align:left;font-size:12px;margin-top:0px;">Verify Code</div>
                                                                                            </td>
                                                                                            <td valign="left">
                                                                                                <div style="font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;font-size:14px;line-height:25px;font-weight:bold;color:#757575;text-align:left;font-size:12px;margin-top:0px;">{{$data['code']}}</div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td style="background-color:#fff; border-top:1px solid #cbd2d4;" align="left">
                                                                                    <table cellpadding="0" cellspacing="0" style="margin:0px 0px 0px;border-bottom:1px solid #e1e1e1">
                                                                                        <tbody>
                                                                                        <tr style="background:#e3ebee; border-top:1px solid #cbd2d4;">
                                                                                            <td style="padding:30px 40px;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;width:720px;font-size:13px;color:#757575;" valign="middle" align="left">
                                                                                                To complete your energy zone profile we need you to confirm your emil address so we know that you're reachable to that address
                                                                                                All the Best<br>
                                                                                                Energy Zone
                                                                                                </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="background-color:#fff" align="center">
                                                                                    <table cellpadding="0" cellspacing="0" style="margin:0px 0px 0px;border-bottom:1px solid #e1e1e1">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td style="padding:30px 40px;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;width:720px;font-size:12px;color:#000; font-weight:bold; line-height:18px;" valign="middle" align="center">
                                                                                               Contact us for further details.
                                                                                                </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>