<!DOCTYPE html>
<html lang="en">
<head>

    <title>SignUp</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">


    @include('user.includes.css')
    @include('user.includes.js')

</head>

<body>

<!-- main- taplate start here -->

<div class="forget-password-wrapper">
    <div class="inner-wrapper">

        <div class="logo-wrapper">
            <a href="{{url(''.env('APP_URL'))}}"><img src="{{asset('energy-zone/user/images/logo.png')}}"></a>
        </div>

        <div class="forget-pas">
            <a href="#">Forgot your password ?</a>
        </div>
        {{--<div class="card-body">--}}

        <div class="form-wrap">
            <form method="POST" action="{{ route('password.update') }}">
                <input type="hidden" name="token" value="{{ $token }}">
                @if (session('status'))
                    <div class="alert alert-success" role="alert" id="alert_notice">
                        {{ session('status') }}
                    </div>
                @endif
                @csrf
                <input type="email" name="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}"
                       placeholder="Enter your email address..." required>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
<br>
                    <input type="password"placeholder="Password" name="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}"
                         required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
                </span>
                    @endif

<br>

                    <input type="password" placeholder="Confirm Password" name="password_confirmation" class="{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           required>



                <button type="submit">Send</button>
            </form>
        </div>

        <div class="back-to-login">
            <a href="{{route('login')}}">Back to Login?</a>
        </div>

    </div>
</div>

<!-- main- taplate start here clsed -->


@include('user.includes.js')


</body>

</html>


