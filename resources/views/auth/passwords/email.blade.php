
{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Reset Password') }}</div>--}}

                {{--<div class="card-body">--}}
                    {{--@if (session('status'))--}}
                        {{--<div class="alert alert-success" role="alert">--}}
                            {{--{{ session('status') }}--}}
                        {{--</div>--}}
                    {{--@endif--}}

                    {{--<form method="POST" action="{{ route('password.email') }}">--}}
                        {{--@csrf--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{ __('Send Password Reset Link') }}--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

        <!DOCTYPE html>
<html lang="en">
<head>

    <title>SignUp</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">


    @include('user.includes.css')




</head>

<body>

<!-- main- taplate start here -->

<div class="forget-password-wrapper">
    <div class="inner-wrapper">

        <div class="logo-wrapper">
            <a href="{{url(''.env('APP_URL'))}}"><img src="{{asset('energy-zone/user/images/logo.png')}}"></a>
        </div>

        <div class="forget-pas">
            <a href="#">Forgot your password ?</a>
        </div>
        {{--<div class="card-body">--}}

        <div class="form-wrap">
            <form method="POST" action="{{ route('password.email') }}">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @csrf
                    @if ($errors->has('email'))
                        <div class="alert alert-danger">
                            <strong>Failed!</strong> {{ $errors->first('email') }}
                        </div>
                    @endif
                <input type="email" name="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Enter your email address..." required>

                <button type="submit">Send</button>
            </form>
        </div>

        <div class="back-to-login">
            <a href="{{route('login')}}">Back to Login?</a>
        </div>

    </div>
</div>

<!-- main- taplate start here clsed -->




@include('user.includes.js')


</body>

</html>

