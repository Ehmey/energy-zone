<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID','152788888640661'),
        'client_secret' => env('FACEBOOK_SECRET_ID','2ca6deebaa04583fb2a0dc0f151e41a4'),
        'redirect' => env('REDIRECT_URL','http://energyzone.technerdstesting.com/facebook-callback'),
    ],

    'google' => [
        'client_id' => env('GMAIL_CLIENT_ID','759561247365-t4qkl1b3phf1kri5gemhts9vv5k7l3oe.apps.googleusercontent.com'),
        'client_secret' => env('GMAIL_SECRET_ID','OXUKCL5zIyYHUBcVh8tr1'),
        'redirect' => env('REDIRECT_URL_GMAIL','http://energyzone.technerdstesting.com/gmail-callback')
    ],
    'linkedin' => [
        'client_id' => env('LINKEDIN_CLIENT_ID','81r0f8b2zppxsq'),
        'client_secret' => env('LINKEDIN_SECRET_ID','Fv2Fdu7B3sEXTjWG'),
        'redirect' => env('REDIRECT_URL_LINKEDIN','http://energyzone.technerdstesting.com/linkedin-callback')
    ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

];
