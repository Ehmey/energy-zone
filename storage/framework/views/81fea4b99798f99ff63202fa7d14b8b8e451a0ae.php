<?php echo $__env->make('alerts.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<form method="post" action="<?php echo e(route('user.register')); ?>" enctype='multipart/form-data'>
    <?php echo csrf_field(); ?>
    <input name="step" value="1" type="text" style="display:none;">
    <input type="text" name="name" placeholder="Name" required>
    <?php if($errors->has('name')): ?>
        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
    <?php endif; ?>

    <input type="email" name="email" placeholder="Email" required>
    <?php if($errors->has('email')): ?>
        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
    <?php endif; ?>
    <input type="password" name="password" placeholder="Password" required>
    <?php if($errors->has('password')): ?>
        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
    <?php endif; ?>

    <div class="site-btn">
        <button type="submit">Next</button>
    </div>
</form>
