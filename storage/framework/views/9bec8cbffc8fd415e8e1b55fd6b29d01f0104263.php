<?php echo $__env->make('alerts.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<form method="post" action="<?php echo e(route('login')); ?>">
    <?php echo csrf_field(); ?>
    <input type="email" name="email" placeholder="Email" class="mail_field">

    <?php if($errors->has('email')): ?>
        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
    <?php endif; ?>
    <input type="password" name="password" placeholder="Password" class="mail_field">
    <?php if($errors->has('password')): ?>
        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
    <?php endif; ?>
    <div class="forget-main">


    <label class="check">
        <input type="checkbox" class="chk" name="remember">
        <span class="checkmark"></span>Remember me
    </label>
    <a class="forgot_pas" href="<?php echo e(route('password.request')); ?>">
        <?php echo e(__('Forgot Your Password?')); ?>

    </a>
    </div>

    <span class="login_btn"><input type="submit" value="Login"></span>
</form>
