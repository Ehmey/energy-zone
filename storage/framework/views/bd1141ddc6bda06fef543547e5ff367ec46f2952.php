<?php if(Session::has('success')): ?>

    <?php if($errors->has('email')): ?>
        <div class="alert alert-danger" id="alert_notice">
            <strong>Failed!</strong> <?php echo e($errors->first('email')); ?>

        </div>
    <?php endif; ?>


    <div class="alert alert-success clearfix" style="width:100%;display:inline-block;">
        <strong>Success!</strong> <?php echo e(Session::get('success')); ?>

    </div>
    <?php elseif(Session::has('exception')): ?>
    <div class="alert alert-danger" id="alert_notice">
        <strong>Failed!</strong> Some technical Issue
    </div>
<?php elseif(Session::has('wrong-code')): ?>
    <div class="alert alert-danger" id="alert_notice">
        <strong>Failed!</strong> <?php echo e(Session::get('wrong-code')); ?>

    </div>
<?php endif; ?>


<?php if(Session::has('edited-success')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('edited-success')); ?>

    </div>
    <?php endif; ?>

    <?php if(Session::has('post-deleted-success')): ?>
        <div class="alert alert-success" id="alert_notice">
            <strong>Success!</strong> <?php echo e(Session::get('post-deleted-success')); ?>

        </div>
<?php endif; ?>



<?php if(Session::has('skill-edit-success')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('skill-edit-success')); ?>

    </div>
<?php endif; ?>

<?php if(Session::has('skill-added')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('skill-added')); ?>

    </div>
<?php endif; ?>




<?php if(Session::has('language-added-failed')): ?>
    <div class="alert alert-danger" id="alert_notice">
        <strong>Failed!</strong> <?php echo e(Session::get('language-added-failed')); ?>

    </div>
<?php endif; ?>

<?php if(Session::has('skill-added-failed')): ?>
    <div class="alert alert-danger" id="alert_notice">
        <strong>Failed!</strong> <?php echo e(Session::get('skill-added-failed')); ?>

    </div>
<?php endif; ?>

<?php if(Session::has('language-added')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('language-added')); ?>

    </div>
<?php endif; ?>




<?php if(Session::has('language-delete-success')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('language-delete-success')); ?>

    </div>
<?php endif; ?>


<?php if(Session::has('post-success')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('post-success')); ?>

    </div>
<?php endif; ?>




<?php if(Session::has('education-added')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('education-added')); ?>

    </div>
<?php endif; ?>

<?php if(Session::has('education-delete-success')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('education-delete-success')); ?>

    </div>
<?php endif; ?>




<?php if(Session::has('post-hide-success')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('post-hide-success')); ?>

    </div>
<?php endif; ?>

<?php if(Session::has('experince-added')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('experince-added')); ?>

    </div>
<?php endif; ?>

<?php if(Session::has('school-added-success')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('school-added-success')); ?>

    </div>
<?php endif; ?>




<?php if(Session::has('degree-added-success')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('degree-added-success')); ?>

    </div>
<?php endif; ?>

<?php if(Session::has('company-added-success')): ?>
    <div class="alert alert-success" id="alert_notice">
        <strong>Success!</strong> <?php echo e(Session::get('company-added-success')); ?>

    </div>
<?php endif; ?>



