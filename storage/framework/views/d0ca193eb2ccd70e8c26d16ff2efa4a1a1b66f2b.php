<?php $__env->startSection('title', "Dashboard"); ?>
<?php $__env->startSection('content'); ?>
    <div class="dashboard-heading clearfix">
        <h2>Dashboard</h2>
        <ul>
            <li>Energy Zone / Dashboard</li>
        </ul>
    </div>
    <br>
<img style="width:100%;" src="<?php echo e(asset('energy-zone/dashboard.jpg')); ?>">
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app-admin', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>