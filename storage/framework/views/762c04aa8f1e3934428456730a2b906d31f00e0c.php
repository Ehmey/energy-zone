<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo e(config('app.name', 'Energy Zone')); ?> - Login</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">-->
    <?php echo $__env->make('user.includes.css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</head>

<body>
<header>
    <section class="landing_login_main_page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-sm-12 bg-white">
                    <div class="landing_colm_left">
                        <div class="landing_left_height">
                            <div class="nav_main">
                                <?php echo $__env->make('user.includes.menus.login-top-nav', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                            <a href="javascript:void(0)" class="visible-xs visible-sm login_mobile_btn"
                               data-toggle="modal" data-target="#myModal">Login</a>
                            <h2 class="right_people_text">Get the Right People </h2>
                            <h3 class="right_place_text">From the Right Place</h3>
                            <form>
                                <div class="landing_left_search_main">
                                    <input type="search" placeholder="Search for people and companies">
                                    <span class="search_btn"><input type="submit" value=""></span>
                                </div>
                            </form>
                            <?php echo $__env->make('user.includes.footers.login-bottom-footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-6 col-sm-12 hidden-xs hidden-sm">
                    <div class="row">
                        <div class="landing_right_height">
                            <div class="bg_landing_page">
                                <div class="bg_landing_page_in">
                                    <div class="landing_login_main">
                                        <div class="login_text_main">
                                            <span class="signin_text">Login</span>
                                            <ul class="signin_social">
                                                <li><a href="<?php echo e(route('linkedin-login')); ?>"><i
                                                            class="fab fa-linkedin-in"></i></a></li>
                                                <li><a href="<?php echo e(route('gmail-login')); ?>"><i
                                                            class="fab fa-google-plus-g"></i></a></li>
                                                <li><a href="<?php echo e(route('facebook-login')); ?>"><i
                                                            class="fab fa-facebook-f"></i></a></li>
                                            </ul>
                                        </div>
                                        <?php echo $__env->make('user.includes.forms.login-form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</header>
<div class="modal fade popup_padding" id="myModal" role="dialog">
    <div class="landing_login_main">
        <a href="#" class="visible-sm visible-xs popup_close_icon" data-dismiss="modal"><i class="fas fa-times"></i></a>
        <div class="login_text_main">
            <span class="signin_text">Login</span>
            <ul class="signin_social">
                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
            </ul>
        </div>
        <?php echo $__env->make('user.includes.forms.login-form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
</div>


<?php echo $__env->make('user.includes.js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>


</body>

</html>
