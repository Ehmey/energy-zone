<div class="edit-profile02 bg-white">
    <div class="our-heading"><h3>Profile Info</h3></div>
    <form method="post" action="<?php echo e(route('user.edit-profile')); ?>">
        <?php echo csrf_field(); ?>

        <div class="form-wrapper">
            <label>
                <span>Job Title*</span>
                <span id="job_title_edit" style="font-size:13px;display:inline-block;margin-right: 20px;">   <?php if(Auth::user()->jobTitle->id  ): ?> <?php echo e(Auth::user()->jobTitle->name); ?> <?php endif; ?> </span> <span id="pencil-click-job" style="display:inline-block;font-size:12px;"><i onclick="changejobTitle(1)"  class="fas fa-pencil-alt"></i></span>



            </label>
            <hr>
            <label>
                <span>Company*</span>

                <span id="company_edit" style="font-size:13px;display:inline-block;margin-right: 20px;">   <?php if(Auth::user()->company->id  ): ?> <?php echo e(Auth::user()->company->name); ?> <?php endif; ?> </span> <span id="pencil-click-company" style="display:inline-block;font-size:12px;;"><i onclick="changeCompany(1)"  class="fas fa-pencil-alt"></i></span>

            </label>
            <hr>
            <label>
                <span>Country*</span>
                <span id="country_edit" style="font-size:13px;display:inline-block;margin-right: 20px;">   <?php if(Auth::user()->country->id  ): ?> <?php echo e(Auth::user()->country->name); ?> <?php endif; ?> </span> <span id="pencil-click-country" style="display:inline-block;font-size:12px;;"><i onclick="changeCountry(1)"  class="fas fa-pencil-alt"></i></span>

            </label>
<hr>
            <label>
                <span><span style="display:inline-block;">Description </span><span id="pencil-click-desc" style="margin-left:10px;font-size:12px; display:inline-block;"><i onclick="changeDesc(1)"  class="fas fa-pencil-alt"></i></span></span>


                <span id="desc_edit" style="font-size:13px;width:100%;display:inline-block;margin-right: 20px;">  <?php if(Auth::user()->userInfo->description == ""): ?> ....  <?php else: ?> <?php echo e(Auth::user()->userInfo->description); ?>  <?php endif; ?> </span>

                
            </label>


        </div>
    </form>
</div>