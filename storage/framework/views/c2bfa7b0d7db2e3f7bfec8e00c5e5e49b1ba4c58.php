<div class="col-md-4 col-sm-12">
    <div class="connection-left-area">

        <div class="connetion-wrapper01 bg-white text-center">
            <h2 id="connection_count"><?php echo e($data['acceptedConnections']->count()); ?></h2>
            <div class="text">Your connections</div>
            <?php if($data['acceptedConnections']->count() != 0): ?>
            <a data-toggle="modal" data-target="#not_found" >See all</a>
            <ul>
                <?php if($data['acceptedConnections']->count() > 0): ?>

                <?php $__currentLoopData = $data['acceptedConnections']->orderBy('id','desc')->take(3)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $accepted): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><a data-toggle="modal" data-target="#not_found" ><img src="<?php echo e(asset($accepted->user->profile_image)); ?>" alt="profile"></a></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>


            </ul>
                <?php endif; ?>
        </div>

        <div class="connetion-wrapper02 bg-white text-center">
            <div class="text">Your contact import is ready Connect with your contacts and never lose touch</div>
            <div><button class="btn-small" data-toggle="modal" data-target="#not_found" >Contiune</button></div>
            <div><a  data-toggle="modal" data-target="#not_found"  >More options</a></div>
        </div>

    </div>
</div>