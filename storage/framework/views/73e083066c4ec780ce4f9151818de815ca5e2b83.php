<?php $__env->startSection('title', Auth::user()->name." - Profile"); ?>
<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('user.includes.developerjs', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo $__env->make('user.includes.sidebar.profile-public-sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="col-lg-8 col-md-7 col-sm-12">

        <div class="public_view_main">

            <div class="row">

                <div class="col-xs-12">
                    <div class="public_view_sec1">

                    <?php 
                    $backgroundPath= 'url("'.asset($data['user']->userInfo->cover_image).'")'
                    ?>
                        <div class="public_view_img" >
                        <div class="public_p" id="preview"><span id="image_upload_preview" style="width: 100%;height: 250px;background:<?php echo e($backgroundPath); ?>;background-size:cover;display:inline-block;"></span>
                        </div> <?php if(Auth::user()->id == $data['user']->id): ?>
                           <div class="edit-pencil2">
                                <label>

                                <input type="file" id="filePost" name="user_cover_images" onchange="changeCover()" accept="image/*">
                                <i class="fas fa-pencil-alt"></i>

                                </label>
                                <script>
                                    var fileInput = document.getElementById('filePost');
                                    var preview = document.getElementById('preview');

                                    fileInput.addEventListener('change', function (e) {//alert(preview);
                                        var url = URL.createObjectURL(e.target.files[0]);

										bgUrl = `url(`+url+`)`;
                preview.innerHTML = `<span id="image_upload_preview" style="width: 100%;height: 250px;background:`+bgUrl+`;background-size:cover;display:inline-block;" alt="error img">`;
                                    });

                                </script>
                                
                                
                            </div><?php endif; ?>
                        </div>
                        
                        <div class="public_in_main">
                            <div class="public_content_main">

                            <span class="public_user_img"><img src="<?php echo e(asset($data['user']->profile_image)); ?>"
                                                               alt="error img"></span>
                                <h2 class="public_user_name"><?php echo e($data['user']->name); ?> </h2>
                                <p class="public_user_content"><?php echo e($data['user']->company->name); ?></p>
                                <span class="public_user_state"> <?php echo e($data['user']->country->name); ?></span>
                                <ul class="public_sec1_ul">
                                    <?php if(Auth::user()->id != $data['user']->id): ?>
                                    <li>
                                        <?php if(!empty($data['request_status'])): ?>
                                        <?php if($data['request_status']->status != "Pending" && $data['request_status']->status != "Accepted"): ?>
                                            <a id="connect_btn"  onclick="sendRequest(<?php echo e($data['user']->id); ?>)">Connect</a>
                                            <?php elseif($data['request_status']->status == "Pending" && $data['request_status']->accept_id != Auth::user()->id && $data['request_status']->send_id == Auth::user()->id): ?>
                                            <a style="background:none !important; box-shadow:none; border: 1px solid #7bb82f;" id="connect_btn" class="clr-green" >Pending</a>
                                            <?php elseif($data['request_status']->status == "Pending" && $data['request_status']->accept_id == Auth::user()->id): ?>
                                                <a id="connect_btn"  onclick="acceptRequest(<?php echo e($data['request_status']->id); ?>)">Accept</a>
                                        <?php elseif($data['request_status']->status == "Accepted"): ?>
                                            <a id="connect_btn" >Accepted</a>
                                            
                                                
                                        <?php endif; ?>

                                            <?php else: ?>

                                            <a id="connect_btn"  onclick="sendRequest(<?php echo e($data['user']->id); ?>)">Connect</a>
                                            <?php endif; ?>
                                    </li>
                                    <?php endif; ?>
                                    <li><a data-toggle="modal" data-target="#not_found">Message</a></li>
                                    <li><a data-toggle="modal" data-target="#not_found">More</a></li>
                                </ul>
                            </div>
                            <ul class="public_sec1_ul2">
                                <li>
                                <span class="public_icon"><img
                                            src="<?php echo e(asset('energy-zone/user/assets/images/ogd-icon.png')); ?>"
                                            alt="error img"></span>
                                    <span class="public_icon_text">OGDCL UAE</span>
                                </li>
                                <li>
                                <span class="public_icon"><img
                                            src="<?php echo e(asset('energy-zone/user/assets/images/contact_icon.png')); ?>"
                                            alt="error img"></span>
                                    <span style="cursor: pointer;" class="public_icon_text" data-toggle="modal" data-target="#contact_info" >See contact info</span>
                                </li>
                                <li>
                                <span class="public_icon"><img
                                            src="<?php echo e(asset('energy-zone/user/assets/images/univ-icon.png')); ?>"
                                            alt="error img"></span>
                                    <span class="public_icon_text"><?php if($data['user']->education()->count() !=0): ?> <?php echo e($data['user']->education->first()->educationSchool->name); ?> <?php else: ?>
                                            .... <?php endif; ?></span>
                                </li>
                                <li>
                                <span class="public_icon"><img
                                            src="<?php echo e(asset('energy-zone/user/assets/images/connection.png')); ?>"
                                            alt="error img"></span>
                                    <span class="public_icon_text">connections (<?php echo e($data['user']->acceptedConnections()->get()->count()); ?>)</span>
                                </li>
                            </ul>
                            <hr>
                            <div><?php echo e($data['user']->userInfo->description); ?></div>
                        </div>
                    </div>
                </div>
                <?php if(Auth::user()->id != $data['user']->id): ?>
                <div class="col-xs-12">
                    <div class="highligts_sec">
                        <h2 class="highlights_heading">Mutual Friends</h2>
                        <div class="connection_div">
                            <ul class="connection_ul">
                                <?php if($data['mutual_friends']['mutual_friends_count'] > 0): ?>
                                    <?php $__currentLoopData = $data['mutual_friends']['mutual_friends']->take(2)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mutual): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><img src="<?php echo e(asset($mutual->user->profile_image)); ?>"
                                         alt="error img"></li>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                    <img style="width:50px;" src="<?php echo e(asset('energy-zone/no-user.png')); ?>"
                                         alt="error img">
                                    <?php endif; ?>
                            </ul>
                            <div class="connection_right_text">
                                <h3 class="mutual_text"><?php echo e($data['mutual_friends']['mutual_friends_count']); ?> Mutual Connections</h3>
                                <span class="mutual_pera"><?php echo e($data['mutual_friends']['mutual_friends_public_profile']); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
<?php endif; ?>

                <?php echo $__env->make('user.includes.public-profile.experience-public-profile-section', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>


                <?php echo $__env->make('user.includes.public-profile.education-public-profile-section', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>


                <div class="col-xs-12">
                    <div class="ah-main-skills-wrapper">

                       <div  class="ah-skills-01 bg-white">
                        <div class="our-heading"><h3>Skills & Endorsements</h3></div>
                        <ul  >
                            <?php if($data['user']->skills()->count() !=0): ?>
                                <?php $__currentLoopData = $data['user']->skills; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $skill): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <h3><?php echo e($skill->name); ?>

                                            <small>.32</small>
                                        </h3>
                                        <div class="text">See 32 endorsements for Supply Chain Management32</div>
                                    </li>


                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <!-- <li class="text-center">
                                        <a href="#">See more</a>
                                    </li>-->

                            <?php else: ?>
                                <li style="padding: 20px 25px;">      No Skills Added till yet.</li>
                            <?php endif; ?>


                        </ul>

                        <?php if($data['user']->skills()->count() > 3): ?>
                            <div class="see_more_main text-center">
                                <a href="javascript:void(0)" id="see_more">See more</a>
                            </div>
                        <?php endif; ?>

                    </div>
                    </div>
                        <div class="ah-main-skills-wrapper">
                        <div class="ah-skills-02 bg-white">
                            <div class="our-heading"><h3>Interests</h3></div>
                            <div class="clearfix">
                                <ul>
                                    <li class="clearfix">
                                        <div class="in-left">
                                            <img src="<?php echo e(asset('energy-zone/user/assets/images/dummy-img.png')); ?>"
                                                 alt="img">
                                        </div>
                                        <div class="in-right">
                                            <h3>Buyer-World EUROPE - NORTH ...</h3>
                                            <div class="text">23,000 Members</div>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="in-left">
                                            <img src="<?php echo e(asset('energy-zone/user/assets/images/dummy-img.png')); ?>"
                                                 alt="img">
                                        </div>
                                        <div class="in-right">
                                            <h3>Buyer-World EUROPE - NORTH ...</h3>
                                            <div class="text">23,000 Members</div>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="in-left">
                                            <img src="<?php echo e(asset('energy-zone/user/assets/images/dummy-img.png')); ?>"
                                                 alt="img">
                                        </div>
                                        <div class="in-right">
                                            <h3>Buyer-World EUROPE - NORTH ...</h3>
                                            <div class="text">23,000 Members</div>
                                        </div>
                                    </li>
                                </ul>
                                <ul>
                                    <li class="clearfix">
                                        <div class="in-left">
                                            <img src="<?php echo e(asset('energy-zone/user/assets/images/dummy-img.png')); ?>"
                                                 alt="img">
                                        </div>
                                        <div class="in-right">
                                            <h3>Buyer-World EUROPE - NORTH ...</h3>
                                            <div class="text">23,000 Members</div>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="in-left">
                                            <img src="<?php echo e(asset('energy-zone/user/assets/images/dummy-img.png')); ?>"
                                                 alt="img">
                                        </div>
                                        <div class="in-right">
                                            <h3>Buyer-World EUROPE - NORTH ...</h3>
                                            <div class="text">23,000 Members</div>
                                        </div>
                                    </li>

                                    <li class="clearfix">
                                        <div class="in-left">
                                            <img src="<?php echo e(asset('energy-zone/user/assets/images/dummy-img.png')); ?>"
                                                 alt="img">
                                        </div>
                                        <div class="in-right">
                                            <h3>Buyer-World EUROPE - NORTH ...</h3>
                                            <div class="text">23,000 Members</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="seemorebutton text-center"><a href="#">See all</a></div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="contact_info" role="dialog">
        <div class="general_popup_in">
            <span class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></span>
            <h1 class="general_popup_heading">Contact Info</h1>
            <div class="popup_white_sec">

                <ul class="popup_ul">

                    <li>
                        <label>Email</label>
                        <label><?php echo e($data['user']->email); ?></label>
                    </li>
                </ul>

            </div>
        </div>
    </div>

<?php echo $__env->make('user.includes.public-profile.public-profile-js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>