<div class="adding-wrapper bg-white">
    <div class="our-heading clearfix">
        <h3>Skills</h3>
        <a class="btn-small"  data-toggle="modal" data-target="#editSkill">Add Skills</a>
    </div>

    <div class="text"><?php if(Auth::user()->skills()->count() ==0): ?> You haven't added any skill yet. <?php else: ?>

                          <?php $__currentLoopData = Auth::user()->skills; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $skill): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                              <a href="<?php echo e(route('user.delete.skill',['id'=>$skill->id])); ?>" class="label label-primary new_btn_icn"><?php echo e($skill->name); ?> <i class="fa fa-times"></i> </a>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <?php endif; ?></div>
</div>

<?php echo $__env->make('user.includes.popups.edit-skill-popup', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
