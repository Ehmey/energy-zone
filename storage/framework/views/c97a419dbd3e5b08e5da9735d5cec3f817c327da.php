<div class="col-lg-4 col-md-5 col-sm-12">
    <div class="row">
        <div class="col-xs-12">
            <div class="profile_sec1_left">
                <span class="profile_pic"><img
                            src="<?php if(Auth::user()->profile_image != null): ?> <?php echo e(asset(Auth::user()->profile_image)); ?> <?php else: ?>  <?php echo e(asset('energy-zone/userIcon.png')); ?> <?php endif; ?>"
                            alt="profile pic"></span>
                <div class="profile_sec1_right">
                    <h4 class="tech_heading"><a
                                href="<?php echo e(route('user.public-profile',[Auth::user()->id])); ?>"><?php echo e(Auth::user()->name); ?></a>
                    </h4>
                    <span class="exclusive_digital"><?php echo e(Auth::user()->company->name); ?></span>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <ul class="viewed_profile_ul">
                <li>
                    <span class="viewed_text">14</span>
                    <span class="profile_view">Who's viewed <br>your profile</span>
                </li>
                <li>
                    <span class="viewed_text"><?php if( !empty($data['acceptedConnections'])): ?> <?php echo e($data['acceptedConnections']->count()); ?> <?php else: ?>
                            0 <?php endif; ?> </span>
                    <span class="profile_view">Connections<br><a data-toggle="modal" data-target="#not_found">Grow your network</a></span>
                </li>
            </ul>
        </div>
        <div class="col-xs-12">
            <div class="upgrade_sec">
                <p class="access_pera">Access exclusive tools &amp; insights Free Upgrade to Premium</p>
                <a data-toggle="modal" data-target="#not_found" class="upgrade_btn">Upgrade</a>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="suggestion_tab_main">
                <span class="suggestion_text">People You May Know <i class="fas fa-info-circle"></i></span>
                <ul class="ad_your_feed">

                    <?php $__currentLoopData = $recent_users['user']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $recent_user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(isset($recent_user[0]->JobTitle->name)): ?>
                            <li>
                                <div class="ad_fed_left">
                                    <span class="feed_img"><img src="<?php echo e(asset($recent_user[0]->profile_image)); ?>"
                                                                alt="error img"></span>
                                    <div class="feed_right">
                                        <span class="fed_nam"><a href="<?php echo e(route('user.public-profile',[$recent_user[0]->id])); ?>"><?php echo e($recent_user[0]->name); ?></a></span>
                                        <span class="chairman_oil"><?php echo e($recent_user[0]->JobTitle->name); ?>

                                            , <?php echo e($recent_user[0]->company->name); ?></span>
                                    </div>
                                </div>
                                <a data-toggle="modal" data-target="#not_found" class="feed_follow_btn">+ Follow</a>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                </ul>
                <a data-toggle="modal" data-target="#not_found" class="feed_view_All">View all recommendations</a>
            </div>
        </div>
    </div>
</div>