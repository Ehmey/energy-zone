
<?php echo $__env->make('alerts.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<form  method="post" action="<?php echo e(route('user.step')); ?>" enctype='multipart/form-data'>
    <?php echo csrf_field(); ?>
    <input type="text" name="step" value="3" style="display: none;">
    <input  type="text" name="user_id" value="<?php echo e(app('request')->input('id')); ?>" style="display: none;">
    <div class="inner-form-content verification-input">
        <h3 class="clr-blue">Authentication Code </h3>
        <div class="text">Enter the 6-digit code that we sent to<br> <?php echo e(\App\user::find(app('request')->input('id'))->email); ?></div>
        <input type="text" name="auth_code" id="verification-input"  placeholder="------" maxlength="8">
    </div>
    <div class="site-btn"><button type="submit">Next</button></div>
</form>



