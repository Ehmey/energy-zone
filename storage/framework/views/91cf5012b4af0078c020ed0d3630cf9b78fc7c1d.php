<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding:0px;background:#f2f2f2">
    <tbody>
    <tr>
        <td style="background-color:#fff;">
            <table cellpadding="0" cellspacing="0" align="center" width="100%">
                <tbody>
                <tr>
                    <td>
                        <table bgcolor="#FFFFFF" align="center" border="0" cellpadding="10" cellspacing="0" width="600" style="border:1px solid #000;">
                            <tbody>
                            <tr>
                                <td align="center" valign="top" style="padding:0px;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="600">
                                        <tbody>

                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="600">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center" valign="top" width="600">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td align="center" valign="top">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" style="padding:60px 40px 50px;;background-size:100%;" width="100%">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td valign="top" align="center">
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tbody>
                                                                                                    <tr align="center">
                                                                                                        <td style="width:100%" align="left">
                                                                                                            <a href="#"><img width="218" alt="Welcome mail" title="Welcome mail" src="<?php echo e(asset('energy-zone/user/images/logo.png')); ?>" class="CToWUd"></a>
                                                                                                        </td>

                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr align="center">
                                                                                <td style="padding:40px 40px 0px; background:#f9fbfc;">
                                                                                    <div style="padding:0px 0px 40px 0px;margin:0px;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;line-height:25px;font-size:32px">
                                                                                        <span style="color:#283a84;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;">Welcome!</span>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr align="center">
                                                                                <td style="padding:0px 40px 0px; background:#f9fbfc;">
                                                                                    <div style="padding:0px 0px 40px 0px;margin:0px;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;line-height:25px;font-size:14px">
                                                                                        <span style="color:#767676;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;">Thank you for siging up with TheAdmin! We hope you enjoy your time with us. Check your account and update your profile.</span>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr align="center">
                                                                                <td style="padding:0px 40px 0px; background:#f9fbfc;">
                                                                                    <div style="padding:0px 0px 40px 0px;margin:0px;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;line-height:25px;font-size:14px">
                                                                                        <span style="color:#767676;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;">If your have any questions.just reply to this email-we're alwasy happy to help out.</span>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr align="center">
                                                                                <td style="padding:0px 40px 0px; background:#f9fbfc;">
                                                                                    <div style="padding:0px 0px 40px 0px;margin:0px;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;line-height:25px;font-size:14px;font-weight:bold;">
                                                                                        <span style="color:#000;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;">Cheers,<br><?php echo e($data); ?></span>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="background-color:#fff;padding:30px 40px; border-top:1px solid #d4dbde;" align="center">
                                                                                    <table cellpadding="0" cellspacing="0" style="margin:0px 0px 0px;">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td style="font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;width:720px;font-size:16px;color:#000; font-weight:bold; line-height:15px;" valign="middle" align="center"> Need more help?</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;font-size:16px;color:#283a84; font-weight:bold; line-height:15px; padding-top:10px;" valign="middle" align="center"> We’re here, ready to talk</td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>