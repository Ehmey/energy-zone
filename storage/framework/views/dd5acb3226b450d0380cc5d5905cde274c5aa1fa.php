<div class="fixed-left-menu-wrapper" id="sliding">
    <ul>
        <li class="text-center"><a href="index.html"><img src="<?php echo e(asset('energy-zone/user/images/logo.png')); ?>" alt="logo"></a></li>
        <li class=" <?php echo $__env->yieldContent('settings_active'); ?> ">
            <a  class=" <?php echo $__env->yieldContent('settings_active'); ?> " data-toggle="collapse" data-target="#settings" ><i class="fas fa-cog"></i> <div class="hide-wraper">Site Settings</div>  </a>
            <ul id="settings" class="collapse">
                <li><a href="<?php echo e(route('admin.profile.settings')); ?>"><i class="fas fa-user-tie"></i> User Profile Settings</a></li>
                <li><a href="<?php echo e(route('admin.general.settings')); ?>"><i class="fas fa-user-tie"></i> General Settings</a></li>
            </ul>
        </li>

        <li class=" <?php echo $__env->yieldContent('users_active'); ?> ">
            <a  class=" <?php echo $__env->yieldContent('users_active'); ?> "  href="<?php echo e(route('admin.users.management')); ?>" ><i class="fas fa-users"></i><div class="hide-wraper">Users Management</div>  </a>

        </li>


        <li >
            <a data-toggle="collapse" data-target="#demo1" href="#" ><i class="fas fa-th-large"></i> <div class="hide-wraper">Dashboard</div> <span class="fas fa-caret-right"></span></a>
            <ul id="demo1" class="collapse">
                <li><a href="skills.html"><i class="fas fa-user-tie"></i> Skills</a></li>
                <li><a href="school.html"><i class="fas fa-user-tie"></i> School</a></li>
                <li><a href="degree.html"><i class="fas fa-user-tie"></i> Degree</a></li>
                <li><a href="job-title.html"><i class="fas fa-user-tie"></i> Job Title</a></li>
                <li><a href="companies.html"><i class="fas fa-user-tie"></i> Companies</a></li>
                <li><a href="country.html"><i class="fas fa-user-tie"></i> Countries</a></li>
            </ul>
        </li>

        <li>
            <a data-toggle="collapse" data-target="#demo3" href="#"><i class="fas fa-bell"></i> <div class="hide-wraper">Notification</div> <span class="fas fa-caret-right"></span></a>
            <ul id="demo3" class="collapse">
                <li><a href="#"> <i class="fas fa-user-tie"></i>Menu</a></li>
                <li><a href="#"> <i class="fas fa-user-tie"></i>Menu</a></li>
                <li><a href="#"> <i class="fas fa-user-tie"></i>Menu</a></li>
                <li><a href="#"> <i class="fas fa-user-tie"></i>Menu</a></li>
                <li><a href="#"> <i class="fas fa-user-tie"></i>Menu</a></li>
            </ul>
        </li>
        <li>
            <a data-toggle="collapse" data-target="#demo4" href="#"><i class="fas fa-envelope"></i> <div class="hide-wraper">Message</div> <span class="fas fa-caret-right"></span></a>
            <ul id="demo4" class="collapse">
                <li><a href="#"> <i class="fas fa-user-tie"></i>Menu</a></li>
                <li><a href="#"> <i class="fas fa-user-tie"></i>Menu</a></li>
                <li><a href="#"> <i class="fas fa-user-tie"></i>Menu</a></li>
                <li><a href="#"> <i class="fas fa-user-tie"></i>Menu</a></li>
                <li><a href="#"> <i class="fas fa-user-tie"></i>Menu</a></li>
            </ul>
        </li>
        <li>
            <a data-toggle="collapse" data-target="#demo5" href="#"><i class="fas fa-briefcase"></i> <div class="hide-wraper">Job Posting</div> <span class="fas fa-caret-right"></span></a>
            <ul id="demo5" class="collapse">
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
            </ul>
        </li>
        <li>
            <a data-toggle="collapse" data-target="#demo6" href="#"><i class="fas fa-lock"></i> <div class="hide-wraper">Security</div> <span class="fas fa-caret-right"></span></a>
            <ul id="demo6" class="collapse">
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
            </ul>
        </li>
        <li>
            <a data-toggle="collapse" data-target="#demo7" href="#"><i class="fas fa-sign-out-alt"></i> <div class="hide-wraper">Logout</div> <span class="fas fa-caret-right"></span></a>
            <ul id="demo7" class="collapse">
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
                <li><a href="#"><i class="fas fa-user-tie"></i> Menu</a></li>
            </ul>
        </li>
    </ul>
</div>