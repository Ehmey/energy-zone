<?php echo $__env->make('alerts.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<form method="post" action="<?php echo e(route('user.for-social-login-update-info')); ?>" enctype='multipart/form-data'>
    <?php echo csrf_field(); ?>

    <input name="step" value="1" type="text" style="display:none;">
    <input name="social" value="1" type="text" style="display:none;">
    <input name="user_id"value="<?php echo e($data['user']->id); ?>" type="text" style="display:none;">
    <input type="text" name="name" value="<?php echo e($data['user']->name); ?>" placeholder="Name" required readonly>
    <?php if($errors->has('name')): ?>
        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
    <?php endif; ?>

    <input type="email" name="email" placeholder="Email" value="<?php echo e($data['user']->email); ?>" required readonly>
    <?php if($errors->has('email')): ?>
        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
    <?php endif; ?>
    <input type="password" name="password" placeholder="Password" required>
    <?php if($errors->has('password')): ?>
        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
    <?php endif; ?>

    <div class="site-btn">
        <button type="submit">Next</button>
    </div>
</form>
