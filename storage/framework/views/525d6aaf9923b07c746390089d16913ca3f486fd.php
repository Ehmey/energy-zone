<div class="main-header-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-3 col-sm-12">
                <div class="site-logo-here">

                    <a href="<?php echo e(route('profile')); ?>"><img src="<?php echo e(asset('energy-zone/user/images/logo.png')); ?>" alt="logo"></a>

                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <div class="main-header-search">

                    <form>
                        <input type="text" placeholder="Search for people and companies">
                        <button type="submit" class="fa fa-search"></button>
                    </form>

                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="main-header-profile">

                    <ul>
                        <li>
                            <a href="#">
                                <i class="fas fa-envelope"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-bell"></i>
                                <span>1</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo e(route('user.connections')); ?>">
                                <i class="fas fa-user-plus"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php if(Auth::user()->profile_image != null): ?> <?php echo e(asset(Auth::user()->profile_image)); ?> <?php else: ?>  <?php echo e(asset('energy-zone/userIcon.png')); ?> <?php endif; ?>" alt="profile">
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo e(route('user.edit-profile')); ?>"><i class="fas fa-pencil-alt"></i> &nbsp; Edit Profile</a></li>
                                <li><a href="<?php echo e(route('user.public-profile', ['id' => Auth::user()->id])); ?>"><i class="fas fa-eye"></i> &nbsp;View Profile </a></li>
                                <li><a href="<?php echo e(route('logout')); ?>"><i class="fas fa-lock"></i> &nbsp; Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>