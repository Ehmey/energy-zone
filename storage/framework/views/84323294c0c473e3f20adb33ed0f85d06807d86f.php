<div class="modal fade" id="addEducation" role="dialog">
    <div class="general_popup_in">
        <span class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></span>
        <h1 class="general_popup_heading">Add Education</h1>
        <div class="popup_white_sec">
            <form method="post" action="<?php echo e(route('user.add.education')); ?>">
                <?php echo csrf_field(); ?>
                <ul class="popup_ul">

                    <li>
                        <label>School</label>
                        <select name="school_id">

                            <?php $__currentLoopData = $data['schools']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $school): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($school->id); ?>" ><?php echo e($school->name); ?> </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </li>

                    <li>
                        <label>Degree</label>
                        <select name="degree_id">
                            <?php $__currentLoopData = $data['degrees']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $degree): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($degree->id); ?>"><?php echo e($degree->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </li>

                    <li>
                        <label>Activities and societies</label>
                        <textarea name="activities" required></textarea>
                    </li>

                </ul>
                <input type="submit" value="Save" class="popup_save_btn">
            </form>
        </div>

    </div>
</div>