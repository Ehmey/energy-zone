<div class="col-md-4">
    <div class="edit-profile-left-area">

        <div class="edit-wrapper01 bg-white text-center">
            <div class="text">Want to connect with people who are not in your connections list? Here is the way</div>
            <a class="btn-small" data-toggle="modal" data-target="#not_found" >Renew</a>
        </div>

        <div class="edit-wrapper02 bg-white">
            <div class="our-heading"><h3>Joined Groups</h3></div>
            <div class="text">You haven't joined any group yet.</div>
        </div>

        <div class="edit-wrapper02 bg-white">
            <div class="our-heading"><h3>Following Companies</h3></div>
            <div class="text">You are not following any companies.</div>
        </div>

        <div class="edit-wrapper02 bg-white">
            <div class="our-heading"><h3>Applied Jobs</h3></div>
            <div class="text">You haven't applied for any jobs yet.</div>
        </div>

    </div>
</div>