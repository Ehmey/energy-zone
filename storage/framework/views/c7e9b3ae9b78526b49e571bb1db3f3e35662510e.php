
    <div class="container-fluid">
        <div class="row clearfix">

            <div class="top-left-header">
                <ul>
                    <li><a onclick="slide_menu()" href="#"><i class="fas fa-bars"></i></a></li>
                    <li>
                        <a href="#">Activity</a>
                        <ul>
                            <li><a href="#">Menu</a></li>
                            <li><a href="#">Menu</a></li>
                            <li>
                                <a href="#">Submenu <i class="fas fa-caret-right"></i></a>
                                <ul>
                                    <li><a href="#">Menu</a></li>
                                    <li><a href="#">Menu</a></li>

                                    <li>
                                        <a href="#">Submenu <i class="fas fa-caret-right"></i></a>
                                        <ul>
                                            <li><a href="#">Menu</a></li>
                                            <li><a href="#">Menu</a></li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>
                            <li><a href="#">Menu</a></li>
                            <li><a href="#">Menu</a></li>
                            <li>
                                <a href="#">Submenu <i class="fas fa-caret-right"></i></a>
                                <ul>
                                    <li><a href="#">Menu</a></li>
                                    <li><a href="#">Menu</a></li>
                                    <li><a href="#">Menu</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="top-right-header">
                <ul>
                    <li><a href="#"><i class="fas fa-search"></i></a></li>
                    <li><a href="#"><i class="fas fa-envelope"></i></a></li>
                    <li><a href="#"><i class="fas fa-bell"></i><span>0</span></a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="<?php echo e(asset(Auth::user()->profile_image)); ?>" alt="profile"></a>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="fas fa-pencil-alt"></i> &nbsp;&nbsp;Edit</a></li>
                            <li><a href="#"><i class="fas fa-cog"></i> &nbsp;&nbsp;Setting</a></li>
                            <li><a href="<?php echo e(route('admin.logout')); ?>"><i class="fas fa-sign-out-alt"></i> &nbsp;&nbsp;Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

        </div>
    </div>
