<script>

    function sendRequest(acceptId){
        document.getElementById('connect_btn').innerHTML ="Pending";
        document.getElementById('connect_btn').style.backgroundColor  ="transparent";
        document.getElementById('connect_btn').style.border ="1px solid #7bb82f";
        document.getElementById('connect_btn').classList.add("clr-green");

        baseUrl = `<?php echo e(url("user/")); ?>`+'/'+acceptId+`/send-request`;

        $.get(baseUrl, function (data, status) {


        });
    }


    function acceptRequest(id){
        document.getElementById('connect_btn').innerHTML = "Accepted";

        baseUrl = `<?php echo e(url("user/accept_invitation/")); ?>`+'/'+id;

        $.get(baseUrl, function (data, status) {


        });
    }

</script>