<nav class="main-site-menu-wrapper">
    <div class="contaner-fluid">

        <div class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-nav-bar1">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>

            <div class="collapse navbar-collapse main-nav-bar1 removepadd">

                <ul class="menu nav navbar-nav" id="navigation-bar">

                    <li><a href="<?php echo e(route('profile')); ?>"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="<?php echo e(route('user.public-profile',[Auth::user()->id])); ?>"><i class="fas fa-user"></i> Profile</a></li>
                    <li><a href="<?php echo e(route('user.connections')); ?>"><i class="fas fa-users"></i> Connections</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#not_found" ><i class="fas fa-briefcase"></i> Jobs</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#not_found" ><i class="fas fa-star"></i> Interests</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#not_found" ><i class="fas fa-home"></i> Groups</a></li>

                </ul>

            </div>
        </div>

    </div>
</nav>