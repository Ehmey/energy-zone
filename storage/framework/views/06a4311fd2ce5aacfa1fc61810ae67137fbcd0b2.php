<div class="adding-wrapper <?php if($data['user_education']->count() == 0): ?> bg-white <?php endif; ?>" style="padding-bottom:0px;">
    <div class="our-heading clearfix">
        <h3>Education</h3>
        <a class="btn-small" data-toggle="modal" data-target="#addEducation">Add Education</a>
    </div>

    <?php if($data['user_education']->count() != 0): ?>



        <?php $__currentLoopData = $data['user_education']->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $education): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <div class="edit-profile03">
                <div style="margin:0px;" class="hr-accontant bg-white">
                    <div class="clearfix">
                        <div class="in-left">
                            <img src="<?php echo e(url($education->educationSchool->first()->image_path)); ?>" alt="img">
                        </div>
                        <div class="in-right">

                            <h3><?php echo e($education->educationSchool->name); ?>   </h3>
                            <h4><?php echo e($education->educationDegree->name); ?>    </h4>
                            <div class="text" style="padding:0px;"><?php echo e($education->activities); ?>

                            </div>
                        </div>
                    </div>
                    <div class="adit-del">
                        <a data-toggle="modal" data-target="#editEducation_<?php echo e($education->id); ?>"><i class="fas fa-pencil-alt"></i></a>
                        <a href="<?php echo e(route('user.delete.education',['id'=>$education->id])); ?>"><i
                                    class="fas fa-trash-alt"></i></a>
                    </div>
                </div>
            </div>

            

            
            
            <?php echo $__env->make('user.includes.popups.edit-education-popup', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <div class="text">
            You haven't added any education yet.
        </div>
    <?php endif; ?>
</div>

<?php echo $__env->make('user.includes.popups.add-education-popup', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>