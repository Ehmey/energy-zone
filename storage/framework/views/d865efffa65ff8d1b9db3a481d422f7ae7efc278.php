<?php $__env->startSection('title', "Dashboard"); ?>
<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('user.includes.sidebar.profile-dashboard-sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('user.includes.developerjs', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="col-lg-8 col-md-7 col-sm-12">

        <div class="row">
            <div class="col-xs-12">
                <div class="pd_right_sec1">
                    <?php echo $__env->make('user.includes.forms.post-form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
            <div class="col-xs-12">
                <ul class="profile_db_comments_ul" id="posts_url">
                    <?php if($data['posts']->count() == 0): ?>
                    <li id="no_pst" style="text-align:center;"><br><br><img style="margin:0 auto;" src="<?php echo e(asset('energy-zone/post/no_post.png')); ?>">
                    <br><br><p class="access_pera">No Posts to Show</p>
                    </li>
                        <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>

    <script>loadposts();</script>
    <?php echo $__env->make('user.includes.posts.postsJs', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>