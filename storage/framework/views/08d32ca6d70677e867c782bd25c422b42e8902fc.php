<div class="modal fade" id="editExperince_<?php echo e($experince->id); ?>" role="dialog">
    <div class="general_popup_in">
        <span class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></span>
        <h1 class="general_popup_heading">Edit Experience</h1>
        <div class="popup_white_sec">
            <form method="post" action="<?php echo e(route('user.edit.experience')); ?>">
                <input style="display:none" name="user_experience_id" value="<?php echo e($experince->id); ?>">
                <?php echo csrf_field(); ?>
                <ul class="popup_ul">

                    <li>
                        <label>Company</label>
                        <select name="company_id">

                            <?php $__currentLoopData = $data['companies']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($company->id); ?>" <?php if($company->id == $experince->experinceCompany->id): ?> selected <?php endif; ?>><?php echo e($company->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </li>

                    <li>
                        <label>Job Title</label>
                        <select name="job_title_id">
                            <?php $__currentLoopData = $data['job_title']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $job_title): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($job_title->id); ?>" <?php if($job_title->id == $experince->experinceJobTitle->id): ?> selected <?php endif; ?>><?php echo e($job_title->name); ?>  </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </li>

                    <li>
                        <label>How was your experince?</label>
                        <textarea name="experience" required  ><?php echo e($experince->experince); ?></textarea>
                    </li>

                </ul>
                <input type="submit" value="Save" class="popup_save_btn">
            </form>
        </div>

    </div>
</div>