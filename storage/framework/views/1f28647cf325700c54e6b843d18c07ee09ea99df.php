<?php $__env->startSection('title', "Edit Profile"); ?>
<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('user.includes.sidebar.edit-profile-sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="col-md-8">

        <div class="edit-profile-right-area">

            <div class="edit-profile01 bg-white clearfix">
                <div class="clearfix">
                    <div class="in-left">
                        <div class="ed-profile">
                           <div id="img_preview"> <img  src="<?php if(Auth::user()->profile_image != null): ?> <?php echo e(asset(Auth::user()->profile_image)); ?> <?php else: ?>  <?php echo e(asset('energy-zone/userIcon.png')); ?> <?php endif; ?>"
                                 alt="img"></div>
                            <div class="edit-pencil">
                                <label>
                                    <input type="file" id="image_profile" name="image_profile" onchange="editProfileImage()" accept="image/*">
                                    <i class="fas fa-pencil-alt"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="in-right">
                        <h3> <span id="user_name"> <?php echo e(Auth::user()->name); ?> </span> <span id="pencil-click"><i onclick="changeName(1)" class="fas fa-pencil-alt"></i></span></h3>
                    </div>
                </div>
                <div class="connection"><a href="<?php echo e(route('user.public-profile',[Auth::user()->id])); ?>" style="margin-right:20px;">View Profile</a><a href="<?php echo e(route('user.connections')); ?>">Connections</a></div>
            </div>


            <?php echo $__env->make('user.includes.edit-profile.edit-basic-info-section', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>


            <div class="edit-profile04">




                <?php echo $__env->make('user.includes.edit-profile.experince-section', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>


                <?php echo $__env->make('user.includes.edit-profile.education-section', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <?php echo $__env->make('user.includes.edit-profile.language-section', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <?php echo $__env->make('user.includes.edit-profile.skill-section', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>


            </div>

        </div>
    </div>
    <?php echo $__env->make('user.includes.edit-profile.edit-profile-js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>