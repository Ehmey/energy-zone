<div class="adding-wrapper bg-white">
    <div class="our-heading clearfix">
        <h3>Languages</h3>
        <a class="btn-small" data-toggle="modal" data-target="#editLanguage">Add Languages</a>
    </div>
    <div class="text"><?php if(Auth::user()->languages()->count() ==0): ?> You haven't added any Languages yet. <?php else: ?>

            <?php $__currentLoopData = Auth::user()->languages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <a href="<?php echo e(route('user.delete.language',['id'=>$language->id])); ?>" class="label label-primary new_btn_icn"><?php echo e($language->name); ?> <i class="fa fa-times"></i> </a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  <?php endif; ?>
              </div>
</div>

<?php echo $__env->make('user.includes.popups.edit-language-popup', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>