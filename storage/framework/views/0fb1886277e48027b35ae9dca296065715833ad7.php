<?php echo $__env->make('alerts.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<form method="post" action="<?php echo e(route('user.step')); ?>" enctype='multipart/form-data'>
    <?php echo csrf_field(); ?>
    <input value="2" type="text" name="step" value="2" style="display: none;">
    <input type="text" name="user_id" value="<?php echo e(app('request')->input('id')); ?>" style="display: none;">
    <div class="off-apperance">
        <select name="country_id" required>
            <?php $__currentLoopData = $data['countries']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($country->id); ?>"><?php echo e($country->name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
    </div>
    <div class="off-apperance">
        <select name="company_id" required>
            <?php $__currentLoopData = $data['companies']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $company): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($company->id); ?>"><?php echo e($company->name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
    </div>
    <div class="off-apperance">
        <select name="job_title_id" required>

            <?php $__currentLoopData = $data['job_titles']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $job_title): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($job_title->id); ?>"><?php echo e($job_title->name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
    </div>
    <div class="site-btn">
        <button type="submit">Next</button>
    </div>
</form>
