<?php $__env->startSection('title', "General Settings"); ?>

<?php $__env->startSection('settings_active', "active_nav"); ?>

<?php $__env->startSection('content'); ?>

    <div class="manage-content-wrapper">

        <div class="dashboard-heading clearfix">
            <h2>General Settings</h2>
            <ul>
                <li>Energy Zone / General Settings</li>
            </ul>
        </div>

        <div class="content-sorting clearfix">
            <?php echo $__env->make('alerts.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="drop_down_tab_main">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">

                    <li><a href="#setups" role="tab" data-toggle="tab"  onclick="getSetupsData('schools')">Schools</a></li>
                    <li><a href="#setups" role="tab" data-toggle="tab"  onclick="getSetupsData('degrees')">Degress</a></li>



                </ul>
                <!-- Tab panes -->
                <div class="tab-content" id="setups-tab-content">
                    <div class="tab-pane active" id="setups">

                        <div id="setup-table"> </div>
                    </div>


                </div>
            </div>





        </div>

    </div>

    <?php echo $__env->make('admin.includes.popups.setup-popups', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('admin.includes.setups.setups-js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script>getSetupsData('schools');</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app-admin', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>