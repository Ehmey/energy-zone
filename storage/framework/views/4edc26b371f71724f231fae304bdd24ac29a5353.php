<form method="post" action="<?php echo e(route('user.do-post')); ?>" enctype="multipart/form-data">
    <?php echo csrf_field(); ?>
    <input style="display:none;" name="user_id" value="<?php echo e(Auth::user()->id); ?>">
    <ul class="publish_ul">
        <li id="real_upload_video">
            <label class="file_upload_btn"><input type="file" id="filePostVideo" name="videoFile"
                                                  accept="video/*"><span><i
                            class="fas fa-video"></i> Uplaod Video</span>
            </label>
        </li>


        <li id="dummy_upload_video" style="display:none;">
            <label class="file_upload_btn" style="background:#eff5f72b;cursor: not-allowed;"><span><i
                            class="fas fa-video"></i> Uplaod Video</span>
            </label>
        </li>


        <li id="real_upload_image">
            <label class="file_upload_btn">
                <input type="file" id="filePost" name="myFile" accept="image/*">
                <span>
                    <i class="fas fa-upload"></i>
                    Uplaod an Image
                </span>
            </label>
        </li>

        <li style="display:none;" id="dummy_upload_image">
            <label class="file_upload_btn"  style="background:#eff5f72b;cursor: not-allowed;">

                <span>
                    <i class="fas fa-upload"></i>
                    Uplaod an Image
                </span>
            </label>
        </li>


        <li class="dropdown">
            <a href="#" class="share_update dropdown-toggle" data-toggle="dropdown"><i
                        class="fas fa-globe"></i> Share an Update <i class="fas fa-caret-down"></i></a>
            <ul class="dropdown-menu">
                <li><a href="#">Share with friens</a></li>
                <li><a href="#">Share with friens</a></li>
                <li><a href="#">Share with friens</a></li>
            </ul>
        </li>


    </ul>
    <div class="ck_editor_main_div"><textarea id="editor1" name="description" class="publish_textarea"></textarea>

        <div id="video-preview-post" class="text-center" style="display:none;">


        </div>
        <div id="preview"></div>
    </div>


    <script>

        // $('#editor1').CKEDITOR({ contentCss: "<?php echo e(asset('energy-zon/user/front.css')); ?>" });

        CKEDITOR.replace('editor1', {
            contentsCss: "<?php echo e(asset('energy-zone/user/front.css')); ?>",
            toolbar: []
        });
    </script>


    <script>


        var fileInputVideo = document.getElementById('filePostVideo');
        var previewVideo = document.getElementById('video-preview-post');
        previewVideo.style.display = "block";
        fileInputVideo.addEventListener('change', function (e) {

            FileUploadVideoPath = fileInputVideo.value;
            var VideoExtension = FileUploadVideoPath.substring(
                FileUploadVideoPath.lastIndexOf('.') + 1).toLowerCase();
            //The file uploaded is an image
            if (VideoExtension != "png" && VideoExtension != "avi" && VideoExtension != "flv" && VideoExtension != "wmv" && VideoExtension != "mov" && VideoExtension != "mp4") {
                document.getElementById('for_alert').innerHTML = `<div class="alert alert-warning">
  <strong>Warning!</strong> Only .AVI, .FLV, .MOV, .MP4 and WMV files are allowed.
</div>`;
                setTimeout(function () {
                    $('#for_alert').fadeOut('slow');
                }, 5000);
return;
            }
                var url = URL.createObjectURL(e.target.files[0]);
            document.getElementById('real_upload_video').style.display = "none";
            document.getElementById('dummy_upload_video').style.display = "block";


            document.getElementById('real_upload_image').style.display = "none";
            document.getElementById('dummy_upload_image').style.display = "block";
                previewVideo.innerHTML = `<div class="closing" onclick="removeItemInPost('video')"><i class="glyphicon glyphicon-remove"></i></div><video  controls style="width:100%;">
                        <source src="` + url + `" type="video/mp4">
                        Your browser does not support HTML5 video.
                    </video>`;

            }
        );

    </script>


    <script>
        var fileInput = document.getElementById('filePost');


        var preview = document.getElementById('preview');

        fileInput.addEventListener('change', function (e) {


            FileUploadPath = fileInput.value;
            var Extension = FileUploadPath.substring(
                FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

            //The file uploaded is an image

            if (Extension != "png"
                && Extension != "jpeg" && Extension != "jpg") {
                document.getElementById('for_alert').innerHTML = `<div class="alert alert-warning">
  <strong>Warning!</strong> Only .PNG, .JPG, and JPEG files are allowed.
</div>`;
                setTimeout(function () {
                    $('#for_alert').fadeOut('slow');
                }, 5000);

                return;
            }

            preview.style.display ="block";
            document.getElementById('real_upload_video').style.display = "none";
            document.getElementById('dummy_upload_video').style.display = "block";


            document.getElementById('real_upload_image').style.display = "none";
            document.getElementById('dummy_upload_image').style.display = "block";

            var url = URL.createObjectURL(e.target.files[0]);
            preview.innerHTML =`<div class="closing" onclick="removeItemInPost('image')">X</div><img src="`+url+`">`;
          //  preview.setAttribute('src', url);
        });

    </script>
    <div class="share_public_drop_down_main">
        <button type="submit" class="share_btn">Share</button>
    </div>
</form>