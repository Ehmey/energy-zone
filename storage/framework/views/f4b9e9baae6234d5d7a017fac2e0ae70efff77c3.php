<script>


    function getSetupsData(value) {
        var xhttp = new XMLHttpRequest();
        baseUrl = "<?php echo e(url('admin/get-setups-data?setup=')); ?>" + value;


        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                data = this.responseText;
                console.log(JSON.parse(data));
                data = JSON.parse(data);
                document.getElementById('setup-table').innerHTML = data.html_menu;
                loadData(value);
            }
        }
        xhttp.open("GET", baseUrl, true);
        xhttp.send();
    }






    function loadData(value) {

        $(function () {
            var baseUrl;
            if (value == "companies") {
                baseUrl = '<?php echo route('admin.datatables.company.data'); ?>';
            }
            else if (value == "skills") {
                baseUrl = '<?php echo route('admin.datatables.skills.data'); ?>';
            }
            else if (value == "countries") {
                baseUrl = '<?php echo route('admin.datatables.countries.data'); ?>';
            }
            else if (value == "schools") {
                baseUrl = '<?php echo route('admin.datatables.schools.data'); ?>';
            }
            else if (value == "degrees") {
                baseUrl = '<?php echo route('admin.datatables.degrees.data'); ?>';
            }
            else if (value == "languages") {
                baseUrl = '<?php echo route('admin.datatables.languages.data'); ?>';
            }
            else if(value  == "users"){
                baseUrl     =   '<?php echo route('admin.datatables.users.data'); ?>';
            }
          //  alert(baseUrl);

            var columns;
            if (value == "schools" || value == "companies"  ) {

                columns = [
                    {data: 'id', name: 'id'},
                    {
                        data: 'image_path', "render": function (data, type, row) {
                            return '<img style="max-width:100px;" class="img-responsive" src="' + data + '" />';
                        }
                    },
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action'},
                    {data: 'created_at', name: 'created_at'}

                ];
            } else if( value == "users"){

                columns = [
                    {data: 'id', name: 'id'},
                    {
                        data: 'image_path', "render": function (data, type, row) {
                            return '<img style="max-width:100px;" class="img-responsive" src="' + data + '" />';
                        }
                    },
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action'},
                    {data: 'created_at', name: 'created_at'}

                ];
            }else {
                columns = [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action'},
                    {data: 'created_at', name: 'created_at'}

                ];
            }
console.log(columns);
            table = $('#setups-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: baseUrl,
                columns: columns
            });

        });
    }


    setTimeout(function () {
        $('#alert_notice').fadeOut('slow');
    }, 5000);


</script>

<script>

    function saveSetup(modalId) {
        //alert(document.getElementById('add_language').value);
        if (modalId == 'languageModal') {
            addLanguage(modalId);

        }
        else if (modalId == 'companyModal') {
            addCompany(modalId);
        }
        else if (modalId == 'skillModal') {
            addSkill(modalId);
        }
        else if (modalId == 'countryModal') {
            addCountry(modalId);
        }
        else if (modalId == 'schoolModal') {
            addSchool(modalId);
        }
        else if (modalId == 'degreeModal') {
            addDegree(modalId);
        }


    }

    function addDegree(modalId) {
        nameValue = document.getElementById('add_degree');

        if (nameValue != "") {
            $('#' + modalId).modal('hide');
            $.get(`<?php echo e(url('admin/add/degree/')); ?>` + `?name=` + nameValue.value, function (data, status) {
                nameValue.value = "";
                getSetupsData('degrees');
            });
        }
    }

    function addSchool(modalId) {
        nameValue = document.getElementById('add_school');
        imageValue = document.getElementById('add_school_image');
        if (nameValue != "") {
            $('#' + modalId).modal('hide');
            var formData = new FormData(this);
            formData.append('name', nameValue.value);
            formData.append('school_image', $('#add_school_image')[0].files[0]);
            baseUrl = "<?php echo e(url('admin/add/school/')); ?>";

            $.ajax({
                type: 'POST',
                url: baseUrl,
                data: formData,
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},

                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    nameValue.value = "";
                    imageValue.value = "";
                    getSetupsData('schools');
                },
                error: function (data) {
                    console.log("error");
                    console.log(data);
                }
            });
        }
    }

    function addLanguage(modalId) {
        nameValue = document.getElementById('add_language');
        if (nameValue != "") {
            $('#' + modalId).modal('hide');
            $.get(`<?php echo e(url('admin/add/language/')); ?>` + `?name=` + nameValue.value, function (data, status) {
                nameValue.value = "";
                getSetupsData('languages');
            });
        }
    }

    function addCountry(modalId) {
        nameValue = document.getElementById('add_country');
        if (nameValue != "") {
            $('#' + modalId).modal('hide');
            $.get(`<?php echo e(url('admin/add/country/')); ?>` + `?name=` + nameValue.value, function (data, status) {
                nameValue.value = "";
                getSetupsData('countries');
            });
        }
    }

    function addCompany(modalId) {
        nameValue = document.getElementById('add_company');
        imageValue = document.getElementById('company_image');
        if (nameValue != "") {
            $('#' + modalId).modal('hide');


            var formData = new FormData(this);
                formData.append('name',nameValue.value);
                formData.append('company_image', $('#company_image')[0].files[0]);
                baseUrl = "<?php echo e(url('admin/add/company')); ?>";

                $.ajax({
                    type: 'POST',
                    url: baseUrl,
                    data: formData,
                    headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},

                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        console.log(data);
                        nameValue.value = "";
                        imageValue.value = "";
                        getSetupsData('companies');
                    },
                    error: function (data) {
                        console.log("error");
                        console.log(data);
                    }
                });




        }
    }

    function addSkill(modalId) {
        nameValue = document.getElementById('add_skill');
        if (nameValue != "") {
            $('#' + modalId).modal('hide');
            $.get(`<?php echo e(url('admin/add/skill/')); ?>` + `?name=` + nameValue.value, function (data, status) {
                nameValue.value = "";
                getSetupsData('skills');
            });
        }
    }


    function changeStatus(id, value) {
        var baseUrl;
        var load;
        if (value == "companies") {
            baseUrl = `<?php echo e(url('admin/change-status/company/')); ?>` + "/" + id;
            load = "companies";
        }
        else if (value == "skills") {
            baseUrl = `<?php echo e(url('admin/change-status/skill/')); ?>` + "/" + id;
            load = "skills";
        }
        else if (value == "countries") {
            baseUrl = `<?php echo e(url('admin/change-status/country/')); ?>` + "/" + id;
            load = "countries";
        }
        else if (value == "degrees") {
            baseUrl = `<?php echo e(url('admin/change-status/degree/')); ?>` + "/" + id;
            load = "degrees";
        }

        else if (value == "schools") {
            baseUrl = `<?php echo e(url('admin/change-status/school/')); ?>` + "/" + id;
            load = "schools";
        }
        else if (value == "languages") {
            baseUrl = `<?php echo e(url('admin/change-status/language/')); ?>` + "/" + id;
            load = "languages";
        }
        else if (value == "users") {
            baseUrl = `<?php echo e(url('admin/change-status/user/')); ?>` + "/" + id;
            load = "users";
        }


        $.get(baseUrl, function (data, status) {
            //  alert("Data: " + data + "\nStatus: " + status);
        });
        getSetupsData(load);

    }</script>







