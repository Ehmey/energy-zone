<?php $__env->startSection('title', "Connections"); ?>
<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('user.includes.sidebar.connection-sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('user.includes.developerjs', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="col-md-8 col-sm-12">
        <div class="connection-right-area">

            <div class="inner-content01 bg-white">
                <?php if($data['pending_connections']->count() > 0): ?>
                <div class="our-heading"><h3 id="invitation_heading">invitations</h3></div>
                <?php $__currentLoopData = $data['pending_connections']->take(5); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $connection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="clearfix inner-content" id="invitation_<?php echo e($connection->user->id); ?>">

                    <div class="in-left">

                            <img src="<?php echo e(asset($connection->user->profile_image)); ?>" alt="profile">

                    </div>
                    <div class="in-right">
                        <div class="text">
                            <a href="<?php echo e(route('user.public-profile',[$connection->user->id])); ?>"><?php echo e($connection->user->name); ?></a>
                        </div>
                        <a class="btn-small" onclick="acceptInvitation(<?php echo e($connection->user->id); ?>,<?php echo e($connection->id); ?>)">Accept</a>
                    </div>
                </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                    <div class="our-heading"><h3>No Pending invitations</h3></div>
                <?php endif; ?>
            </div>

            <div class="our-heading"><h3> People you may know</h3></div>

            <div class="row" id="load_thumbnail">

               
                
                    

                        
                        
                        
                        
                        

                    
                

                   
    
                    
    
            </div>

        </div>
    </div>

    <?php echo $__env->make('user.includes.connections.connections-js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>