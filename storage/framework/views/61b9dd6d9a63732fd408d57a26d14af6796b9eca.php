<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo e(config('app.name', 'Energy Zone')); ?> - Login</title>
	<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">


	<?php echo $__env->make('admin.includes.css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
       
</head>

	<body>

	 <section class="admin_login_bg">
     	<div class="admin_login_main">
			<form method="post" action="<?php echo e(route('login')); ?>">
				<?php echo csrf_field(); ?>
        	<h1 class="admin_login_text">Login</h1>
            <ul class="admin_login_main_ul">
            	<li>
                	<i class="fas fa-user"></i>
                    <input type="text" placeholder="Email" name="email">

                </li>
				<?php if($errors->has('email')): ?>
					<span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
				<?php endif; ?>
                <li>
                	<i class="fas fa-key"></i>
                    <input type="password" placeholder="Password" name="password">

                </li>
				<?php if($errors->has('password')): ?>
					<span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
				<?php endif; ?>
                <li>
                	<input type="submit" value="Sign in">
                </li>
            </ul>
				<a class="forgot_pasword_admin" href="<?php echo e(route('password.request')); ?>">
					<?php echo e(__('Forgot Your Password?')); ?>

				</a>

			</form>
        </div>
     </section>
		



		<?php echo $__env->make('admin.includes.js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	   
		
	</body>

</html>