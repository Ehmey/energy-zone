<footer class="main-footer-wrapper text-center bg-white">
    <div class="container">

        <ul class="social-media-footer">
            <li>
                <a href="#"><i class="fab fa-twitter"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
                <a href="#"><i class="fab fa-facebook-f"></i></a>
                <a href="#"><i class="fab fa-youtube"></i></a>
            </li>
        </ul>

        <div class="text">© Copyright Energy Zone all rights reserved</div>

    </div>
</footer>