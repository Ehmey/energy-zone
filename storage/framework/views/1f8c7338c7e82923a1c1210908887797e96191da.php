<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?> - <?php echo $__env->yieldContent('title'); ?></title>
    <link rel="shortcut icon" type="image/png" href="<?php echo e(asset('energy-zone/favicon.png')); ?>"/>
    <!-- Scripts -->

    <link rel="shortcut icon" type="image/png" href="<?php echo e(asset('energy-zone/favicon.png')); ?>"/>
    <!-- Fonts -->


    <!-- Styles -->
    <?php echo $__env->make('user.includes.css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('user.includes.js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
<header class="complate-main-header-wrapper bg-white">
    <?php echo $__env->make('user.includes.header.user-main-header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('user.includes.menus.user-main-menu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</header>
<section class="main-site-area">
    <?php echo $__env->make('alerts.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div id="for_alert">  </div>
    <div class="profile_db_main">
        <div class="container">
            <div class="row">
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>
    </div>

</section>
<?php echo $__env->make('user.includes.footers.user-footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('user.includes.popups.not-found-popup', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</body>
</html>
