<div class="col-xs-12">
    <div class="highligts_sec">
        <h2 class="highlights_heading">Experience</h2>
        <ul class="experience_ul toggle_exp_ul">
            <?php if(Auth::user()->experince()->count() !=0): ?>
                <?php $__currentLoopData = $data['user']->experince; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $experience): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                            <span class="experience_img"><img
                                        src="<?php echo e(asset($experience->experinceCompany->image_path)); ?>"
                                        alt="error img"></span>
                        <div class="experience_right_content">
                            <span class="experience_title"><?php echo e($experience->experinceCompany->name); ?></span>
                            <span class="experience_descreption"><?php echo e($experience->experinceJobTitle->name); ?></span>
                            <span class="experience_descreption"> <?php echo e($experience->experince); ?> </span>
                            
                        </div>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
                No Data till yet.
            <?php endif; ?>
        </ul>
        <?php if($data['user']->experince()->count() > 3): ?>
            <div class="see_more_main text-center" style="background:#fff;">
                <a href="javascript:void(0)" id="see_more3">See more</a>
            </div>
        <?php endif; ?>
    </div>
</div>