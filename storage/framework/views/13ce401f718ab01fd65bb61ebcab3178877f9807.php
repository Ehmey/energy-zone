<div class="adding-wrapper <?php if(Auth::user()->experince()->count() == 0): ?> bg-white <?php endif; ?>" style="padding:0px;">
    <div class="our-heading clearfix" style="border:0px;">
        <h3>Experince</h3>
        <a class="btn-small" data-toggle="modal" data-target="#addExperince">Add Experince</a>
    </div>


<?php if(Auth::user()->experince()->count() != 0): ?>

<?php $__currentLoopData = Auth::user()->experince; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $experince): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="edit-profile03">
        <div style="margin:0px;" class="hr-accontant bg-white">
            <div class="clearfix">
                <div class="in-left">
                    <img src="<?php echo e(asset($experince->experinceCompany->image_path)); ?>" alt="img">
                </div>
                <div class="in-right">
                    <h3><?php echo e($experince->experinceJobTitle->name); ?></h3>
                    <h4><?php echo e($experince->experinceCompany->name); ?></h4>
                    <div class="text" style="padding:0px;"><?php echo e($experince->experince); ?>


                    </div>
                </div>
            </div>
            <div class="adit-del">
                <a data-toggle="modal" data-target="#editExperince_<?php echo e($experince->id); ?>"><i class="fas fa-pencil-alt"></i></a>
                <a href="<?php echo e(route('user.delete.experince',[$experince->id])); ?>"><i class="fas fa-trash-alt"></i></a>
            </div>
        </div>
    </div>

            <?php echo $__env->make('user.includes.popups.edit-experience-popup', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php else: ?>
    <div class="text">
        You haven't added any Experince yet.
    </div>
<?php endif; ?>
</div>


<?php echo $__env->make('user.includes.popups.add-experince-popup', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>