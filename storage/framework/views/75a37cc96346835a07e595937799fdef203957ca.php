<div class="modal fade" id="editSkill" role="dialog">
    <div class="general_popup_in">
        <span class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></span>
        <h1 class="general_popup_heading">Add Skill</h1>
        <div class="popup_white_sec">
            <form method="post" action="<?php echo e(route('user.add.skill')); ?>">
                <?php echo csrf_field(); ?>
                <ul class="popup_ul">

                    <li>
                        <label>Add Skill</label>
                        <select name="skill_id">
                            <?php $__currentLoopData = $data['all_skills']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $skill): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($skill->id); ?>"><?php echo e($skill->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </select>
                    </li>


                </ul>
                <input type="submit" value="Save" class="popup_save_btn">
            </form>
        </div>

    </div>
</div>