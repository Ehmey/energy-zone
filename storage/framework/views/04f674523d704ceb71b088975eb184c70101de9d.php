<link href="https://use.fontawesome.com/releases/v5.0.1/css/all.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('energy-zone/admin/assets/css/bootstrap.min.css')); ?>">
<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('energy-zone/admin/assets/css/front.css')); ?>">
<script src="<?php echo e(asset('energy-zone/admin/assets/js/jquery.js')); ?>"></script>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>