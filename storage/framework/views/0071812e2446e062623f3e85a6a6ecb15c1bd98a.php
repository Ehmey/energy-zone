<div class="modal fade" id="editLanguage" role="dialog">
    <div class="general_popup_in">
        <span class="close_popup" data-dismiss="modal"><i class="fas fa-times"></i></span>
        <h1 class="general_popup_heading">Add Skill</h1>
        <div class="popup_white_sec">
            <form method="post" action="<?php echo e(route('user.add.language')); ?>">
                <?php echo csrf_field(); ?>
                <ul class="popup_ul">

                    <li>
                        <label>Add Language</label>
                        <select name="language_id">
                            <?php $__currentLoopData = $data['all_languages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $language): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($language->id); ?>"><?php echo e($language->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </select>
                    </li>

                    <li>
                        <label>Proficency</label>
                        <select name="proficency">
                            <option value="Fluent">Fluent</option>
                            <option value="Native">Native</option>
                        </select>
                    </li>

                </ul>
                <input type="submit" value="Save" class="popup_save_btn">
            </form>
        </div>

    </div>
</div>