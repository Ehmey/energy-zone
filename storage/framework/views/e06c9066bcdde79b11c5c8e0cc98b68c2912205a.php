<div class="col-xs-12">
    <div class="highligts_sec">
        <h2 class="highlights_heading">Education</h2>

        <ul class="experience_ul onlyfor_toggleli_education">

            <?php if(Auth::user()->education()->count() !=0): ?>
                <?php $__currentLoopData = $data['user']->education; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $education): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                                <span class="experience_img"><img
                                            src="<?php echo e(asset('energy-zone/user/assets/images/experience-icon.png')); ?>"
                                            alt="error img"></span>
                        <div class="experience_right_content">
                            <span class="experience_title"><?php echo e($education->educationSchool->name); ?></span>

                            <span class="experience_descreption"><?php echo e($education->educationDegree->name); ?> </span>
                            <span class="experience_descreption"><?php echo e($education->activities); ?> </span>
                            <span class="experience_descreption"><?php echo e($education->_from); ?> - <?php echo e($education->_to); ?> </span>
                        </div>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
                No Data till yet.
            <?php endif; ?>
        </ul>

        <?php if($data['user']->skills()->count() > 3): ?>
            <div class="see_more_main text-center" style="background:#fff;">
                <a href="javascript:void(0)" id="see_more2">See more</a>
            </div>
        <?php endif; ?>

    </div>
</div>