<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;

class Post extends Model
{
    protected $fillable = ['user_id','description'];




    /////// SCOPES



    public function user()
    {
        return $this->belongsTo('App\User')->withoutGlobalScope(ActiveScope::class);
    }


    public function hiddenPosts()
    {
        return $this->hasMany('App\User\HidePost');
    }


    public function images()
    {
        return $this->hasMany('App\User\PostImages');
    }

    public function videos()
    {
        return $this->hasMany('App\User\PostVideo');
    }

    public function comments()
    {
        return $this->hasMany('App\User\Comment');
    }

    public function likes()
    {
        return $this->hasMany('App\User\Like');
    }

    protected $dates = [
        'created_at',
        'updated_at'
    ];

}
