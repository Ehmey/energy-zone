<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;
use App\Traits\FormatDates;
use App\Scopes\ActiveScope;

class Skill extends Model
{
    use FormatDates;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }

    protected $fillable = ['name','active'];
    protected $dates = [
        'created_at',
        'updated_at'
    ];
}
