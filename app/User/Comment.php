<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;
use App\Traits\FormatDates;
use App\Scopes\ActiveScope;

class Comment extends Model
{
    use FormatDates;
    protected $fillable = ['post_id','comment','user_id'];



    public function user()
    {
        return $this->belongsTo('App\User')->withoutGlobalScope(ActiveScope::class);
    }


    public function replies()
    {
        return $this->hasMany('App\User\Reply');
    }

    public function post()
    {
        return $this->belongsTo('App\User\Post');
    }

    protected $dates = [
        'created_at',
        'updated_at'
    ];

}
