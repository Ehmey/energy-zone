<?php

namespace App\User;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;

class UserExperince extends Model
{
    protected $fillable = ['user_id','company_id','job_title_id','experince','_from','_to'];

    public function experinceCompany()
    {
        return $this->belongsTo('App\Company','company_id','id')->withoutGlobalScope(ActiveScope::class);;
    }


    public function experinceJobTitle()
    {
        return $this->belongsTo('App\JobTitle','job_title_id','id')->withoutGlobalScope(ActiveScope::class);;
    }


}
