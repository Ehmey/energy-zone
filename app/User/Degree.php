<?php

namespace App\User;

use App\Scopes\ActiveScope;
use App\Traits\FormatDates;
use Illuminate\Database\Eloquent\Model;

class Degree extends Model
{
    use FormatDates;
    protected $fillable = ['name','active'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }

    protected $dates = [
        'created_at',
        'updated_at'
    ];
}
