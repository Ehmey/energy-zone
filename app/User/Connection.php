<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;

class Connection extends Model
{
    protected $fillable = ['accept_id','send_id','status'];

    public function user(){
return $this->belongsTo('App\User', 'send_id', 'id');
}



}
