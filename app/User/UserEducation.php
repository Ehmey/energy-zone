<?php

namespace App\User;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;

class UserEducation extends Model
{
    protected $fillable = ['user_id','degree_id','school_id','activities','_from','_to'];


    public function getFromAttribute($_from)
    {
        return \Carbon\Carbon::parse($_from)->format('M-d-Y');
    }


    public function getToAttribute($_to)
    {
        return \Carbon\Carbon::parse($_to)->format('M-d-Y');
    }


    protected $dates = [
        '_from',
        '_to'
    ];





    public function educationSchool()
    {
        return $this->belongsTo('App\User\School','school_id','id')->withoutGlobalScope(ActiveScope::class);;
    }


    public function educationDegree()
    {
        return $this->belongsTo('App\User\Degree','degree_id','id')->withoutGlobalScope(ActiveScope::class);;
    }


}
