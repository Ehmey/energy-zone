<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;

class HidePost extends Model
{
    protected $fillable = ['user_id','post_id'];
}
