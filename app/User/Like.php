<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = ['comment_id','reply_id','user_id','post_id'];
}
