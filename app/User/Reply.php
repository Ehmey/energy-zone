<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;
use App\Traits\FormatDates;


class Reply extends Model
{use FormatDates;
    protected $fillable = ['user_id','comment_id','reply'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
