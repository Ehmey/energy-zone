<?php

namespace App\User;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;
use App\Traits\FormatDates;


class Language extends Model
{
    use FormatDates;
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }


    protected $fillable = ['name','active'];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

}
