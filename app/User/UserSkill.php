<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;

class UserSkill extends Model
{
    protected $fillable = ['user_id','skill_id'];
}
