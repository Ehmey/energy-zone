<?php

namespace App\User;

use Illuminate\Database\Eloquent\Model;

class PostVideo extends Model
{
    protected $fillable = ['post_id','video_path'];
}
