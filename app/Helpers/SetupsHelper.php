<?php
/**
 * Created by PhpStorm.
 * User: bilal
 * Date: 4/27/18
 * Time: 12:22 PM
 */

namespace App\Helpers;


use Illuminate\Http\Request;
use Image;

class SetupsHelper
{

    ///////     Type can be link or anything else. In $file we will receive path of file or object of file
    public static function getSetups($setpName)
    {
        if ($setpName == "companies") {
            return self::getCompaniesHtml();
        } elseif ($setpName == "skills") {
            return self::getSkillsHtml();
        } elseif ($setpName == "countries") {
            return self::getCountriesHtml();
        } elseif ($setpName == "schools") {
            return self::getSchoolsHtml();
        } elseif ($setpName == "degrees") {
            return self::getDegreesHtml();
        } elseif ($setpName == "languages") {
            return self::getLanguagesHtml();
        }
        elseif ($setpName == "users") {
            return self::getUsersHtml();
        }
    }



    public static function getUsersHtml()
    {

        return "<hr>



                        <table class=\"table table-bordered\" id=\"setups-table\">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Profile Image</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                            </table>
                        
                        ";
    }


    public static function getCompaniesHtml()
    {

        return "<hr>
<button type=\"button\" class=\"btn btn-primary btn-sm\"   style=\"margin-bottom:20px;margin-top:0px;\"  data-toggle=\"modal\" data-target=\"#companyModal\">Add Company <i class='glyphicon glyphicon-plus'></i></button>


                        <table class=\"table table-bordered\" id=\"setups-table\">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                            </table>
                        
                        ";
    }


    public static function getSkillsHtml()
    {
        return "<hr>
<button type=\"button\" class=\"btn btn-primary btn-sm\"  style=\"margin-bottom:20px;margin-top:0px;\"  data-toggle=\"modal\" data-target=\"#skillModal\">Add Skill <i class='glyphicon glyphicon-plus'></i></button>


                        <table class=\"table table-bordered\" id=\"setups-table\">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                            </table>
                        
                        ";
    }


    public static function getLanguagesHtml()
    {
        return "<hr>
<button type=\"button\" class=\"btn btn-primary btn-sm\"  style=\"margin-bottom:20px;margin-top:0px;\"  data-toggle=\"modal\" data-target=\"#languageModal\">Add Language <i class='glyphicon glyphicon-plus'></i></button>


                        <table class=\"table table-bordered\" id=\"setups-table\">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                            </table>
                        
                        ";
    }


    public static function getCountriesHtml()
    {
        //                        <ul class="dropsown_sub_links">
//                            <li><a href="#">Active Comapanies</a></li>
//                            <li><a href="#">DeActive Companies</a></li>
//                        </ul>
        return "<hr>
<button type=\"button\" class=\"btn btn-primary btn-sm\"  style=\"margin-bottom:20px;margin-top:0px;\"  data-toggle=\"modal\" data-target=\"#countryModal\">Add Country <i class='glyphicon glyphicon-plus'></i></button>


                        <table class=\"table table-bordered\" id=\"setups-table\">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                            </table>
                        
                        ";
    }


    public static function getSchoolsHtml()
    {

        return "<hr>
<button type=\"button\" class=\"btn btn-primary btn-sm\" style=\"margin-bottom:20px;margin-top:0px;\" data-toggle=\"modal\" data-target=\"#schoolModal\">Add School <i class='glyphicon glyphicon-plus'></i></button>

                        <table class=\"table table-bordered\" id=\"setups-table\">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                            </table>
                        
                        ";
    }


    public static function getDegreesHtml()
    {
        //                        <ul class="dropsown_sub_links">
//                            <li><a href="#">Active Comapanies</a></li>
//                            <li><a href="#">DeActive Companies</a></li>
//                        </ul>
        return "<hr>
<button type=\"button\" class=\"btn btn-primary btn-sm\"  style=\"margin-bottom:20px;margin-top:0px;\"  data-toggle=\"modal\" data-target=\"#degreeModal\">Add Degrees <i class='glyphicon glyphicon-plus'></i></button>

 


                        <table class=\"table table-bordered\" id=\"setups-table\">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                            </table>
                        
                        ";
    }


}