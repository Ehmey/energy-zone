<?php

namespace App\Groups;

use Illuminate\Database\Eloquent\Model;

class GroupConnections extends Model
{
    protected $fillable = ['user_id','group_id'];
}
