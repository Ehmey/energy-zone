<?php

namespace App\Groups;

use Illuminate\Database\Eloquent\Model;

class GroupTypes extends Model
{
    protected $fillable = ['name','active'];
}
