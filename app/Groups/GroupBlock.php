<?php

namespace App\Groups;

use Illuminate\Database\Eloquent\Model;

class GroupBlock extends Model
{
    protected $fillable = ['user_id','group_id'];
}
