<?php

namespace App\Http\Controllers\Admin;

use App\Services\CountryService;
use Illuminate\Http\Request;
use App\Helpers\SetupsHelper;
use Yajra\Datatables\Datatables;
use App\Services\LanguageService;
use App\Services\CompanyService;
use App\Services\SchoolService;
use App\Services\SkillService;
use App\Services\DegreeService;
use App\User;

use App\Http\Controllers\Controller;

class CrudsController extends Controller
{
    public function profileSettings()
    {
        return view('admin.content.setups');
    }


    public function generalSettings()
    {
        return view('admin.content.general-settings');
    }

    public function getSetup(Request $request)
    {
        return ['html_menu' => SetupsHelper::getSetups($request->setup)];
    }


    public function anyData(Request $request, CompanyService $companyService)
    {
        return $companyService->getCompanyData();
    }



    ///////////     LANGUAGE    ////////////

    public function languagesData(LanguageService $languageService)
    {
        return $languageService->getLanguageData();
    }

    public function changeLanguageStatus($id, LanguageService $languageService)
    {
        $languageService->changeLanguageStatus($id);
        return "success";
    }

    public function addLanguage(Request $request, LanguageService $languageService)
    {
        $languageService->addLanguage($request);
        return "success";
    }

    ///////////     COMPANY     ////////////



    public function changeCompanyStatus($id, CompanyService $companyService)
    {
        $companyService->changeCompanyStatus($id);
        return redirect()->back();
    }



    public function addCompany(Request $request, CompanyService $companyService)
    {
        $companyService->addCompany($request);
        return redirect()->back();
    }



    ///////////     SKILL       /////////////

    public function changeSkillStatus($id, SkillService $skillService)
    {
        $skillService->changeSkillsStatus($id);
        return redirect()->back();
    }


    public function skillsData(SkillService $skillService)
    {
        return $skillService->getSkillsData();
    }

    public function addSkill(Request $request, SkillService $skillService)
    {
        $skillService->addSkill($request);
        return redirect()->back();
    }






    ///////////    COOUNTRY     ////////////

    public function changeCountryStatus($id, CountryService $countryService)
    {
        $countryService->changeCountryStatus($id);
        return redirect()->back();
    }


    public function addCountry(Request $request, CountryService $countryService)
    {
        $countryService->addCountry($request);
        return redirect()->back();
    }


    public function countriesData(CountryService $countryService)
    {
        return $countryService->getCountryData();
    }



    ////////////    SCHOOLS    ////////////


    public function schoolsData(SchoolService $schoolService)
    {
        return $schoolService->getSchoolsData();
    }

    public function changeSchoolStatus($id, SchoolService $schoolService)
    {
        return $schoolService->changeSchoolStatus($id);
    }

    public function addSchool(Request $request, SchoolService $schoolService)
    {
         $schoolService->addSchool($request);
        return redirect()->back();
    }


    /////////////       DEGRESS     /////////////

    public function changeDegreeStatus($id, DegreeService $degreeService)
    {
        $degreeService->changeDegreeStatus($id);
        return redirect()->back();
    }

    public function degreesData(DegreeService $degreeService)
    {
        return $degreeService->getDegreeData();
    }

    public function addDegree(Request $request,DegreeService $degreeService)
    {
         $degreeService->addDegree($request);
        return redirect()->back();
    }





}
