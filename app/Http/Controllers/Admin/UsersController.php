<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    //

    public function index(){
        return view('admin.content.users-managament');
        //return view('admin.content.users-managment');
    }


    public function usersData()
    {
        $users = User::withoutGlobalScopes()->get();
        $result = [];

        foreach ($users as $user) {
            $status = 'active';

            $active = "<a class=\"btn btn-danger btn-xs\" onclick=\"changeStatus(".$user->id.",'users')\" >Deactive</a>";
            if ($user->active == 0) {
                $status = 'deactive';
                $active = "<a class=\"btn btn-success btn-xs\" onclick=\"changeStatus(".$user->id.",'users')\">Active </a>";
            }
            $imageUrl = asset($user->profile_image);
            $result['users'][] = [
                'action' => $active,
                'email' => $user->email,
                'name' => $user->name,
                'image_path' =>$imageUrl,
                'id' => $user->id,
                'created_at' => $user->created_at,
                'status' => $status
            ];
        }


        return Datatables::of($result['users'])->make(true);
    }




    public function changeUserStatus($id){
        $user = User::withoutGlobalScopes()->find($id);
        if($user->active == 0){
            $user->update(['active' => 1]);
            session()->flash('user-status-active',$user->name." Activated.");

        }else{
            $user->update(['active' => 0]);
            session()->flash('user-status-deactive',$user->name." DeActivated.");

        }
    }


}
