<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Services\ReplyService;
use App\Http\Controllers\Controller;

class ReplyController extends Controller
{
    private $replyService;

    public function __construct()
    {
        $this->replyService = new ReplyService();
    }

    public function loadReply($id,Request $request)
    {
        return response()->json([
            $this->replyService->loadReply($id, $request)
        ]);
    }



    public function postReply($commentId,Request $request)
    {
        return response()->json([
            $this->replyService->postReply($commentId, $request,Auth::user()->id)
        ]);
    }


    public function deleteReply($replyId)
    {
        return response()->json([
            $this->replyService->deleteReply($replyId)
        ]);
    }


}
