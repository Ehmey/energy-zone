<?php

namespace App\Http\Controllers\User;

use App\User\UserEducation;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;

class EducationController extends Controller
{
    public function deleteEducation($id)
    {

        UserEducation::destroy($id);
        session()->flash('education-delete-success', 'Education Deleted Successfully.');
        return redirect()->back();
    }






    public function editEducation(Request $request)
    {

        try
        {
            UserEducation::find($request->user_education_id)->update(['user_id'=>Auth::user()->id,'school_id'=>$request->school_id,'activities'=>$request->activities,'degree_id'=>$request->degree_id]);
            session()->flash('education-added','Education Edited Successfully.');
            return redirect()->back();
        }
        catch(\Exception $exception){
            session()->flash('education-added-failed','Probably Duplication in Education.');
            return redirect()->back();
        }

    }



    public function addEducation(Request $request)
    {

        try
        {
            UserEducation::create(['_from' => '2018-09-25 10:06:01','_to' => '2018-09-25 10:06:01','user_id'=>Auth::user()->id,'school_id'=>$request->school_id,'activities'=>$request->activities,'degree_id'=>$request->degree_id]);
            session()->flash('education-added','Education Added Successfully.');
            return redirect()->back();
        }
        catch(\Exception $exception)
        {
            session()->flash('education-added-failed','Probably Duplication in Education.');
            return redirect()->back();
        }

    }
}
