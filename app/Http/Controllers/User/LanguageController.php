<?php

namespace App\Http\Controllers\User;

use App\User\Language;
use Auth;
use Illuminate\Http\Request;
use App\User\Skill;
use App\User\UserLanguage;
use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    public function deleteLanguage($id)
    {
        UserLanguage::where('user_id',Auth::user()->id)->where('language_id',$id)->delete();
        session()->flash('language-delete-success', 'Language Edited Successfully.');
        return redirect()->back();
    }


    public function addLanuage(Request $request)
    {
        try{
            UserLanguage::create(['user_id'=>Auth::user()->id,'language_id'=>$request->language_id,'proficency'=>$request->proficency]);
            session()->flash('language-added','Language Added Successfully.');
            return redirect()->back();
        }
        catch(\Exception $exception){
            session()->flash('language-added-failed','Probably Duplication in Language.');
            return redirect()->back();
        }

    }
}
