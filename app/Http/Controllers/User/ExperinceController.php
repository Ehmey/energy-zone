<?php

namespace App\Http\Controllers\User;

use Auth;
use App\User\UserExperince;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExperinceController extends Controller
{
    public function addExperince(Request $request)
    {
        UserExperince::create(['user_id' => Auth::user()->id,'experince' => $request->experince, 'company_id' => $request->company_id, 'job_title_id' => $request->job_title_id]);
        session()->flash('experince-added','Experince Added Successfully.');
        return redirect()->back();
    }


    public function deleteExperince($id){
        UserExperince::destroy($id);
        session()->flash('experince-deleted','Experince Deelted Successfully.');
        return redirect()->back();
    }



    public function editExperince(Request $request)
    {

        try
        {
            UserExperince::find($request->user_experience_id)->update(['user_id'=>Auth::user()->id,'company_id'=>$request->company_id,'experince'=>$request->experience,'job_title_id'=>$request->job_title_id]);
            session()->flash('experince-added','Experience Edited Successfully.');
            return redirect()->back();
        }
        catch(\Exception $exception){
            session()->flash('experince-added-failed','Probably Duplication in Education.');
            return redirect()->back();
        }

    }



}
