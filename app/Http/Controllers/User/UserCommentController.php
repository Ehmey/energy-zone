<?php

namespace App\Http\Controllers\User;

use App\User\Comment;
use Illuminate\Http\Request;
use App\User\Post;
use Auth;
use App\Http\Controllers\Controller;

class UserCommentController extends Controller
{
    public function delteComment($id)
    {
        $commentsPosts = Comment::find($id)->post()->first();
        $commentsPostId = $commentsPosts->id;
        Comment::where('user_id', Auth::user()->id)->where('id', $id)->delete();
        return response()->json([
            "success" => "success",
            "comments_count" =>$commentsPosts->comments->count(),
            "post_id" =>$commentsPostId
        ]);
    }


    public function postComment($id,Request $request){
        $comment = Comment::create(['post_id'=>$id,'comment'=>$request->comment,'user_id' => $request->user_id]);
        $data = null;
        foreach(Post::find($id)->comments()->orderBy('id','desc')->get() as $comment){
            $data['comments'][] = ['post_id'=>Post::find($id)->id,'user_name'=>$comment->user->name,'comment' => $comment->comment, 'created_at' => $comment->created_at, 'comment_id' => $comment->id, 'user_id' => $comment->user_id, 'user_image' => asset($comment->user->profile_image)];
        }

        return response()->json([
            $data,
            'comments_count' => Post::find($id)->comments()->count()
        ]);

    }

    public function loadComment($id, Request $request)
    {
        $data = null;
        foreach (Post::find($id)->comments()->orderBy('id', 'desc')->get() as $comment) {
            $data['comments'][] = ['replies_count'=>$comment->replies()->count(),'post_id' => Post::find($id)->id, 'user_name' => $comment->user->name, 'comment' => $comment->comment, 'created_at' => $comment->created_at, 'comment_id' => $comment->id, 'user_id' => $comment->user_id, 'user_image' => asset($comment->user->profile_image)];
        }
        return response()->json([
            $data
        ]);

    }

}
