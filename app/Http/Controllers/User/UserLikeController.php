<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\User\Like;
use Auth;
use App\User\Post;
use App\Http\Controllers\Controller;

class UserLikeController extends Controller
{

    public function postLike($id, Request $request)
    {
        if(Like::where('post_id',$id)->where('user_id',$request->user_id)->count() != 0){
            Like::where('post_id',$id)->where('user_id',$request->user_id)->delete();
            return response()->json([
                'status' =>'dis_like',
                'likes_count' => Post::find($id)->likes()->count()
            ]);
        }else{
            Like::create(['post_id'=>$id,'user_id'=>$request->user_id]);
            return response()->json([
                'status' =>'like',
                'likes_count' => Post::find($id)->likes()->count()
            ]);

        }
    }


    public function commentLike($id, Request $request){

        return Like::create(['user_id' => Auth::user()->id, 'comment_id' => $id]);


    }


}
