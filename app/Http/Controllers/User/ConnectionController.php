<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Services\ConnectionsService;
use App\Http\Controllers\Controller;
use Auth;
use App\User\Connection;

class ConnectionController extends Controller
{
    public function sendRequest($id, Request $request, ConnectionsService $connectionsService)
    {
        return $connectionsService->addConnection($id, $request);
    }

    public function connections()
    {
        $data['people_you_may_know'] = Auth::user()->peopleYouMayKnow();
        $data['pending_connections'] = Auth::user()->pendingConnections();
        $data['acceptedConnections'] = Auth::user()->acceptedConnections();
        return view('user.content.connections', compact('data'));
    }

    public function peopleYouMayKnow($start = 0)
    {

        $peoples = Auth::user()->peopleYouMayKnow($start);

        $data = [];
        if (!empty($peoples['user'])) {

            foreach ($peoples['user'] as $people) {

                if (isset($people[0]->jobTitle->name)) {
                    $data['users'][] = [


                        'user_profile_url' => route('user.public-profile', [$people[0]->id]),


                        'user_id' => $people[0]->id,


                        'profile_image' => asset($people[0]->profile_image),

                        'name' => $people[0]->name,


                        'job_at' => $people[0]->jobTitle->name . " at " . $people[0]->company->name];

                }
            }

        }

        if (count($peoples['user']) < 12) {
            $data['see_more'] = "no_see_more";
        }


        return $data;
    }


    public function acceptInvitation($id)
    {

        Connection::find($id)->update(['status' => 'Accepted']);
        return ['count' => Auth::user()->pendingConnections()->count(), 'accepted_connections' => Auth::user()->acceptedConnections()->get()->count()];
    }


}
