<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use App\Services\JobsService;

class JobsController extends Controller
{
    public function editJob(Request $request, JobsService $jobsService){
        $jobsService->editUserJob($request);
    }
}
