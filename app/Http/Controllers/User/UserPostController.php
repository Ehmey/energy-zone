<?php

namespace App\Http\Controllers\User;

use App\User\HidePost;
use App\User\PostVideo;
use App\User\Comment;
use App\User\PostImages;
use Auth;
use Illuminate\Http\Request;
use App\User\Post;
use App\Http\Controllers\Controller;

class UserPostController extends Controller
{


    public function getPosts(Request $request)
    {
        $data['posts'] = Post::orderBy('id', 'desc')->whereDoesntHave('hiddenPosts', function ($q) {
            $q->where('user_id', Auth::user()->id);
        })->orderBy('id', 'desc')->skip($request->start)->take(5)->get();

        $result = null;
        if($data['posts']->count() > 0) {
            foreach ($data['posts'] as $post) {
                $postVideopath = null;
                $postImagepath = null;
                $userStatus = null;
                $deletePostUrl = null;
                $likeBy = "not_me";

                if (!empty($post->videos->first()->video_path)) {
                    $postVideopath = asset($post->videos->first()->video_path);
                }

                if (!empty($post->images->first()->image_path)) {
                    $postImagepath = asset($post->images->first()->image_path);
                }
                if ($post->user_id == Auth::user()->id) {

                    $userStatus = "me";
                    $deletePostUrl = route('user.delete.post', ['id' => $post->id]);
                }
                if ($post->likes->where('post_id', $post->id)->where('user_id', Auth::user()->id)->count() == 1) {
                    $likeBy = "me";
                }

                $result['posts'][] = [
                    'total_posts' => Post::orderBy('id', 'desc')->whereDoesntHave('hiddenPosts', function ($q) {
                        $q->where('user_id', Auth::user()->id);
                    })->orderBy('id', 'desc')->count(),
                    'post_desc' => $post->description,
                    'posts_like' => $post->likes->count(),
                    'like_by' => $likeBy,
                    'post_id' => $post->id,
                    'posted_by' => $userStatus,
                    'user_public_profile' => route('user.public-profile', ['id' => $post->user_id]),
                    'posted_user_name' => $post->user->name,
                    'user_img' => $post->user->profile_image,
                    'post_image_path' => $postImagepath,
                    'post_video_path' => $postVideopath,
                    'created_at' => $post->created_at->diffForHumans(),
                    'post_comments' => $post->comments->count(),
                    'post_delete_url' => route('user.delete.post', ['id' => $post->id]),
                    'hide_post_url' => route('user.hide.post', ['id' => $post->id]),
                    'delete_post_url' => $deletePostUrl
                ];
            }
        }else{
            return $result['posts'][]=[];
        }


        return $result;
    }

    public function doPost(Request $request)
    {
        $postId = Post::create($request->except(['_token']));


        if ($request->has('myFile')) {
            $file = $request->file('myFile');
            $fileName = time() . '-post_image-' . '.' . $file->getClientOriginalExtension();
            $folderPath = public_path('energy-zone/post/images/');


            $file->move($folderPath, $fileName);
            PostImages::create(['image_path' => "energy-zone/post/images/" . $fileName, 'post_id' => $postId->id]);
        }

        if ($request->has('videoFile')) {
            $file = $request->file('videoFile');
            $fileName = time() . '-post_video' . '.' . $file->getClientOriginalExtension();
            $folderPath = public_path('energy-zone/post/videos/');
            $file->move($folderPath, $fileName);
            PostVideo::create(['video_path' => "energy-zone/post/videos/" . $fileName, 'post_id' => $postId->id]);
        }


        session()->flash('post-success', 'Posted Successfully.');
        return redirect()->back();
    }

    public function hidePosts($id)
    {
        HidePost::create(['user_id' => Auth::user()->id, 'post_id' => $id]);
        session()->flash('post-hide-success', 'Post Hidden Successfully.');
        return redirect()->back();
    }


    public function deletePost($id)
    {
        Post::destroy($id);
        session()->flash('post-deleted-success', 'Post Deleted Successfully.');
        return redirect()->back();
    }


}
