<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User\Skill;
use App\User\UserSkill;
use Auth;

class SkillController extends Controller
{
    public function deleteSkill($id)
    {
        UserSkill::where('user_id',Auth::user()->id)->where('skill_id',$id)->delete();
        session()->flash('skill-edit-success', 'Skill Edited Successfully.');
        return redirect()->back();
    }


    public function addSkill(Request $request)
    {
        try {
            UserSkill::create(['user_id' => Auth::user()->id, 'skill_id' => $request->skill_id]);
            session()->flash('skill-added', 'Skill Added Successfully.');
            return redirect()->back();
        }
        catch(\Exception $exception){
            session()->flash('skill-added-failed', 'Probably Duplication in Skills.');
            return redirect()->back();
        }
    }

}
