<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\Services\CompanyService;

class CompanyController extends Controller
{
    //
    public function editCompany(Request $request, CompanyService $companyService)
    {
        $companyService->editUserCompany($request);
    }

}
