<?php

namespace App\Http\Controllers\User;

use App\Company;
use App\JobTitle;
use App\Services\ConnectionsService;
use App\User;
use App\Country;
use App\User\Degree;
use App\User\Language;
use App\User\School;
use App\User\UserEducation;
use App\UserInfo;
use Illuminate\Http\Request;
use Image;
use Mail;
use App\User\Post;
use App\User\Connection;
use App\User\Skill;
use Auth;
use App\Mail\WelcomeEmail;
use App\Helpers\ImageHelpers;
use App\Http\Controllers\Controller;

class UserProfileController extends Controller
{

    public function publicProfile($id, ConnectionsService $connectionsService)
    {



        $data['user'] = User::find($id);
        $data['mutual_friends'] = 0;

        $data['mutual_friends'] = $connectionsService->mutualFriends($id,  $data['user']);

        ///dd($data['mutual_friends']);
        $data['request_status'] = Connection::where(
            [
                ['accept_id', Auth::user()->id],
                ['send_id', $data['user']->id]
            ])
            ->orWhere(
                [
                    ['accept_id', $data['user']->id],
                    ['send_id', Auth::user()->id]
                ]
            )
            ->first();


        return view('user.content.public-profile', compact('data'));
    }


    public function editDesc(Request $request)
    {
        UserInfo::where('user_id', Auth::user()->id)->first()->update(['description' => $request->name]);

    }


    public function userEditName(Request $request)
    {
        Auth::user()->update(['name' => $request->name]);
        return "done";
    }


    public function profile()
    {
        try {
            if (Auth::user()->welcome_flag != 1) {
                $data = null;
                Mail::to(Auth::user())->send(new WelcomeEmail(['name' => Auth::user()->name]));
                Auth::user()->update(['welcome_flag' => 1]);
            }
            $data['acceptedConnections'] = Auth::user()->acceptedConnections();

            $data['posts'] = Post::orderBy('id', 'desc')->whereDoesntHave('hiddenPosts', function ($q) {
                $q->where('user_id', Auth::user()->id);
            })->orderBy('id', 'desc')->get();
            return view('user.content.dashboard-content', compact('data'));
        } catch (\Exception $exception) {

        }
        return view('user.content.dashboard-content', compact('data'));
    }


    public function editProfilePost(Request $request)
    {
        Auth::user()->userInfo->update($request->except(['_token']));
        session()->flash('edited-success', 'Edited Successfully.');
        return redirect()->back();
    }

    public function editProfile(Request $request)
    {
        $data['all_languages'] = Language::all();
        $data['all_skills'] = Skill::all();
        $data['companies'] = Company::all();
        $data['countries'] = Country::all();
        $data['job_title'] = JobTitle::all();
        $data['schools'] = School::all();
        $data['degrees'] = Degree::all();
        $data['user_education'] = UserEducation::where('user_id', Auth::user()->id);

        return view('user.content.edit-profile-content', compact('data'));
    }


    public function editProfileImage(Request $request)
    {
        $fileName = time() . "-" . Auth::user()->name . ".png";
        $result = ImageHelpers::updateProfileImage('energy-zone/user/images/user_images/', $request->file('image_profile'), Auth::user()->name, 'not-link', $fileName);
        Auth::user()->update(['profile_image' => "energy-zone/user/images/user_images/" . $fileName]);

        return response()->json([
            asset("energy-zone/user/images/user_images/" . $fileName)
        ]);
    }

    public function changeCover(Request $request)
    {
        $fileName = time() . "-" . Auth::user()->name . ".png";
        $result = ImageHelpers::updateProfileImage('energy-zone/user/images/user_cover_images/', $request->file('user_cover_images'), Auth::user()->name, 'not-link', $fileName);

        UserInfo::where('user_id', Auth::user()->id)->first()->update(['cover_image' => "energy-zone/user/images/user_cover_images/" . $fileName]);


    }


}
