<?php

namespace App\Http\Controllers\User;

use App\Country;
use App\Http\Requests\SignUpRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Http\Request;
use App\User;
use App\UserInfo;
use Mail;
use App\Mail\UserVerificationMail;
use App\Http\Controllers\Controller;
use App\Services\RegisterService;
use App\JobTitle;
use App\Company;

class UserRegistrationController extends Controller
{

    private $registrationService;

    public function __construct()
    {
        $this->registrationService = new RegisterService();
    }

    public function registerView(Request $request)
    {
        $user = null;
        if (!empty($request->id)) {
            if(!User::find($request->id)){
                echo "user not found";
                return;
            }else{
                $user = User::find($request->id);
            }
        }
        $data['user'] = $user;
        $data['countries'] = Country::all();
        $data['companies'] = Company::all();
        $data['job_titles'] = JobTitle::all();
        return view('user.register',compact('data'));
    }



    public function UpdateInfoSocialLogin(PasswordRequest $request)
    {


        try
        {
            $user = $this->registrationService->updatePasswordForSocialAccount($request);
            return redirect()->route('register', ['step' => 2, 'id' => $user->id]);
        } catch (\Exception $exception)
         {
            session()->flash('exception', $exception->getMessage());
            return redirect()->back();
        }


    }

    public function registerPost(SignUpRequest $request)
    {
        try
        {
            $user = $this->registrationService->register($request);
            return redirect()->route('register', ['step' => 2, 'id' => $user->id]);
        } catch (\Exception $exception) {
            session()->flash('exception', $exception->getMessage());
            return redirect()->route('register');
        }


    }


    public function stepInfoUpdate(Request $request)
    {
        try
        {

            $user = $this->registrationService->updateUserStep($request);
            if ($request->step == 2) {

                $this->registrationService->updateStep2($user, $request);
                return redirect()->route('user.register', ['step' => 3, 'id' => $user->id]);
            }

            if ($request->step == 3) {
                $user = User::find($request->user_id);
                $step3 = $this->registrationService->updateStep3($user, $request);
                if ($step3 == "active") {
                    session()->flash('success', 'Account Activated');
                    return redirect()->route('login');
                } else {
                    session()->flash('wrong-code', 'Please Enter Valid Code.');
                    return redirect()->back();
                }
            }
        }
        catch (\Exception $exception)
         {
           session()->flash('failed', $exception->getMessage());
            return redirect()->back();
        }


    }

}
