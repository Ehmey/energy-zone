<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Services\CountryService;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    public function editCountry(Request $request, CountryService $companyService)
    {
        $companyService->editUserCountry($request);
    }
}
