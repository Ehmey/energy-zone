<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Socialite;
use Auth;
use App\Services\FacebookAccountService;
use App\Services\GoogleAccountService;
use App\Http\Controllers\Controller;

class UsersSocialLoginController extends Controller
{

    ///////////////////////////////////////////
    /////////   FACEBOOK SOCIAL MEDIA ACTIONS
    /////////////////////////////////////////
    public function facebookRedirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookCallback(FacebookAccountService $facebookAccountService)
    {
        $user = $facebookAccountService->createOrGetUser(Socialite::driver('facebook')->user());
        auth()->login($user);
        return redirect()->to('user/profile');
    }



    ////////////////////////////////////////
    /////////   GMAIL SOCIAL MEDIA ACTIONS
    ///////////////////////////////////////
    public function gamilCallback(GoogleAccountService $googleAccountService)
    {
        try
        {
            $user = $googleAccountService->createOrGetUser(Socialite::driver('google')->user());
            Auth::loginUsingId($user->id);
            return redirect()->to('user/profile');
        } catch (\Exception $exception)
        {
            session()->flash('failed','Some Tecnical Issue');
            return redirect()->back();
        }
    }

    public function gmailRedirect()
    {
        return Socialite::driver('google')->redirect();
    }



    //////////////////////////////
    /////////   LINKEDIN ACTIONS
    /////////////////////////////
    public function linkedinRedirect(){
        return Socialite::driver('linkedin')->redirect();
    }


    public function linkedinCallback(GoogleAccountService $googleAccountService)
    {
        try
        {
            $user = $googleAccountService->createOrGetUser(Socialite::driver('linkedin')->user());
            Auth::loginUsingId($user->id);
            return redirect()->to('user/profile');
        } catch (\Exception $exception)
        {
            session()->flash('failed','Some Tecnical Issue');
            return redirect()->back();
        }
    }

}
