<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    //
    public function home(){
        echo "Home Page is In progrss";
    }


    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }


    public function adminLogout(){
        Auth::logout();
        return redirect()->route('admin.login');
    }



}
