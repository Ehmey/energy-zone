<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Auth;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()){
            return redirect()->route('login');
        }




        if(Auth::user()->role->name == 'Admin'){
return redirect()->route('admin.dashboard');
        }

//        if(Auth::user()->email_verified_at == null){
//            return redirect()->route('admin.dashboard');
//        }

        if (Auth::user()->step != 3) {
            $user = Auth::user();
            Auth::logout();
            return redirect()->route('register',['step'=>$user->step,'id'=>$user->id]);
        }



        view()->share([
            'recent_users' => User::peopleYouMayKnow( 0,5)
        ]);


        return $next($request);
    }
}
