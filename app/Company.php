<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\FormatDates;
use App\Scopes\ActiveScope;

class Company extends Model
{
    use FormatDates;
    protected $fillable = ['name','active','image_path'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }

    protected $dates = [
        'created_at',
        'updated_at'
    ];

}
