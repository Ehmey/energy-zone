<?php
/**
 * Created by PhpStorm.
 * User: bilal
 * Date: 4/27/18
 * Time: 12:22 PM
 */

namespace App\Services;


use App\Http\Requests\SignUpRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\UserInfo;
use Mail;
use App\Mail\UserVerificationMail;

class RegisterService
{


    public function updatePasswordForSocialAccount($request)
    {

        $user = User::find($request->user_id);
        $user->update(['password' => bcrypt($request->password)]);
        return $user;
    }


    /////    This function will register user and enter 1 record in userInfo agianst that user     /////
    public function register($request)
    {
        $user = User::create(['country_id'=>$request->country_id,'job_id'=>$request->job_id,'company_id'=>$request->company_id,'step' => $request->step, 'password' => bcrypt($request->password), 'email' => $request->email, 'name' => $request->name]);
        UserInfo::create(['user_id' => $user->id, 'auth_code' => rand(100000, 900000)]);
        return $user;
    }

    /////   This funciton will update user info for step 2    /////
    public function updateStep2($user = null, $request = null)
    {
        $userInfo = UserInfo::find($user->userInfo->id);
        $userInfo->update($request->except(['_token']));
        Mail::to($user)->send(new UserVerificationMail(['code' => $userInfo->auth_code,'email'=>$user->email]));

    }

    /////   This function is to update user information for step 3  /////
    public function updateStep3($user = null, $request = null)
    {
        if ($user->userInfo->auth_code == $request->auth_code) {dd('active');
            $user->update(['email_verified_at' => Carbon::now(), 'step' => 3]);
            return "active";
        } else {
            return "wrong-code";
        }
    }

    /////   This function will update user steps    /////
    public function updateUserStep($request)
    {
        $user = user::find($request->user_id);
        $user->update($request->except(['_token']));
        return $user;
    }


}