<?php
/**
 * Created by PhpStorm.
 * User: bilal
 * Date: 4/27/18
 * Time: 12:22 PM
 */

namespace App\Services;
use Yajra\Datatables\Datatables;
use App\Helpers\ImageHelpers;
use App\Company;
use Auth;
use Illuminate\Http\Request;





class CompanyService
{

    public function editUserCompany($request){
        Auth::user()->update(['company_id' => $request->name]);
    }

    public function addCompany($request){
        $fileName = time() . "-" . 'company_images' . ".png";
        ImageHelpers::updateProfileImage('energy-zone/company_images/', $request->file('company_image'), Auth::user()->name, 'not-link', $fileName);
        Company::create(['name'=>$request->name,'image_path' => 'energy-zone/company_images/' . $fileName]);
        session()->flash('company-added', $request->name.'  Added.');
    }


    public function getCompanyData()
    {
        $companies = Company::withoutGlobalScopes()->get();
        $result = [];

        foreach ($companies as $company) {
            $status = 'active';

            $active = "<a class=\"btn btn-danger btn-xs\" onclick=\"changeStatus(".$company->id.",'companies')\" >Deactive</a>";
            if ($company->active == 0) {
                $status = 'deactive';
                $active = "<a class=\"btn btn-success btn-xs\" onclick=\"changeStatus(".$company->id.",'companies')\">Active </a>";
            }
            $imageUrl = asset($company->image_path);
            $result['company'][] = [
                'action' => $active,
                'image_path' =>$imageUrl,
                'name' => $company->name,
                'id' => $company->id,
                'created_at' => $company->created_at,
                'status' => $status
            ];
        }


        return Datatables::of($result['company'])->make(true);
    }




    public function changeCompanyStatus($id){
        $company = Company::withoutGlobalScopes()->find($id);
        if($company->active == 0){
            $company->update(['active' => 1]);
            session()->flash('company-status-active',$company->name." Activated.");
            return redirect()->back();
        }else{
            $company->update(['active' => 0]);
            session()->flash('company-status-deactive',$company->name." DeActivated.");
            return redirect()->back();
        }
    }




}