<?php
/**
 * Created by PhpStorm.
 * User: bilal
 * Date: 4/27/18
 * Time: 12:22 PM
 */

namespace App\Services;
use Yajra\Datatables\Datatables;
use Auth;
use App\Helpers\ImageHelpers;
use App\Country;

use Illuminate\Http\Request;





class CountryService
{

    public function editUserCountry($request){
        Auth::user()->update(['country_id' => $request->name]);
    }

    public function addCountry($request){
        Country::create(['name'=>$request->name]);
    }


    public function getCountryData()
    {
        $countries = Country::withoutGlobalScopes()->get();
        $result = [];
        foreach ($countries as $country) {
            $status = 'active';
            $urlForActive = route('admin.change-status.country', [$country->id]);
            $active = "<a class=\"btn btn-xs btn-danger\" onclick=\"changeStatus(".$country->id.",'countries')\">Deactive</a>";
            if ($country->active == 0) {
                $status = 'deactive';
                $active = "<a class=\"btn btn-xs  btn-success\" onclick=\"changeStatus(".$country->id.",'countries')\">Active </a>";
            }

            $result['country'][] = [
                'action' => $active,
                'name' => $country->name,
                'id' => $country->id,
                'created_at' => $country->created_at,
                'status' => $status
            ];
        }

        return Datatables::of($result['country'])->make(true);

    }




    public function changeCountryStatus($id){
        $country = Country::withoutGlobalScopes()->find($id);
        if ($country->active == 0) {
            $country->update(['active' => 1]);
            session()->flash('country-status-active', $country->name . " Activated.");
        } else {
            $country->update(['active' => 0]);
            session()->flash('country-status-deactive', $country->name . " DeActivated.");

        }
    }




}