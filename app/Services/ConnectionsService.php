<?php
/**
 * Created by PhpStorm.
 * User: bilal
 * Date: 4/27/18
 * Time: 12:22 PM
 */

namespace App\Services;

use Yajra\Datatables\Datatables;
use App\Helpers\ImageHelpers;
use App\User\Connection;
use Auth;
use App\User;
use Illuminate\Http\Request;


class ConnectionsService
{

    public function addConnection($id, $request)
    {
        $connection = Connection::firstOrCreate(['accept_id' => $id, 'send_id' => Auth::user()->id, 'status' => 'Pending']);
        return $connection;
    }


    public function connections()
    {
        return Connection::where([['accept_id', Auth::user()->id], ['status', 'Accepted']])->orWhere([['send_id', Auth::user()->id], ['status', 'Accepted']]);
    }


    public function connectionsAddedByMe($otherPersonId)
    {
        return Connection::where([['accept_id', '!=', $otherPersonId], ['status', 'Accepted']])->where([['send_id', $otherPersonId], ['status', 'Accepted']]);
    }

    public function connectionsAddedByNotMe($otherPersonId)
    {
        return Connection::where([['accept_id', $otherPersonId], ['status', 'Accepted']])->where([['send_id', '!=', $otherPersonId], ['status', 'Accepted']]);
    }

    public function mutualFriends($otherPersonId, $user)
    {
        //try
        {
            $connectionsAddedByMe = (new ConnectionsService())->connectionsAddedByMe($otherPersonId)->pluck('accept_id')->toArray();
            $connectionsAddedByNotMe = (new ConnectionsService())->connectionsAddedByNotMe($otherPersonId)->pluck('send_id')->toArray();

            $comleteMyConnections = array_unique(array_merge($connectionsAddedByMe, $connectionsAddedByNotMe));

            $unsetMyIdFromArrayKey = array_search(Auth::user()->id, $comleteMyConnections);
            if ($unsetMyIdFromArrayKey != false) {
                unset($comleteMyConnections[$unsetMyIdFromArrayKey]);
            }


            $unsetMyIdFromArrayKeyOther = array_search($otherPersonId, $comleteMyConnections);
            if ($unsetMyIdFromArrayKeyOther != false) {
                unset($comleteMyConnections[$unsetMyIdFromArrayKeyOther]);
            }

            $connections = Connection::whereIn('accept_id',$comleteMyConnections)->where([ ['status', 'Accepted']])->orWhere([['status', 'Acepted'], ['accept_id', Auth::user()->id]])->whereIn('send_id', $comleteMyConnections);
          //  dd($connections->get());

            if ($connections->get()->count() > 0) {
                $mutalFriends['mutual_friends'] = $connections;
                $mutalFriends['mutual_friends_count'] = $mutalFriends['mutual_friends']->get()->count();
                $mutalFriends['mutual_friends_public_profile'] = "You and ".$mutalFriends['mutual_friends']->get()->count()." others Know ".$user->name;

                //if ($mutalFriends['mutual_friends']->get()->count() == 1)
//                {dd($mutalFriends['mutual_friends']->get()[0]);
//                    if ($mutalFriends['mutual_friends']->get()[0]->send_id != Auth::user()->id) {
//                        $mutalFriends['mutual_friends_public_profile'] = "You and " . $mutalFriends['mutual_friends']->get()[0]->user->name . " know " . $user->name;
//                    } else {//dd($connections->get()); //dd( User::find($mutalFriends['mutual_friends']->get()[0]->accept_id)->name);
//                        $mutalFriends['mutual_friends_public_profile'] = "You and " . User::find($mutalFriends['mutual_friends']->get()[0]->accept_id)->name . " know " . $user->name;
//                    }
//                }

//                elseif ($mutalFriends['mutual_friends']->get()->count() == 2) {
//
//                    $second = "";
//                    $i = 0;
//                    foreach ($mutalFriends['mutual_friends']->limit(2)->get() as $data) {
//                        if ($data->send_id != $otherPersonId) {
//                            if ($i != 1) {
//                                $second = $second . $data->user->name . " and ";
//                            } else {
//                                $second = $second . $data->user->name;
//                            }
//
//                        } else {
//                            if ($i != 1) {
//                                $second = $second . User::find($data->accept_id)->name . " and ";
//                            } else {
//                                $second = $second . User::find($data->accept_id)->name;
//                            }
//                        }
//                    }
//                    $second = $second . " know " . $user->name;
//
//                }

//                elseif ($mutalFriends['mutual_friends']->get()->count() > 2) {
//                    $others = $mutalFriends['mutual_friends']->get()->count() - 2;
//                    $mutalFriends['mutual_friends_public_profile'] = "You " . $mutalFriends['mutual_friends']->get()[0]->user->name . ", " . $mutalFriends['mutual_friends']->get()[1]->user->name . " know " . $user->name . " and " . $others . " others. ";
//                }
            } else {
                $mutalFriends['mutual_friends_count'] = 0;
                $mutalFriends['mutual_friends_public_profile'] = "There are no mutual friends. Only You know " . $user->name;
            }
          //  dd($mutalFriends);
            return $mutalFriends;
        } //catch (\Exception $exception)
        {
            $mutalFriends['mutual_friends_count'] = 0;
            $mutalFriends['mutual_friends_public_profile'] = "There are no mutual friends. Only You know " . $user->name;
            return $mutalFriends;
        }
    }


}