<?php
/**
 * Created by PhpStorm.
 * User: bilal
 * Date: 4/27/18
 * Time: 12:22 PM
 */

namespace App\Services;


use App\Http\Requests\SignUpRequest;
use Illuminate\Http\Request;
use App\User;
use App\User\Reply;
use App\UserInfo;
use Mail;
use Auth;
use App\User\Comment;
use App\Mail\UserVerificationMail;

class ReplyService
{


    public function loadReply($id, $request)
    {
        $replies = Reply::where('comment_id', $id)->orderBy('id', 'desc')->get();
        if($request->has('order')){
            $replies = Reply::where('comment_id', $id)->orderBy('id', 'asc')->get();
        }
        $data = null;
        foreach ($replies as $reply) {
            $selfStatus = "mine";
            if($reply->user_id != Auth::user()->id){$selfStatus = 'not_mine';}
            $data['replies'][] = ['self_status'=>$selfStatus,'reply_id' => $reply->id, 'user_id' => $reply->user->id, 'reply' => $reply->reply, 'created_at' => $reply->created_at, 'user_name' => $reply->user->name, 'profile_image' => $reply->user->profile_image];
        }
        return $data;
    }


    public function deleteReply($id)
    {
        $data = null;
        Reply::destroy($id);
    }


    public function postReply($commentId,Request $request,$userId)
    {
        $data = null;
        Reply::create(['comment_id'=>$commentId,'reply'=>$request->reply,'user_id'=>$userId]);
        $data['reply'] = "success";
        $data['comments_count'] = Comment::find($commentId)->replies()->count();
        return $data;
    }


}