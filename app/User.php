<?php

namespace App;

use App\Scopes\ActiveScope;
use App\User\Connection;
use Auth;
use App\Traits\FormatDates;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable,FormatDates;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'email_verified_at' ,'job_title_id', 'company_id', 'country_id', 'welcome_flag', 'profile_image', 'name', 'email', 'password', 'is_block', 'role_id', 'step', 'active'
    ];


    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }

    public function pendingRequests()
    {
        return $this->hasOne('App\User\Connection', 'send_id', 'id');
    }


    public function acceptPendingRequests()
    {
        return $this->hasOne('App\User\Connection', 'accept_id', 'id');
    }


    public function userInfo()
    {
        return $this->hasOne('App\UserInfo');
    }

    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id', 'id');
    }


    public function education()
    {
        return $this->hasMany('App\User\UserEducation', 'user_id', 'id');
    }


    public function experince()
    {
        return $this->hasMany('App\User\UserExperince', 'user_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company')->withoutGlobalScope(ActiveScope::class);
    }


    public function posts()
    {
        return $this->hasMany('App\User\Post');
    }


    public static function connections()
    {
        return Connection::where('accept_id', Auth::user()->id)->where('send_id', '!=', Auth::user()->id)->get();
    }

    public static function pendingConnections()
    {
        return Connection::where('accept_id', Auth::user()->id)->where('status', 'Pending')->get();
    }


    public static function acceptedConnections()
    {
        return Connection::where([['accept_id', Auth::user()->id],['status', 'Accepted']])->orWhere([['send_id',Auth::user()->id],['status', 'Accepted']]);
    }

    public static function peopleYouMayKnow($start = 0, $limit = 12)
    {

        $connections = self::connections();
        $data = [];
        $users = User::where([['role_id', '!=', 1], ['id', '!=', Auth::user()->id]])->orwhere([ ['id', '!=', Auth::user()->id],['role_id', '!=', 1], ['company_id', '=', Auth::user()->company_id], ['job_title_id', '=', Auth::user()->job_title_id]])->skip($start)->take($limit)->orderBy('id','desc')->get();
        foreach ($users as $user) {
           if(Connection::where(
               [
                   ['accept_id' ,Auth::user()->id],
                   ['send_id' , $user->id]
               ])
               ->orWhere(
                   [
                       ['accept_id',$user->id],
                       ['send_id',Auth::user()->id]
                   ]
               )
               ->get()->count() == 0)
            {
                $data['user'][] = [$user];
            }
        }


        return $data;
    }

    public function country()
    {
        return $this->belongsTo('App\Country')->withoutGlobalScope(ActiveScope::class);
    }

    public function jobTitle()
    {
        return $this->belongsTo('App\JobTitle', 'job_title_id', 'id')->withoutGlobalScope(ActiveScope::class);
    }


    public function SocialAccount()
    {
        return $this->hasOne('App\SocialAccounts');
    }


    public function skills()
    {
        return $this->belongsToMany('App\User\Skill', 'user_skills', 'user_id', 'skill_id')->withoutGlobalScope(ActiveScope::class);
    }


    public function languages()
    {
        return $this->belongsToMany('App\User\Language', 'user_languages', 'user_id', 'language_id');
    }


    public function getProfileImageAttribute($profile_image)
    {

        if ($profile_image != null) {
            return asset($profile_image);
        } else {
            return asset('energy-zone/userIcon.png');
        }

    }


    protected $hidden = [
        'password', 'remember_token',
    ];
}
