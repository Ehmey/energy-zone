<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/migrate', function () {
    exec('composer dump-autoload');
    Artisan::call('migrate:refresh');
    Artisan::call('db:seed');
    dd(\App\User::find(1));
    dd(\App\Company::all());
});



Route::get('/user', 'UserController@index');

Route::get('/register', 'UserRegistrationController@index')->name('home');

Route::namespace('admin')->group(function () {
    Route::get('/register', 'UserRegistrationController@registerView')->name('register');
});


Auth::routes();


Route::get('/home', 'User\HomeController@home')->name('home');


//////  Routes after User login
Route::middleware(['CheckUser'])->namespace('User')->group(function () {




    Route::post('user/do-post', 'UserPostController@doPost')->name('user.do-post');
    Route::post('user/edit/post/{postId}', 'UserPostController@editPost')->name('user.edit.post');
    Route::get('user/delete/post/{postId}', 'UserPostController@deletePost')->name('user.delete.post');
    Route::get('user/hide/post/{postId}', 'UserPostController@hidePosts')->name('user.hide.post');


    //////////////      COMMENTS ROUTES
    Route::get('user/post/{postId}/comment', 'UserCommentController@postComment');
    Route::get('user/post/{postId}/load-comment', 'UserCommentController@loadComment');
    Route::get('/user/post/comment/{commentid}/delete', 'UserCommentController@delteComment');


    ///////////     USER SKILL

    Route::get('user/delete/skill/{id}', 'SkillController@deleteSkill')->name('user.delete.skill');
    Route::post('user/add/skill', 'SkillController@addSkill')->name('user.add.skill');


    //////////  USER LANGUAGE


    Route::get('user/delete/language/{id}', 'LanguageController@deleteLanguage')->name('user.delete.language');
    Route::post('user/add/language', 'LanguageController@addLanuage')->name('user.add.language');


    //////////  USER EDUCATION

    Route::get('user/delete/education/{id}', 'EducationController@deleteEducation')->name('user.delete.education');
    Route::post('user/add/education', 'EducationController@addEducation')->name('user.add.education');
    Route::post('user/edit/education', 'EducationController@editEducation')->name('user.edit.education');
    Route::post('user/edit/profile-image', 'UserProfileController@editProfileImage');


    ///////////     USER JOBS
     Route::get('user/edit/jobtitle', 'JobsController@editJob');


     /////////      USER COMPANY

    Route::get('user/edit/company', 'CompanyController@editCompany');


    /////////      USER COUNTRY

    Route::get('user/edit/country', 'CountryController@editCountry');

    //////////  USER EXPERINCE

    Route::post('user/add/experince', 'ExperinceController@addExperince')->name('user.add.experince');
    Route::get('user/delete/experince/{id}', 'ExperinceController@deleteExperince')->name('user.delete.experince');
    Route::post('user/edit/experience', 'ExperinceController@editExperince')->name('user.edit.experience');




    /////////////       LIKE ROUTES

    Route::get('user/post/{postId}/like', 'UserLikeController@postLike');
    Route::get('user/comment/{commentId}/like', 'UserLikeController@commentLike');


    ////////////        REPLY ROUTES

    Route::get('user/comment/{commentId}/reply', 'ReplyController@loadReply');
    Route::get('user/reply/{replyId}/delete', 'ReplyController@deleteReply');
    Route::get('user/comment/{commentId}/reply/post', 'ReplyController@postReply');





    ////////////        PUBLIC PROFILE ROUTE

    Route::get('user/{userId}/public-profile', 'UserProfileController@publicProfile')->name('user.public-profile');
    Route::post('user/edit/cover-image', 'UserProfileController@changeCover');
    Route::get('user/edit/desc', 'UserProfileController@editDesc');


    /////       POST ROUTES

    Route::get('user/load/posts', 'UserPostController@getPosts');


    ///////////     PROFILE ROUTES

    Route::get('/user/profile', 'UserProfileController@profile')->name('profile');


    //////////      EDIT PROFILE ROUTES

    Route::get('user/edit/name', 'UserProfileController@userEditName');
    Route::post('user/edit-profile', 'UserProfileController@editProfilePost')->name('user.edit-profile');
    Route::get('user/edit-profile', 'UserProfileController@editProfile')->name('user.edit-profile');



    //////////      CONNECTION ROUTES


    Route::get('user/{userId}/send-request', 'ConnectionController@sendRequest');
    Route::get('user/connections', 'ConnectionController@connections')->name('user.connections');



    //////////      CONNECTION ROUTES


    Route::get('user/{userId}/send-request', 'ConnectionController@sendRequest');
    Route::get('user/connections', 'ConnectionController@sendRequest')->name('user.edit-profile');



    //////////      CONNECTION ROUTES


    Route::get('user/{userId}/send-request', 'ConnectionController@sendRequest');
    Route::get('user/connections', 'ConnectionController@connections')->name('user.connections');
    Route::get('user/people-you-may-know/{id}', 'ConnectionController@peopleYouMayKnow');
    Route::get('user/accept_invitation/{id}', 'ConnectionController@acceptInvitation');












});


Route::get('/updatePic', 'user\UserProfileController@updateImage');
















//////  Routes without login
Route::middleware(['WithoutLogin'])->group(function () {

    Route::get('/register', 'User\UserRegistrationController@registerView')->name('register');
    Route::post('register', 'User\UserRegistrationController@registerPost')->name('user.register');
    Route::post('step', 'User\UserRegistrationController@stepInfoUpdate')->name('user.step');
    Route::get('/login', 'User\LoginController@loginView')->name('login');

    Route::post('for-social-login-update-info', 'User\UserRegistrationController@UpdateInfoSocialLogin')->name('user.for-social-login-update-info');

    Route::get('/facebook-login', 'User\UsersSocialLoginController@facebookRedirect')->name('facebook-login');
    Route::get('/facebook-callback', 'User\UsersSocialLoginController@facebookCallback');
    Route::get('/gmail-callback', 'User\UsersSocialLoginController@gamilCallback');
    Route::get('/linkedin-callback', 'User\UsersSocialLoginController@linkedinCallback');
    Route::get('/linkedin-login', 'User\UsersSocialLoginController@linkedinRedirect')->name('linkedin-login');
    Route::get('/gmail-login', 'User\UsersSocialLoginController@gmailRedirect')->name('gmail-login');
});


















Route::get('/logout', 'User\HomeController@logout');
Route::get('admin/logout', 'User\HomeController@adminLogout')->name('admin.logout');

Route::middleware(['CheckAdmin'])->namespace('Admin')->group(function () {

    ////        ADMIN COMPANY ROUTES

    Route::get('admin/anydata/company/data','CrudsController@anyData')->name('admin.datatables.company.data');
    Route::get('admin/change-status/company/{companyId}', 'CrudsController@changeCompanyStatus')->name('admin.change-status.company');
    Route::post('admin/add/company','CrudsController@addCompany')->name('admin.add.company');

    ////    ADMIN SKILLS ROUTES

    Route::get('admin/anydata/skills/data','CrudsController@skillsData')->name('admin.datatables.skills.data');
    Route::get('admin/change-status/skill/{skillId}', 'CrudsController@changeSkillStatus')->name('admin.change-status.skill');
    Route::get('admin/add/skill','CrudsController@addSkill')->name('admin.add.skill');

    ////    ADMIN COUNTRIES

    Route::get('admin/anydata/countries/data','CrudsController@countriesData')->name('admin.datatables.countries.data');
    Route::get('admin/change-status/country/{countryId}', 'CrudsController@changeCountryStatus')->name('admin.change-status.country');
    Route::get('admin/add/country','CrudsController@addCountry')->name('admin.add.country');

    ////    ADMIN SCHOOLS

    Route::get('admin/anydata/schools/data','CrudsController@schoolsData')->name('admin.datatables.schools.data');
    Route::get('admin/change-status/school/{schoolId}', 'CrudsController@changeSchoolStatus')->name('admin.change-status.school');
    Route::post('admin/add/school','CrudsController@addSchool')->name('admin.add.school');


    ////    ADMIN DEGREES

    Route::get('admin/anydata/degrees/data','CrudsController@degreesData')->name('admin.datatables.degrees.data');
    Route::get('admin/add/degree','CrudsController@addDegree')->name('admin.add.degree');
    Route::get('admin/change-status/degree/{schoolId}', 'CrudsController@changeDegreeStatus')->name('admin.change-status.degree');

    ////    ADMIN LANGUAGES

    Route::get('admin/anydata/languages/data','CrudsController@languagesData')->name('admin.datatables.languages.data');
    Route::get('admin/change-status/language/{languageId}', 'CrudsController@changeLanguageStatus')->name('admin.change-status.language');
    Route::get('admin/add/language','CrudsController@addLanguage')->name('admin.add.language');

    Route::get('admin/get-setup-record', 'CrudsController@getSetupRecord');
    Route::get('admin/get-setups-data', 'CrudsController@getSetup');

    Route::get('admin/dashboard', 'ProfileController@profile')->name('admin.dashboard');
    Route::get('admin/login', 'LoginController@loginView')->name('admin.login');




    ///////////////             ADMIN USER SETTINGS

    Route::get('admin/users/management', 'UsersController@index')->name('admin.users.management');
    Route::get('admin/get-users-data', 'UsersController@getSetup');
    Route::get('admin/user/data','UsersController@usersData')->name('admin.datatables.users.data');
    Route::get('admin/change-status/user/{userId}', 'UsersController@changeUserStatus')->name('admin.change-status.user');

    //////////////          ADMIN SETTINGS

    Route::get('admin/settings', 'CrudsController@profileSettings')->name('admin.profile.settings');
    Route::get('admin/general/settings', 'CrudsController@generalSettings')->name('admin.general.settings');

});





